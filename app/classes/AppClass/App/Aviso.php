<?php

namespace AppClass\App;

/**
 * Avisos
 *
 * @package \AppClass\App\Aviso
 * @created 10/04/2014
 * @author Jalon Vítor Cerqueira Silva
 * @version 1.0.1
 */
class Aviso {
	
	/**
	 * Objeto que irá guardar a instância
	 */
	private static $instance;
	
	/**
	 * Construtor
	 *
	 * @return object
	 */
	public static function criar($tipo,$mensagem) {
	
		/**
		 * Verifica se o tipo do Grid é válido
		 */
		switch ($tipo) {
			case \AppClass\App\Aviso\Tipo::ERRO:
				self::$instance	= new \AppClass\App\Aviso\Tipo\Erro($mensagem);
				break;
			case \AppClass\App\Aviso\Tipo::ALERTA:
				self::$instance	= new \AppClass\App\Aviso\Tipo\Alerta($mensagem);
				break;
			case \AppClass\App\Aviso\Tipo::INFO:
				self::$instance	= new \AppClass\App\Aviso\Tipo\Info($mensagem);
				break;
			case \AppClass\App\Aviso\Tipo::SUCCESS:
				self::$instance	= new \AppClass\App\Aviso\Tipo\Success($mensagem);
				break;
			default:
				die ('Tipo de aviso não implementado !!!');
				break;
		}
	
		return self::$instance;
	}
	
	/**
	 * Construtor privado, usar \AppClass\App\Aviso::getInstance();
	 */
	private function __construct($tipo,$mensagem) {
	
	}
	
	/**
	 * @return the $instance
	 */
	public static function getInstance() {
		return self::$instance;
	}


}