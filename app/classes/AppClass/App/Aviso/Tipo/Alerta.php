<?php

namespace AppClass\App\Aviso\Tipo;

/**
 * 
 *
 * @package AppClass\App\Aviso\Tipo\Alerta
 * @created 10/04/2013
 * @author Jalon Vítor Cerqueira Silva
 * @version 1.0.1
 *         
 */
class Alerta extends \AppClass\App\Aviso\Tipo {
	
	/**
	 * Construtor
	 *
	 * @param string $mensagem
	 * @return void
	 */
	public function __construct($mensagem) {
	
		/**
		 * Define o ícone
		 */
		$this->setIcone("fa fa-warning");
		
		/**
		 * Define o tipo
		 */
		$this->setTipo(\AppClass\App\Aviso\Tipo::ALERTA);
		
		/**
		 * Define a classe
		 */
		$this->setClasse("alert-warning");
		
		/**
		 * Define a mensagem
		 */
		$this->setMensagem($mensagem);
	}
	
}