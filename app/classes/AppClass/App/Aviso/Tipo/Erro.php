<?php

namespace AppClass\App\Aviso\Tipo;

/**
 * 
 *
 * @package AppClass\App\Aviso\Tipo\Erro
 * @created 10/04/2013
 * @author Jalon Vítor Cequeira Silva
 * @version 1.0.1
 *         
 */
class Erro extends \AppClass\App\Aviso\Tipo {
	
	/**
	 * Construtor
	 *
	 * @param string $mensagem
	 * @return void
	 */
	public function __construct($mensagem) {
	
		/**
		 * Define o ícone
		 */
		$this->setIcone("fa fa-times-circle");
		
		/**
		 * Define o tipo
		 */
		$this->setTipo(\AppClass\App\Aviso\Tipo::ERRO);
		
		/**
		 * Define a classe
		 */
		$this->setClasse("alert-danger");
		
		/**
		 * Define a mensagem
		 */
		$this->setMensagem($mensagem);
	}
	
}