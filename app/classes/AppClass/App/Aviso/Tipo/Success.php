<?php

namespace AppClass\App\Aviso\Tipo;

/**
 * 
 *
 * @package AppClass\App\Aviso\Tipo\Success
 * @created 02/09/2016
 * @author Jalon Vítor Cerqueira Silva
 * @version 1.0.1
 *         
 */
class Success extends \AppClass\App\Aviso\Tipo {
	
	/**
	 * Construtor
	 *
	 * @param string $mensagem
	 * @return void
	 */
	public function __construct($mensagem) {
	
		/**
		 * Define o ícone
		 */
		$this->setIcone("fa fa-info");
		
		/**
		 * Define o tipo
		 */
		$this->setTipo(\AppClass\App\Aviso\Tipo::SUCCESS);
		
		/**
		 * Define a classe
		 */
		$this->setClasse("alert-success");
				
		/**
		 * Define a mensagem
		 */
		$this->setMensagem($mensagem);
	}
	
}