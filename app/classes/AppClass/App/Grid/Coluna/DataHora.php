<?php

namespace AppClass\App\Grid\Coluna;

/**
 * Gerenciar as colunas to tipo DataHora
 *
 * @package DataHora
 * @author Jalon Vitor Ceruqueira Silva
 * @version 1.0
 *         
 */
class DataHora extends \AppClass\App\Grid\Coluna {
	
	/**
	 * Construtor
	 */
	public function __construct() {
		parent::__construct ();
		
		$this->setTipo ( \AppClass\App\Grid\Tipo::TP_DATAHORA );
	}
	
	/**
	 * Gerar o código Html da célula
	 */
	public function geraHtmlValor($valor) {
		$html = $valor;
		return ($html);
	}
}
