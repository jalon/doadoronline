<?php

namespace AppClass\App;

use AppClass\App\Mascara\Tipo\Cnpj;
/**
 * Gerenciar Mascaras
 *
 * @package \AppClass\App\Mascara
 * @created 31/08/2014
 * @author Jalon Vitor Cerqueira Silva
 * @version 1.0.3
 *         
 */
class Mascara  {
	
	/**
	 * Objeto que irá guardar a instância
	 */
	private static $instance;
	
	/**
	 * Construtor 
	 *
	 * @return object
	 */
	public static function tipo($tipo) {
		
		/**
		 * Verifica se o tipo da Máscara é válido
		 */
		switch ($tipo) {
			case \AppClass\App\Mascara\Tipo::TP_CARTAO:
				self::$instance	= new \AppClass\App\Mascara\Tipo\Cartao();
				break;
			case \AppClass\App\Mascara\Tipo::TP_CEP:
				self::$instance	= new \AppClass\App\Mascara\Tipo\Cep();
				break;
			case \AppClass\App\Mascara\Tipo::TP_CNPJ:
				self::$instance	= new \AppClass\App\Mascara\Tipo\Cnpj();
				break;
			case \AppClass\App\Mascara\Tipo::TP_CPF:
				self::$instance	= new \AppClass\App\Mascara\Tipo\Cpf();
				break;
			case \AppClass\App\Mascara\Tipo::TP_DATA:
				self::$instance	= new \AppClass\App\Mascara\Tipo\Data();
				break;
			case \AppClass\App\Mascara\Tipo::TP_DINHEIRO:
				self::$instance	= new \AppClass\App\Mascara\Tipo\Dinheiro();
				break;
			case \AppClass\App\Mascara\Tipo::TP_FONE:
				self::$instance	= new \AppClass\App\Mascara\Tipo\Fone();
				break;
			case \AppClass\App\Mascara\Tipo::TP_NUMERO:
				self::$instance	= new \AppClass\App\Mascara\Tipo\Numero();
				break;
			case \AppClass\App\Mascara\Tipo::TP_PORCENTAGEM:
				self::$instance	= new \AppClass\App\Mascara\Tipo\Porcentagem();
				break;
			case \AppClass\App\Mascara\Tipo::TP_TEMPO:
				self::$instance	= new \AppClass\App\Mascara\Tipo\Tempo();
				break;
			case \AppClass\App\Mascara\Tipo::TP_PLACA:
				self::$instance	= new \AppClass\App\Mascara\Tipo\Placa();
				break;
			default:
				die ('Tipo de Mascara não implementado !!!');
				break;
		}
		
		return self::$instance;
	}
	
	/**
	 * Construtor privado, usar \AppClass\App\Mascara::getInstance();
	 */
	private function __construct($tipo) {
		
	}
	

}
