<?php

namespace AppClass\App\Mascara\Tipo;

/**
 * Gerenciar as Mascaras do Tipo CARTAO
 *
 * @package \AppClass\App\Mascara\Tipo\Cartao
 * @created 31/08/2014
 * @author Jalon Vitor Cerqueira Silva
 * @version 1.0.1
 *         
 */
class Cartao extends \AppClass\App\Mascara\Tipo {
	
	/**
	 * Construtor
	 */
	public function __construct() {
		
		parent::__construct();

		/**
		 * Carrega as configurações
		 */
		$this->setTipo($this::TP_CARTAO);
		$this->_loadConfigFromDb();
		
	}

}
