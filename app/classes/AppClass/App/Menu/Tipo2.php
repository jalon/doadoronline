<?php

namespace AppClass\App\Menu;

/**
 * Gerenciar os menus do tipo 2 (menu Lateral esquerda)
 *
 * @package \AppClass\App\Menu\Tipo2
 * @author Jalon Vitor Cerqueira Silva
 * @version 1.0.1
 */ 
class Tipo2 extends \AppClass\App\Menu\Tipo {
	
	/**
	 * Construtor
	 * 
	 * @param string $tipo        	
	 * @return void
	 */
	public function __construct($tipo) {
		parent::__construct($tipo);
	}
	
	
	/**
	 * Inicializa o código html, de acordo com o tipo do menu
	 */
	protected function iniciaHtml() {
		global $system,$_user,$_emp,$tr,$_org;
		
		/** Montar o combo de Organização **/
		if (is_object($_org)) {
			$codOrg				= $_org->CODIGO;
			$ident				= $_org->IDENTIFICACAO;
		}else{
			$codOrg				= "!";
			$ident				= "Nenhuma organização";
		}
		
		/** Carrega as informações do usuário **/
		$msg		= ($_user->COD_SEXO == "FEM") ? "Bem Vinda" : "Bem Vindo";
		$avatar		= /*($_user->AVATAR) 	? $_user->AVATAR :*/ IMG_URL."/users/user-35.jpg";
		$nomeUsu	= /*($_user->APELIDO)	? $_user->APELIDO :*/ \AppClass\App\Util::getNomeCompleto($_user->NOME);
		
		$this->html	.= '
	<!-- Modal Logout -->
	<div class="md-modal md-just-me" id="logout-modal">
		<div class="md-content">
			<h3><strong>Sair</strong></h3>
			<div>
				<p class="text-center">Tem certeza quer deseja sair do sistema?</p>
				<p class="text-center">
				<button class="btn btn-danger md-close">Não!</button>
				<a href="%ROOT_URL%/Seg/logoff.php" class="btn btn-success md-close">Sim, Tenho certeza.</a>
				</p>
			</div>
		</div>
	</div>
	<!-- Modal End -->
<!-- Begin page -->
<div id="wrapper">
		
<!-- Top Bar Start -->
<div class="topbar">
    <div class="topbar-left">
        <div class="logo">
            <h1><a href="/"><img src="%IMG_URL%/Logo-vertical.png" width="60%" height="60%" alt="Logo"></a></h1>
        </div>
        <button class="button-menu-mobile open-left">
        <i class="fa fa-bars"></i>
        </button>
    </div>
    <!-- Botão exibição móvel a entrar menu lateral -->
    <div class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="navbar-collapse2">
                <ul class="nav navbar-nav navbar-right top-navbar">
					<li class="dropdown iconify hide-phone">';
						$grupo = \AppClass\Seg\Usuario::listaGrupoAcesso();
						
						if($system->getCodGrupoAcesso() == 'DOA'){
							$nomeGrupo = 'Doador';
						}else if($system->getCodGrupoAcesso() == 'REC'){
							$nomeGrupo = 'Receptor';
						}else if($system->getCodGrupoAcesso() == 'PAR'){
							$nomeGrupo = 'Parceiro';
						}else{
							$nomeGrupo = 'Administrador';
						
						
						$this->html .= str_repeat(\AppClass\App\ZWS::TAB,6).'<a href="#" class="dropdown-toggle" data-toggle="dropdown" href="#">'.\AppClass\App\ZWS::NL;
						$this->html .= str_repeat(\AppClass\App\ZWS::TAB,7).'<i class="ace-icon fa fa-building-o"></i>&nbsp;'.$nomeGrupo.\AppClass\App\ZWS::NL;
						$this->html .= str_repeat(\AppClass\App\ZWS::TAB,6).'</a>' .\AppClass\App\ZWS::NL;
						
						$this->html .= str_repeat(\AppClass\App\ZWS::TAB,6).'<ul class="dropdown-menu">'.\AppClass\App\ZWS::NL;
						$this->html .= str_repeat(\AppClass\App\ZWS::TAB,7).'<li class="dropdown-header"><i class="ace-icon fa fa-building-o"></i> Selecione :</li>'.\AppClass\App\ZWS::NL;
							
						for ($i = 0; $i < sizeof($grupo); $i++) {
							if ($grupo[$i]->CODIGO == $system->getCodGrupoAcesso()) {
								$icone		= "ace-icon fa fa-circle";
								$sel 		= '<span class="pull-right"><i class="ace-icon fa fa-check"></i></span>';
								$nome		= "<b>".$grupo[$i]->DESCRICAO."</b>";
							}else{
								$icone		= "ace-icon fa fa-circle-o";
								$sel 		= null;
								$nome		= $grupo[$i]->DESCRICAO;
							}
					
							$url 	= "?did=".\AppClass\App\Util::encodeUrl('_codGrupoAcesso='.$grupo[$i]->CODIGO);
					
							$this->html .= str_repeat(\AppClass\App\ZWS::TAB,7).'<li><a href="'.$url.'"><div class="clearfix"><span class="pull-left"><i class="ace-icon '.$icone.'"></i>&nbsp;'.$nome.'</span>'.$sel.'</div></a></li>'.\AppClass\App\ZWS::NL;
						}
							
							$this->html	.= str_repeat(\AppClass\App\ZWS::TAB,6).'</ul></li>'.\AppClass\App\ZWS::NL;
							$this->html	.= str_repeat(\AppClass\App\ZWS::TAB,5).'</li>'.\AppClass\App\ZWS::NL;
						}
					## NOTIFICACOES ##								
    $this->html	.= '<li id="notificacoesID" class="dropdown iconify hide-phone"></li>
    		
                   <!-- <li class="dropdown iconify hide-phone">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i><span class="label label-danger absolute">3</span></a>
                        <ul class="dropdown-menu dropdown-message">
                            <li class="dropdown-header notif-header"><i class="icon-mail-2"></i> New Messages</li>
                            <li class="unread">
                                <a href="#" class="clearfix">
                                    <img src="%IMG_URL%/users/chat/2.jpg" class="xs-avatar ava-dropdown" alt="Avatar">
                                    <strong>John Doe</strong><i class="pull-right msg-time">5 minutes ago</i><br />
                                    <p>Duis autem vel eum iriure dolor in hendrerit ...</p>
                                </a>
                            </li>
                            <li class="unread">
                                <a href="#" class="clearfix">
                                    <img src="%IMG_URL%/users/chat/1.jpg" class="xs-avatar ava-dropdown" alt="Avatar">
                                    <strong>Sandra Kraken</strong><i class="pull-right msg-time">22 minutes ago</i><br />
                                    <p>Duis autem vel eum iriure dolor in hendrerit ...</p>
                                </a>
                            </li>
                            <li class="dropdown-footer"><div class=""><a href="#" class="btn btn-sm btn-block btn-primary"><i class="fa fa-share"></i> Ver todas messagens</a></div></li>
                        </ul>
                    </li> -->
                    <li class="dropdown iconify hide-phone"><a href="#" onclick="javascript:toggle_fullscreen()"><i class="icon-resize-full-2"></i></a></li>
                    <li class="dropdown topbar-profile">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><!--<span class="rounded-image topbar-profile-image"><img src="'.$avatar.'"></span>--> <strong>'.$nomeUsu["primeiro_nome"].'</strong> <i class="fa fa-caret-down"></i></a>
                        <ul class="dropdown-menu">
                            <!--<li><a href="#">Meu Perfil</a></li>
                            <li><a href="#">Mudar Senha</a></li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="icon-help-2"></i> Ajuda</a></li>
                            <li><a href="lockscreen.html"><i class="icon-lock-1"></i> Bloquear-me</a></li>-->
                            <li><a class="md-trigger" data-modal="logout-modal"><i class="icon-logout-1"></i> Sair</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>';
	}
	
	/**
	 * Inicializa o código html, de acordo com o tipo do menu
	 */
	protected function finalizaHtml() {
		$this->html .= '
	<!-- End of page -->
	<!-- the overlay modal element -->
	<div class="md-overlay"></div>
	<!-- End of eoverlay modal -->
	<script>
		var resizefunc = [];
	</script>';
	}
	
	/**
	 * Inicia o código html do menu lateral
	 */
	private function iniciaMenuLateral() {
		
	}
	
	/**
	 * Finaliza o código html do menu lateral
	 */
	private function finalizaMenuLateral() {

	}
	
	/**
	 * Gerar o código html do menu
	 * @return void
	 */
	protected function geraHtml() {

		/**
		 * Inicializa o código HTML
		 */
		$this->iniciaHtml();
		$this->iniciaMenuPadrao();
		//$this->iniciaMenuLateral();
		
		if ($this->_array) {
			foreach ($this->_array as $codigo => $array) {
				$this->geraHtmlItem($codigo,$array);
			}
		}
		$this->criaDivCentral();
		$this->finalizaMenuPadrao();
		//$this->finalizaMenuLateral();
		
		/**
		 * Finaliza o código HTML
		 */
		$this->finalizaHtml();
		
	}
	
}
