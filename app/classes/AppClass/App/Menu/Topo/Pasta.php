<?php

namespace AppClass\App\Menu\Topo;

/**
 * Gerenciar as pastas do Menu (Sub-Menu) 
 *
 * @package Pasta
 * @author Jalon Vítor Cerqueira Silva
 * @version 1.0.1
 */
class Pasta {
	
	/**
	 * Codigo da Pasta
	 * @var string
	 */
	private $codigo;
	
	/**
	 * Nome da pasta
	 * @var string
	 */
	private $nome;
	
	/**
	 * Icone da pasta
	 */
	private $icone;
	
	/**
	 * Codigo do Item Pai (superior)
	 * @var Integer
	 */
	private $itemPai;
		
	/**
	 * Nivel do pasta
	 * @var Integer
	 */
	private $nivel;
	
	
	/**
	 * Construtor
	 * 
	 * @return void
	 */
	public function __construct() {
		
	}
	
	/**
	 * Gera o código html 
	 * @return void
	 */
	public function geraHtml() {
		global $tr;
		if ($this->getIcone() != null) {
			$menuIcone	= '<i class="'.$this->getIcone().'"></i>';
		}else{
			$menuIcone	= '<i class="menu-icon fa fa-caret-right"></i>';
		}

		$html  = str_repeat(\AppClass\App\ZWS::TAB,7).'<a id="pasta_a_'.$this->getCodigo().'" href="javascript:void(0);">'.\AppClass\App\ZWS::NL;
		$html .= str_repeat(\AppClass\App\ZWS::TAB,8).$menuIcone.'<span>'.trim($this->getNome()).'</span>'.\AppClass\App\ZWS::NL;
		$html .= str_repeat(\AppClass\App\ZWS::TAB,8).'<span class="pull-right"><i class="fa fa-angle-down"></i></span>'.\AppClass\App\ZWS::NL;
		$html .= str_repeat(\AppClass\App\ZWS::TAB,7).'</a>'.\AppClass\App\ZWS::NL;
		$html .= str_repeat(\AppClass\App\ZWS::TAB,7).'<ul>'.\AppClass\App\ZWS::NL;

		return $html;
	}
	
	/**
	 * Abrir/Inicia a tag html do item
	 * @param string $codigo
	 * @return string
	 */
	public function abrirTag() {
		return str_repeat(\AppClass\App\ZWS::TAB,0).'<li class="has_sub">'.\AppClass\App\ZWS::NL;
	}
	
	/**
	 * Fechar a tag do código html do item
	 * @param String $codigo
	 * @return string
	 */
	public function fecharTag() {
		$html = str_repeat(\AppClass\App\ZWS::TAB,7).'</ul>'.\AppClass\App\ZWS::NL;
		$html .= str_repeat(\AppClass\App\ZWS::TAB,6).'</li>'.\AppClass\App\ZWS::NL;
		return $html;

	}
	
	/**
	 * @return the $codigo
	 */
	public function getCodigo() {
		return $this->codigo;
	}

	/**
	 * @param field_type $codigo
	 */
	public function setCodigo($codigo) {
		$this->codigo = $codigo;
	}

	/**
	 * @return the $nome
	 */
	public function getNome() {
		return $this->nome;
	}

	/**
	 * @param field_type $nome
	 */
	public function setNome($nome) {
		$this->nome = $nome;
	}

	/**
	 * @return the $icone
	 */
	public function getIcone() {
		return $this->icone;
	}

	/**
	 * @param field_type $icone
	 */
	public function setIcone($icone) {
		$this->icone = $icone;
	}

	/**
	 * @return the $itemPai
	 */
	public function getItemPai() {
		return $this->itemPai;
	}

	/**
	 * @param field_type $itemPai
	 */
	public function setItemPai($itemPai) {
		$this->itemPai = $itemPai;
	}

	/**
	 * @return the $nivel
	 */
	public function getNivel() {
		return $this->nivel;
	}

	/**
	 * @param field_type $nivel
	 */
	public function setNivel($nivel) {
		$this->nivel = $nivel;
	}
	
}
