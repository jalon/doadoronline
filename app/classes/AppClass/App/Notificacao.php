<?php
namespace AppClass\App;

use \Zend\Mail;
use \Zend\Mail\Message;
use \Zend\Mime\Message as MimeMessage;
use \Zend\Mime\Part as MimePart;
Use \Zend\Mime;

/**
 * Implementação de notificações
 * 
 * @package: Notificacao
 * @Author: Jalon Vitor Cerqueira Silva
 * @version: 1.0.1
 * 
 */
class Notificacao {
	
	
	const TIPO_MENSAGEM_TEXTO 		= 'TX';
	const TIPO_MENSAGEM_HTML 		= 'H';
	const TIPO_MENSAGEM_TEMPLATE 	= 'TP';
	const TIPO_MENSAGEM_AUDIO 		= 'A';
	
	const TIPO_DEST_USUARIO 		= 'U';
	const TIPO_DEST_ORGANIZACAO		= 'O';
	const TIPO_DEST_ANONIMO			= 'A';
	
	const SOLICITACAO_SANGUE		= 'S';	
	const CONFIRMACAO_CADASTRO		= 'C';
	const ALERTA_APTIDAO			= 'A';
	const MENSAGEM					= 'M';

	private $indViaEmail;
	private $indViaWa;
	private $indViaSistema;
	
	private $assunto;
	private $nome;
	private $email;
	private $mensagem;
	private $codRemetente;
	private $codTemplate;
	private $codTipoDestinatario;
	private $codTipoMensagem;
	private $codSolicitacao;
	private $codTipoNotificacao;
	
	/**
	 * Array de variáveis / valores
	 * @var array
	 */
	private $variaveis;
	
	/**
	 * Array de usuários associados a notificação
	 * @var array
	 */
	private $usuarios;
	
	/**
	 * Array de organizacoes associadas a notificação
	 * @var array
	 */
	private $organizacoes;
	
	/**
	 * Array de pessoas associadas a notificação
	 * @var array
	 */
	private $pessoas;
	
	/**
	 * Array de Anexos
	 * @var array
	 */
	private $anexos;
	
	/**
	 * Construtor
	 *
	 * @return void
	 */
	
	/**
	 * Cria uma nova notificação
	 * @param string $tipoMensagem
	 * @param string $tipoDestinatario
	 */
	public function __construct($tipoMensagem,$tipoDestinatario,$tipoNotificacao) {
		global $db,$log;
		
		#################################################################################
		## Valida o tipo de Mensagem
		#################################################################################
		switch ($tipoMensagem) {
			case \AppClass\App\Notificacao::TIPO_MENSAGEM_AUDIO:
			case \AppClass\App\Notificacao::TIPO_MENSAGEM_HTML:
			case \AppClass\App\Notificacao::TIPO_MENSAGEM_TEMPLATE:
			case \AppClass\App\Notificacao::TIPO_MENSAGEM_TEXTO:
				break;
			default:
				throw new \Exception('Tipo de mensagem desconhecido');
				break;
		}
		
		#################################################################################
		## Valida o tipo de Destinatário
		#################################################################################
		switch ($tipoDestinatario) {
			case \AppClass\App\Notificacao::TIPO_DEST_USUARIO:
			case \AppClass\App\Notificacao::TIPO_DEST_ORGANIZACAO:
			case \AppClass\App\Notificacao::TIPO_DEST_ANONIMO:
				break;
			default:
				throw new \Exception('Tipo de destinatário desconhecido');
				break;
		}
		
		#################################################################################
		## Valida o tipo da Notificacao
		#################################################################################
		switch ($tipoNotificacao) {
			case \AppClass\App\Notificacao::SOLICITACAO_SANGUE:
			case \AppClass\App\Notificacao::CONFIRMACAO_CADASTRO:
			case \AppClass\App\Notificacao::ALERTA_APTIDAO:
			case \AppClass\App\Notificacao::MENSAGEM:
				break;
			default:
				throw new \Exception('Tipo de notificação desconhecido');
				break;
		}
		
		#################################################################################
		## Resgata os objetos do doctrine referente ao tipo de Mensagem e Destinatário
		#################################################################################
    	/*$oDest		= $em->getRepository('\Entidades\ZgappNotificacaoDestTipo')->findOneBy(array('codigo'=> $tipoDestinatario));
    	$oMen		= $em->getRepository('\Entidades\ZgappNotificacaoMensTipo')->findOneBy(array('codigo'=> $tipoMensagem));
    	*/
		
    	if (!$tipoDestinatario)	throw new \Exception('Tipo de destinatário não encontrado');
    	if (!$tipoMensagem)		throw new \Exception('Tipo de mensagem não encontrada');
    	if (!$tipoNotificacao)	throw new \Exception('Tipo de notificação não encontrada');
		
    	#################################################################################
    	## Salva os tipos
    	#################################################################################
    	$this->setCodTipoDestinatario($tipoDestinatario);
    	$this->setCodTipoMensagem($tipoMensagem);
    	$this->setCodTipoNotificacao($tipoNotificacao);
    	
    	#################################################################################
    	## Inicializa os arrays
    	#################################################################################
    	$this->variaveis	= array();
    	$this->usuarios		= array();
    	$this->organizacoes	= array();
    	$this->anexos		= array();
    	$this->pessoas		= array();
    	   
    	#################################################################################
    	## Por padrão a notificação é somente de sistema, ou seja, não envia e-mail nem wa
    	#################################################################################
    	$this->naoEnviaEmail();
    	$this->naoEnviaWa();
	}
	
	public function listarPendentes(){
		global $system,$db,$log;
		
		$oNotificacoes   = $db->extraiTodos('SELECT N.CODIGO, NU.CODIGO AS COD_NOTIF_USUARIO, NU.COD_USUARIO_ENVIOU, NU.COD_USUARIO_RECEBEU, N.ASSUNTO, N.MENSAGEM, N.COD_TIPO, N.DATA_CRIADA FROM `SLNOT_NOTIFICACAO_USUARIO` AS NU
				LEFT OUTER JOIN `SLNOT_NOTIFICACAO` N ON (NU.COD_NOTIFICACAO = N.CODIGO) 
				WHERE NU.COD_USUARIO_RECEBEU = :codUsuario AND NU.IND_LIDA = 0 AND N.IND_VIA_SISTEMA = 1',
					array(':codUsuario' => $system->getCodUsuario()));
		
		return $oNotificacoes;
	}
	
	public function listarTodas(){
		global $system,$db,$log;
	
		$oNotificacoes   = $db->extraiTodos('SELECT N.COD_TIPO, NU.CODIGO, NU.COD_USUARIO_ENVIOU, NU.IND_LIDA, N.COD_SOLICITACAO, N.ASSUNTO, N.MENSAGEM, N.DATA_CRIADA 
				FROM `SLNOT_NOTIFICACAO_USUARIO` AS NU
				LEFT OUTER JOIN `SLNOT_NOTIFICACAO` N ON (NU.COD_NOTIFICACAO = N.CODIGO)
				WHERE NU.COD_USUARIO_RECEBEU = :codUsuario',
				array(':codUsuario' => $system->getCodUsuario()));
	
		return $oNotificacoes;
	}

	/**
	 * Salvar a notificação no banco
	 * @throws \Exception
	 */
	public function salva() {
		global $db,$log;
		
		#################################################################################
		## Valida se os campos obrigatórios foram informados
		#################################################################################
		if ($this->getCodTipoMensagem() == \AppClass\App\Notificacao::TIPO_MENSAGEM_HTML || $this->getCodTipoMensagem() == \AppClass\App\Notificacao::TIPO_MENSAGEM_TEXTO) {
			if (!$this->getAssunto())	throw new \Exception('Assunto deve ser definido !!!');
			if (!$this->getMensagem())	throw new \Exception('Mensagem deve ser definida !!!');
		}elseif ($this->getCodTipoMensagem() == \AppClass\App\Notificacao::TIPO_MENSAGEM_TEMPLATE){
			if (!$this->getCodTemplate())	throw new \Exception('Template deve ser definido para esse tipo de notificação !!!');
		}
		
		#################################################################################
		## Validar alguns campos
		#################################################################################
		if (strlen($this->getAssunto() > 60)) $this->setAssunto(substr($this->getAssunto(),0,60));
		if (!$this->getCodRemetente()) $this->setCodRemetente(null);

		#################################################################################
		## Verificar se a notificação tem alguma via para o usuário
		#################################################################################
		if (!$this->getIndViaSistema() && !$this->getIndViaEmail() && !$this->getIndViaWa() ) {
			throw new \Exception('Pelo menos uma via deve ser informada !!!');
		}

		#################################################################################
		## Verificar se a notificação tem a via de e-mail, se o campo email estiver definido
		#################################################################################
		if ($this->getEmail() && !$this->getIndViaEmail())	throw new \Exception('E-mail só pode ser definido caso a notificação tenha a via de e-mail definida');
		
		#################################################################################
		## Verificar se a notificação tem um destinatário válido
		#################################################################################
		if (($this->getCodTipoDestinatario() == \AppClass\App\Notificacao::TIPO_DEST_USUARIO) && (sizeof($this->usuarios) == 0)) 
			throw new \Exception('Notificação de usuário deve ter pelo menos 1 usuário associado !!');
		if (($this->getCodTipoDestinatario() == \AppClass\App\Notificacao::TIPO_DEST_ORGANIZACAO) && (sizeof($this->organizacoes) == 0)) 
			throw new \Exception('Notificação de Organização deve ter pelo menos 1 organização associada !!');
		
		#################################################################################
		## Validar o tipo de destinatário com a via de envio
		#################################################################################
		if (($this->getCodTipoDestinatario() == \AppClass\App\Notificacao::TIPO_DEST_ANONIMO) && (!$this->getEmail()))
			throw new \Exception('Notificação Anônima deve ter o e-mail definido !!');
		
		#################################################################################
		## Ajusta os campos da via de notificação
		#################################################################################
		if (!$this->getIndViaEmail())	$this->naoEnviaEmail();
		if (!$this->getIndViaWa())		$this->naoEnviaWa();
		if (!$this->getIndViaSistema())	$this->naoEnviaSistema();
		
		/*#################################################################################
		## Calcular se será necessário processar (enviar a notificação)
		#################################################################################
		if ($this->getIndViaEmail() || $this->getIndViaWa()) {
			$this->setIndProcessada(0);
		}else{
			$this->setIndProcessada(1);
		}*/
		
		#################################################################################
		## Salva os campos
		#################################################################################
		/*$_not->setCodTipoDestinatario($this->getCodTipoDestinatario());
		//$_not->setCodTipoMensagem($this->getCodTipoMensagem());
		//$_not->setCodTemplate($this->getCodTemplate());
		$_not->setCodRemetente($this->getCodRemetente());
		//$_not->setData(new \DateTime("now"));
		//$_not->setIndViaEmail($this->getIndViaEmail());
		//$_not->setIndViaWa($this->getIndViaWa());
		//$_not->setIndViaSistema($this->getIndViaSistema());
		//$_not->setAssunto($this->getAssunto());
		//$_not->setMensagem($this->getMensagem());
		$_not->setIndProcessada($this->getIndProcessada());
		//$_not->setEmail($this->getEmail());
		//$_not->setNome($this->getNome());
		$em->persist($_not);
		*/
		
		/*#################################################################################
		## Salva as associações de usuários
		#################################################################################
		if (sizeof($this->usuarios) > 0) {
			foreach ($this->usuarios as $codUsuario => $oUsu) {
				$oAssUsu	= new \Entidades\ZgappNotificacaoUsuario();
				$oAssUsu->setCodNotificacao($_not);
				$oAssUsu->setCodUsuario($oUsu);
				$oAssUsu->setIndLida(0);
				$em->persist($oAssUsu);
			}
		}*/
		
		#################################################################################
		## Salva os anexos
		#################################################################################
		/*if (sizeof($this->anexos) > 0) {
			foreach ($this->anexos as $nome => $anexo) {
				$_notAnexo		= new \Entidades\ZgappNotificacaoAnexo();
				$_notAnexo->setCodNotificacao($_not);
				$_notAnexo->setNome($nome);
				$_notAnexo->setAnexo($anexo);
				$em->persist($_notAnexo);
			}
		}*/
		
		#################################################################################
		## Salva no banco
		#################################################################################
		
		try {
			
			//$db->transaction();
			
			$paramNot = array(':codTipo' => $this->getCodTipoNotificacao(),
					':codSolicitacao' => $this->getCodSolicitacao(),
					':codTipoDestinatario' => $this->getCodTipoDestinatario(),
					':codTipoMensagem' => $this->getCodTipoMensagem(),
					':assunto' => $this->getAssunto(),
					':mensagem' => $this->getMensagem(),
					':codTemplate' => $this->getCodTemplate(),
					':dataCriada' => date('Y-m-d H:m:s'),
					':indViaSistema' => $this->getIndViaSistema(),
					':indViaEmail' => $this->getIndViaEmail(),
					':indViaWA' => $this->getIndViaWa(),
					':nome' => $this->getNome(),
					':email' => $this->getEmail()
			);
			
			$oNotificacao	= $db->Executa('INSERT INTO `SLNOT_NOTIFICACAO`(`COD_TIPO`, `COD_SOLICITACAO`, `COD_TIPO_DESTINATARIO`, `COD_TIPO_MENSAGEM`, `ASSUNTO`, `MENSAGEM`, `COD_TEMPLATE`, `DATA_CRIADA`, `IND_VIA_SISTEMA`, `IND_VIA_EMAIL`, `IND_VIA_WA`, `NOME`, `EMAIL`)
 				VALUES (:codTipo,:codSolicitacao,:codTipoDestinatario,:codTipoMensagem,:assunto,:mensagem,:codTemplate,:dataCriada,:indViaSistema,:indViaEmail,:indViaWA,:nome,:email)', $paramNot);
			
			#################################################################################
			## Salva as variáveis VER DPS
			#################################################################################
			if (sizeof($this->variaveis) > 0) {
				foreach ($this->variaveis as $var => $valor) {
					$paramVar = array(':codNotificacao' => $oNotificacao,
							':variavel' => $var,
							':valor' => $valor
					);
						
					$oVar	= $db->Executa('INSERT INTO `SLNOT_NOTIFICACAO_VARIAVEL`(`COD_NOTIFICACAO`, `VARIAVEL`, `VALOR`)
 							VALUES (:codNotificacao,:variavel,:valor)', $paramVar);
				}
			}
			
			#################################################################################
			## Salva as associações de usuários
			#################################################################################
			if (sizeof($this->usuarios) > 0) {
				foreach ($this->usuarios as $codUsuario => $oUsu) {
					$paramNotUsu = array(':codNotificacao' => $oNotificacao,
							':codUsuarioEnviou' => $this->getCodRemetente(),
							':codUsuarioRecebeu' => $oUsu->CODIGO
					);
					
					$oNotificacaoUsu	= $db->Executa('INSERT INTO `SLNOT_NOTIFICACAO_USUARIO`(`COD_NOTIFICACAO`, `COD_USUARIO_ENVIOU`, `COD_USUARIO_RECEBEU`)
 							VALUES (:codNotificacao,:codUsuarioEnviou,:codUsuarioRecebeu)', $paramNotUsu);
				}
			}
			
			if ($this->getIndViaEmail()) $this->_notificaEmail($oNotificacao);
			
			//$db->commit();
		} catch (\Exception $e) {
			//$db->rollback();
			throw new \Exception($e->getMessage());
		}
		
	}
	
	/**
	 * Adiciona uma variável / Valor
	 * @param string $variavel
	 * @param string $valor
	 * @throws \Exception
	 */
	public function adicionaVariavel($variavel,$valor) {
		
		#################################################################################
		## Verifica se o tipo da Notificação é Template
		#################################################################################
		if (!$this->getCodTipoMensagem()) {
			throw new \Exception('Tipo de mensagem deve ser definida !!!');
		}elseif ($this->getCodTipoMensagem() != \AppClass\App\Notificacao::TIPO_MENSAGEM_TEMPLATE) {
			throw new \Exception('Tipo de mensagem deve ser \AppClass\App\Notificacao::TIPO_MENSAGEM_TEMPLATE !!!');
		}
		
		$this->variaveis[$variavel]		= $valor;
		
	}

	/**
	 * Associa a notificação a um usuário
	 * @param number $codUsuario
	 */
	public function associaUsuario($codUsuario) {
		global $db,$log;
		
		#################################################################################
		## Verifica se o tipo de destinatário da Notificação é Usuário
		#################################################################################
		if (!$this->getCodTipoDestinatario()) {
			throw new \Exception('Tipo de destinatário deve ser definido !!!');
		}elseif ($this->getCodTipoDestinatario() != \AppClass\App\Notificacao::TIPO_DEST_USUARIO) {
			throw new \Exception('Tipo de destinatário deve ser \AppClass\App\Notificacao::TIPO_DEST_USUARIO !!!');
		}
		
		#################################################################################
		## Só associar o usuário caso ainda não esteja
		#################################################################################
		if (!array_key_exists($codUsuario, $this->usuarios)) {
			//$oUsuario	= $em->getRepository('\Entidades\ZgsegUsuario')->findOneBy(array('codigo'=> $codUsuario));
			$oUsuario   = $db->extraiPrimeiro('SELECT * FROM `SLSEG_USUARIO` WHERE CODIGO = :codUsuario',
					array(':codUsuario' => $codUsuario));
			if (!$oUsuario)	throw new \Exception('Usuário não encontrado !!!');
			$this->usuarios[$codUsuario]	= $oUsuario;
		}
		
	}
	
	/**
	 * Anexar um arquivo a notificação
	 * @param string $nome
	 * @param blob $conteudo
	 * @throws \Exception
	 */
	public function anexarArquivo($nome,$conteudo) {
		global $db;
		
		#################################################################################
		## Adicionar o arquivo no array de anexos, verificar se o nome já foi anexado
		#################################################################################
		if (!array_key_exists($nome, $this->anexos)) {
			$this->anexos[$nome]		= $conteudo;
		}else{
			throw new \Exception('Nome de anexo já utilizado !!');
		}
		
	}
	
	/**
	 * Lista as notificações ainda não enviadas
	 */
	public static function listaNaoEnviadas() {
		global $db;
	
		$qb 	= $em->createQueryBuilder();
		try {
			$qb->select('n')
			->from('\Entidades\ZgappNotificacao','n')
			->where($qb->expr()->andX(
				$qb->expr()->eq('n.indProcessada'	, ':processada')
			))
			->orderBy('n.data','ASC')
			->setParameter('processada'	, 0);
				
			$query 		= $qb->getQuery();
			return		($query->getResult());
		} catch (\Exception $e) {
			\AppClass\App\Erro::halt($e->getMessage());
		}
	}
	
	
	/**
	 * Seta a flag de lida na notificação
	 * @param number $codNotificacao
	 * @param number $codUsuario
	 */
	public static function ler($codNotificacao,$codUsuarioRecebido) {
		global $db;
		
		#################################################################################
		## Verifica se a notificação existe
		#################################################################################
		//$oNot		= $em->getRepository('\Entidades\ZgappNotificacao')->findOneBy(array('codigo' => $codNotificacao));
		$oNot   = $db->extraiPrimeiro('SELECT * FROM `SLREC_SOLICITACAO` WHERE CODIGO = :codigo',
					array(':codigo' => $codNotificacao));
		if (!$oNot) exit;
		
		#################################################################################
		## Resgata o objeto do usuário
		#################################################################################
		//$oUsu		= $em->getRepository('\Entidades\ZgsegUsuario')->findOneBy(array('codigo' => $codUsuario));
		$oUsu   = $db->extraiPrimeiro('SELECT * FROM `SLSEG_USUARIO` WHERE CODIGO = :codigo',
					array(':codigo' => $codUsuarioRecebido));
		if (!$oUsu) exit;
		
		#################################################################################
		## Resgata o registro de leitura
		#################################################################################
		$oNotUsu   = $db->extraiPrimeiro('SELECT * FROM `SLNOT_NOTIFICACAO_USUARIO` WHERE COD_NOTIFICACAO = :codNotificacao AND COD_USUARIO_RECEBIDO = :codUsuarioRecebido',
					array(':codNotificacao' => $codNotificacao,':codUsuarioRecebido' => $codUsuarioRecebido));
		
		#################################################################################
		## Salva no banco
		#################################################################################
		try {
			$oNotUsu	= $db->Executa('INSERT INTO `SLNOT_NOTIFICACAO_USUARIO`(`IND_LIDA`, `DATA_LIDA`) 
 				VALUES (:indLida, :dataLida)', array(':indLida' => 1, ':dataLida' => date('Y-m-d H:m:s') ));
			
			//if (stristr($oDoacao,'[ERR]')) throw new Exception();
			
		} catch (\Exception $e) {
			throw new \Exception($e->getMessage());
		}
		
	}
	
	/**
	 * Verifica se uma notificação tem anexo
	 * @param number $codNotificacao
	 */
	public static function temAnexo($codNotificacao) {
		global $db;
	
		$qb 	= $em->createQueryBuilder();
		try {
			$qb->select('count(n.codigo)')
			->from('\Entidades\ZgappNotificacaoAnexo','n')
			->where($qb->expr()->andX(
				$qb->expr()->eq('n.codNotificacao'	, ':codNotificacao')
			))
			->setParameter('codNotificacao'	, $codNotificacao);
				
			$query 		= $qb->getQuery();
			$num		= $query->getSingleScalarResult();
			
			if ($num > 0)	{
				return true;
			}else{
				return false;
			}
			
		} catch (\Exception $e) {
			\AppClass\App\Erro::halt($e->getMessage());
		}
	}
	
	/**
	 * Enviar os e-mails de uma notificação
	 * @param \Entidades\ZgappNotificacao $notificacao
	 */
	public static function _notificaEmail($codNotificacao) {
		#################################################################################
		## Variáveis globais
		#################################################################################
		global $db,$tr,$log,$system;
		
		#################################################################################
		## Resgata a notificação
		#################################################################################
		//$notificacao		= $em->getRepository('\Entidades\ZgappNotificacao')->findOneBy(array('codigo' => $codNotificacao)); 
		$notificacao   = $db->extraiPrimeiro('SELECT * FROM `SLNOT_NOTIFICACAO` WHERE CODIGO = :codNotificacao',
				array(':codNotificacao' => $codNotificacao));
		if (!$notificacao)	{
			throw new \Exception('Notificação não encontrada');
		}
		
		#################################################################################
		## Verifica se a notificação tem a flag de enviar e-mail
		#################################################################################
		if (!$notificacao->IND_VIA_EMAIL)	{
			throw new \Exception('Notificação não está com a flag de envio de e-mail');
		}
		
		#################################################################################
		## Resgatar a mensagem da notificação
		#################################################################################
		$mensagem		= self::_getMensagem($codNotificacao);
		$codTipoDest	= $notificacao->COD_TIPO_DESTINATARIO;
		$assunto		= $notificacao->ASSUNTO;
		
		#################################################################################
		## Criar os objeto do email ,transporte e validador
		#################################################################################
		#$mail 			= \AppClass\App\Mail::getMail();
		#$transport 		= \AppClass\App\Mail::getTransport();
		$validator 		= new \Zend\Validator\EmailAddress();
		$htmlMail 		= new MimePart($mensagem);
		$htmlMail->type = "text/html";
		$body 			= new MimeMessage();
		$bodyArray		= array();
		$bodyArray[]	= $htmlMail;
		
		## IMPORT PHPMAILER ##
		#$mail = new \AppClass\Mail\Mail();
		require (CLASS_PATH. "/AppClass/Mail/class.phpmailer.php");
		
		$mail = new \PHPMailer();
		if (!$mail /*|| !$transport */) return 0;
		
		#################################################################################
		## Decriptar a senha
		#################################################################################
		$crypt				= new \AppClass\App\Crypt();
		$senhaSmtp			= $crypt->decrypt($system->config["mail"]["senhaSmtp"]);
		
		$mail->IsSMTP();
		$mail->CharSet 		= $system->config["mail"]["charset"];
		$mail->Host 		= $system->config["mail"]["servidorSmtp"];
		$mail->SMTPAuth		= $system->config["mail"]["smtpAuth"];
		$mail->Port 		= $system->config["mail"]["portaSmtp"];
		$mail->SMTPSecure 	= $system->config["mail"]["tipoCriptSmtp"];
		$mail->From			= $system->config["mail"]["usuarioSmtp"];
		$mail->FromName		= "Doador Online";
		$mail->Username		= $system->config["mail"]["usuarioSmtp"];
		$mail->Password		= $senhaSmtp;
		
		#################################################################################
		## Colocar os anexos, caso existam
		#################################################################################
		/*$anexos		= $em->getRepository('\Entidades\ZgappNotificacaoAnexo')->findBy(array('codNotificacao' => $notificacao->getCodigo()));
		for ($a = 0; $a < sizeof($anexos); $a++) {
			$attachment 				= new Mime\Part($anexos[$a]->getAnexo());
			$attachment->type 			= 'application/octet-stream';
			$attachment->filename 		= $anexos[$a]->getNome();
			$attachment->disposition 	= Mime\Mime::DISPOSITION_ATTACHMENT;
			$attachment->encoding 		= Mime\Mime::ENCODING_BASE64;
			$bodyArray[]	= $attachment;
		}*/
		
		#################################################################################
		## Definir o conteúdo do e-mail
		#################################################################################
		#$body->setParts($bodyArray);
		#$mail->setBody($body);
		#$mail->setSubject("<DoadorOnline> ".$assunto);
		
		$mail->isHTML(true);
		$mail->Subject = /*"<DoadorOnline> ".*/$assunto;
		$mail->Body = $mensagem;
		
		#################################################################################
		## Controlar a quantidade de emails a enviar
		#################################################################################
		$numEmails	= 0;
		
		#################################################################################
		## Verificar o tipo de destinatário
		#################################################################################
		if ($codTipoDest	== \AppClass\App\Notificacao::TIPO_DEST_ANONIMO) {
			$destinatarios	= array();
			$destinatarios[0]["NOME"]	= $notificacao->NOME;
			$destinatarios[0]["EMAIL"]	= $notificacao->EMAIL;
		}else{
			$destinatarios  = $db->extraiTodos('SELECT SU.*, U.CODIGO AS COD_USUARIO, U.USUARIO, P.NOME AS NOME_USUARIO FROM `SLNOT_NOTIFICACAO_USUARIO` AS SU
								LEFT OUTER JOIN `SLSEG_USUARIO` U ON (SU.COD_USUARIO_RECEBEU = U.CODIGO)
								LEFT OUTER JOIN `SLSEG_PESSOA` P ON (U.COD_PESSOA = P.CODIGO)
									WHERE SU.COD_NOTIFICACAO = :codNotificacao AND U.COD_STATUS = 1',
					array(':codNotificacao' => $notificacao->CODIGO));
		}

		#################################################################################
		## Verifica se esse tipo de notificação já foi enviada e Cria o log de envio
		#################################################################################
		/*$mailLog		= $em->getRepository('\Entidades\ZgappNotificacaoLog')->findOneBy(array('codNotificacao' => $notificacao->getCodigo(),'codFormaEnvio' => "E"));
		if (!$mailLog)	{
			$mailLog		= new \Entidades\ZgappNotificacaoLog();
			$oFormaEnvio	= $em->getReference('\Entidades\ZgappNotificacaoFormaEnvio', "E");
			$mailLog->setCodFormaEnvio($oFormaEnvio);
			$mailLog->setCodNotificacao($notificacao);
		}else{
			if ($mailLog->getIndProcessada() == 1) {
				$log->info("Notificação (".$notificacao->getCodigo().") já foi processado o envio dos e-mails");
				return 1;
			}
		}

		#################################################################################
		## Array para controle de registro de envio por destinatário
		#################################################################################
		$logDest	= array();
		*/
		
		#################################################################################
		## Resgata a lista de usuários/Pessoas que receberão a notificação
		#################################################################################
		for ($j = 0; $j < sizeof($destinatarios); $j++) {
		
			if ($codTipoDest	== \AppClass\App\Notificacao::TIPO_DEST_ANONIMO) {
				$nomeDest		= $destinatarios[$j]["NOME"];
				$codDest		= $destinatarios[$j]["EMAIL"];
				$emailDest		= $destinatarios[$j]["EMAIL"];
				$campoDest		= "email";
			}else{
				$nomeDest		= $destinatarios[$j]->NOME_USUARIO;
				$codDest		= $destinatarios[$j]->COD_USUARIO;
				$emailDest		= $destinatarios[$j]->USUARIO;
				$campoDest		= "codUsuario";
			}
		
			$log->debug("Usuario/Pessoa que será notificado[a]: ".$nomeDest." - Email: ".$emailDest);
		
			#################################################################################
			## Valida o e-mail
			#################################################################################
			if (!$validator->isValid($emailDest)) {
				continue;
			}
			
			#################################################################################
			## Verifica se a notificação já foi enviada para esse destinatário
			#################################################################################
			/*$indEnvia		= 1;
			$logDest[$codDest]	= $em->getRepository('\Entidades\ZgappNotificacaoLogDest')->findOneBy(array('codLog' => $mailLog->getCodigo(), $campoDest => $codDest));
			if (!$logDest[$codDest]) {
				$logDest[$codDest]	= new \Entidades\ZgappNotificacaoLogDest();
			}else{
				if (!$logDest[$codDest]->getIndErro()) {
					$indEnvia		= 0;
				}elseif ($logDest[$codDest]->getIndErro() > 4){
					$indEnvia		= 0;
				}
			}*/

			if ($emailDest) {
					
				#################################################################################
				## Associa os destinatários
				#################################################################################
				/*if ($codTipoDest	== \AppClass\App\Notificacao::TIPO_DEST_USUARIO) {
					$oUsu			= $em->getReference('\Entidades\ZgsegUsuario', $codDest);
					$logDest[$codDest]->setCodUsuario($oUsu);
				}else{
					$logDest[$codDest]->setEmail($emailDest);
				}
	
				$logDest[$codDest]->setCodLog($mailLog);
				$logDest[$codDest]->setDataEnvio(new \DateTime("now"));
				$logDest[$codDest]->setIndErro(0);
				$logDest[$codDest]->setErro(null);*/
				
				#################################################################################
				## Definir os destinatários
				#################################################################################
				if (sizeof($destinatarios) > 1) {
					$mail->addBcc($emailDest);
					if ($notificacao->EMAIL && ($validator->isValid($notificacao->EMAIL)) ) $mail->addBcc($notificacao->EMAIL);
				}else{
					$mail->addAddress($emailDest);
					#$mail->addTo($emailDest);
					if ($notificacao->EMAIL && ($validator->isValid($notificacao->EMAIL)) ) $mail->addCc($notificacao->EMAIL);
				}
				
				$numEmails++;
			}
		}
		
		#################################################################################
		## Salvar as informações e enviar o e-mail
		#################################################################################
		if ($numEmails > 0) {
			try {
				$indOK		= true;
				$log->debug("Vou enviar o e-mail");
				#$transport->send($mail);
				if(!$mail->send()){
					$log->debug( "Erro ao enviar o e-mail: " . $mail->ErrorInfo );
				}else{
					$log->debug( "E-mail enviado com sucesso !!!" );
				}
				$erro		= null;
					
			} catch (\Exception $e) {
				$log->err("Erro ao enviar o e-mail:". $e->getTraceAsString());
				$indOK		= false;
				$erro		= $e->getTraceAsString();
			}
			
			try {
					
				$indProcessada		= ($indOK == true) ? 1 : 0;
				$indErro			= ($indOK == true) ? 0 : 1;
				
				//$mailLog->setIndProcessada($indProcessada);
				
				#################################################################################
				## Atualizar o controle de log por destinatário
				#################################################################################
				//$em->persist($mailLog);
				/*foreach ($logDest as $codDest => $aLogDest) {
					$aLogDest->setDataEnvio(new \DateTime("now"));
					$aLogDest->setErro($erro);
					$aLogDest->setIndErro($indErro);
					$em->persist($aLogDest);
				}
				
				$em->flush();
				$em->clear();*/
			
			} catch (\Exception $e) {
				return 0;
			}
			
			if ($indOK) {
				return 1;
			}else{
				return 0;
			}
		}
		
		return 1;
	}
	
	/**
	 * Enviar as mensagens Whatsapp de uma notificação
	 * @param \Entidades\ZgappNotificacao $notificacao
	 */
	public static function _notificaWa($codNotificacao) {
	
		#################################################################################
		## Variáveis globais
		#################################################################################
		global $db,$tr,$log,$system;
	
		#################################################################################
		## Resgata a notificação
		#################################################################################
		$notificacao		= $em->getRepository('\Entidades\ZgappNotificacao')->findOneBy(array('codigo' => $codNotificacao));
		if (!$notificacao)	{
			throw new \Exception('Notificação não encontrada');
		}
		
		#################################################################################
		## Verifica se a notificação tem a flag de enviar e-mail
		#################################################################################
		if (!$notificacao->getIndViaWa())	{
			throw new \Exception('Notificação não está com a flag de envio de whatsapp');
		}
		
		$log->debug("Envia wa para notificacao: ".$notificacao->getAssunto());
			
		#################################################################################
		## Não enviar templates via whatsapp
		#################################################################################
		if ($notificacao->getCodTipoMensagem() == "TP") {
			return 1;
		}
		
		#################################################################################
		## Resgatar a mensagem da notificação
		#################################################################################
		$mensagem		= self::_getMensagem($codNotificacao);
		$codTipoDest	= $notificacao->getCodTipoDestinatario()->getCodigo();
		
		#################################################################################
		## Verificar o tipo de destinatário
		#################################################################################
		if ($codTipoDest	== \AppClass\App\Notificacao::TIPO_DEST_ANONIMO) {
			/** não enviar whatsapp para o tipo de notificação anônima **/
			return 1;
		}else{
			$destinatarios	= $em->getRepository('\Entidades\ZgappNotificacaoUsuario')->findBy(array('codNotificacao' => $notificacao->getCodigo()));
		}

		#################################################################################
		## Cascade da notificação
		#################################################################################
		$em->persist($notificacao);
		
		#################################################################################
		## Verifica se esse tipo de notificação já foi enviada e Cria o log de envio
		#################################################################################
		$waLog		= $em->getRepository('\Entidades\ZgappNotificacaoLog')->findOneBy(array('codNotificacao' => $notificacao->getCodigo(),'codFormaEnvio' => "W"));
		if (!$waLog)	{
			$waLog		= new \Entidades\ZgappNotificacaoLog();
			$oFormaEnvio	= $em->getReference('\Entidades\ZgappNotificacaoFormaEnvio', "W");
			$waLog->setCodFormaEnvio($oFormaEnvio);
			$waLog->setCodNotificacao($notificacao);
		}else{
			if ($waLog->getIndProcessada() == 1) {
				$log->info("Notificação (".$notificacao->getCodigo().") já foi processado o envio das mensagens whatsapp");
				return 1;
			}
		}
		
		#################################################################################
		## Faz o controle de execução 
		#################################################################################
		$indOK		= true;
		
		#################################################################################
		## Inicializa o array de Chips, que serão usados para enviar as mensagens
		#################################################################################
		$chips 			= array();
		
		#################################################################################
		## Resgata a lista de usuários/Pessoas que receberão a notificação
		#################################################################################
		for ($j = 0; $j < sizeof($destinatarios); $j++) {
		
			if ($codTipoDest	== \AppClass\App\Notificacao::TIPO_DEST_PESSOA) {
				$nomeDest		= $destinatarios[$j]->getCodPessoa()->getNome();
				$codDest		= $destinatarios[$j]->getCodPessoa()->getCodigo();
				$campoDest		= "codPessoa";
			}else{
				$nomeDest		= $destinatarios[$j]->getCodUsuario()->getNome();
				$codDest		= $destinatarios[$j]->getCodUsuario()->getCodigo();
				$campoDest		= "codUsuario";
			}
		
			$log->debug("Usuario/Pessoa que será notificado[a]: ".$nomeDest);

			#################################################################################
			## Verifica se a notificação já foi enviada para esse destinatário
			#################################################################################
			$indEnvia		= 1;
			$logDest		= $em->getRepository('\Entidades\ZgappNotificacaoLogDest')->findOneBy(array('codLog' => $waLog->getCodigo(), $campoDest => $codDest));
			if (!$logDest) {
				$logDest	= new \Entidades\ZgappNotificacaoLogDest();
			}else{
				if (!$logDest->getIndErro()) {
					$indEnvia		= 0;
				}elseif ($logDest->getIndErro() > 4){
					$indEnvia		= 0;
				}
			}
			
				
			if ($indEnvia) {
			
				#################################################################################
				## Associa os destinatários
				#################################################################################
				if ($codTipoDest	== \AppClass\App\Notificacao::TIPO_DEST_PESSOA) {
					$oPessoa			= $em->getReference('\Entidades\ZgfinPessoa', $codDest);
					$logDest->setCodPessoa($oPessoa);
				}elseif ($codTipoDest	== \AppClass\App\Notificacao::TIPO_DEST_USUARIO) {
					$oUsu			= $em->getReference('\Entidades\ZgsegUsuario', $codDest);
					$logDest->setCodUsuario($oUsu);
				}
				
				$logDest->setCodLog($waLog);
				
				#################################################################################
				## Busca o Chip que a mensagem será enviada
				#################################################################################
				$c	= \AppClass\Wap\Chip::buscaChipUsuario($codDest);
			
				#################################################################################
				## Caso não tenha chips disponíveis, não tentar enviar a mensagem
				#################################################################################
				if (!$c) {
					$indOK	= false;
					$logDest->setDataEnvio(new \DateTime("now"));
					$logDest->setErro("Não encontramos chips disponíveis para enviar a mensagem");
					$logDest->setIndErro($logDest->getIndErro() + 1);
					$em->persist($logDest);
					continue;
				}
			
				$log->debug("Chip selecionado: ".$c->getLogin());
			
				#################################################################################
				## Instancia a classe para envio
				#################################################################################
				$chip		= new \AppClass\Wap\Chip();
				$chip->_setCodigo($c->getCodigo());
				
				#################################################################################
				## Converte o número do celular para o formato do whatsapp
				#################################################################################
				$celulares	= \AppClass\Wap\Chip::buscaNumeroComWa($codDest);
				if (!$celulares || sizeof($celulares) ==  0)	continue;
			
				try {
						
					if (!isset($chips[$c->getCodigo()])) {
						$log->debug("Tentando conexão com WA!!! ");
						$chip->conectar();
						$log->debug("Conexão ao wa feita com sucesso !!! ");
			
						$chips[$c->getCodigo()]	= $chip;
			
					}
						
					for ($n = 0; $n < sizeof($celulares); $n++) {
						$log->debug("Convertendo o número : ".$celulares[$n]->getTelefone());
						//$waNumber	= $chip->_convertCellToWaNumber($celulares[$n]->getTelefone());
						$waNumber	= $celulares[$n]->getWaLogin();
						$log->debug("Enviando wa para o número: ".$waNumber);
						$ret	= $chips[$c->getCodigo()]->w->sendMessage($waNumber, $mensagem);
						//$log->debug("Retorno do envio: ".$ret);
					}

					$logDest->setDataEnvio(new \DateTime("now"));
					$logDest->setErro(null);
					$logDest->setIndErro(0);
					$em->persist($logDest);
						
				} catch (\Exception $e) {
					$log->err("Mensagem wa não enviada, por problema no chip: ".$chip->getLogin()." -> ". $e->getMessage());
					$logDest->setDataEnvio(new \DateTime("now"));
					$logDest->setErro($e->getTraceAsString());
					$logDest->setIndErro($logDest->getIndErro() + 1);
					$em->persist($logDest);
					$indOK	= false;
				}
			}
		}

		try {
			
			$indProcessada		= ($indOK == true) ? 1 : 0;
			$waLog->setIndProcessada($indProcessada);
			
			$em->persist($waLog);
			$em->flush();
			$em->clear();
		
		} catch (\Exception $e) {
			return 0;
		}
		
		if ($indOK) {
			return 1;
		}else{
			return 0;
		}
	
	}
	
	/**
	 * Retorna a mensagem associada a notificação
	 * @param \Entidades\ZgappNotificacao $notificacao
	 */
	public static function _getMensagem($codNotificacao) {
		
		#################################################################################
		## Variáveis globais
		#################################################################################
		global $db,$system;
		
		#################################################################################
		## Resgata a notificação
		#################################################################################
		$notificacao  = $db->extraiPrimeiro('SELECT N.*, T.CAMINHO FROM `SLNOT_NOTIFICACAO` AS N
						LEFT OUTER JOIN `SLNOT_NOTIFICACAO_TEMPLATE` T ON (N.COD_TEMPLATE = T.CODIGO)
							WHERE N.CODIGO = :codigo',
				array(':codigo' => $codNotificacao));
		//$notificacao		= $em->getRepository('\Entidades\ZgappNotificacao')->findOneBy(array('codigo' => $codNotificacao));
		if (!$notificacao)	{
			throw new \Exception('Notificação não encontrada');
		}
		
		#################################################################################
		## Monta a mensagem
		#################################################################################
		$codTipoMens		= $notificacao->COD_TIPO_MENSAGEM;
		
		if ($codTipoMens	== \AppClass\App\Notificacao::TIPO_MENSAGEM_TEXTO) {
			$mensagem		= $notificacao->getMensagem();
		}elseif ($codTipoMens	== \AppClass\App\Notificacao::TIPO_MENSAGEM_HTML) {
			$mensagem		= $notificacao->getMensagem();
		}elseif ($codTipoMens	== \AppClass\App\Notificacao::TIPO_MENSAGEM_TEMPLATE) {
		
			#################################################################################
			## Verificar se o template foi informado
			#################################################################################
			if (!$notificacao->COD_TEMPLATE) return null;
		
			#################################################################################
			## Resgata as informações do template
			#################################################################################
			$template		= $notificacao->COD_TEMPLATE;
		
			#################################################################################
			## Verificar se o template existe
			#################################################################################
			if (!file_exists(TPL_PATH . '/' . $notificacao->CAMINHO)) return null;
		
			#################################################################################
			## Carregando o template html
			#################################################################################
			$tpl	= new \AppClass\App\Template();
			$tpl->load(TPL_PATH . '/' . $notificacao->CAMINHO);
		
			#################################################################################
			## Atribui as variáveis do template
			#################################################################################
			$variaveis  = $db->extraiTodos('SELECT * FROM `SLNOT_NOTIFICACAO_VARIAVEL` WHERE COD_NOTIFICACAO = :codNotificacao',
					array(':codNotificacao' => $notificacao->CODIGO));
			//$variaveis		= $em->getRepository('\Entidades\ZgappNotificacaoVariavel')->findBy(array('codNotificacao' => $notificacao->getCodigo()));
			for ($v = 0; $v < sizeof($variaveis); $v++) {
				$tpl->set($variaveis[$v]->VARIAVEL, $variaveis[$v]->VALOR);
			}
		
			#################################################################################
			## Por fim exibir a página HTML
			#################################################################################
			$mensagem	= $tpl->getHtml();
		
		}else{
			return null;
		}

		return $mensagem;
		
	}
	
	/**
	 * Define a flag para enviar e-mail
	 */
	public function enviaEmail() {
		$this->indViaEmail = 1;
	}
	
	/**
	 * Define a flag para enviar Whatsapp
	 */
	public function enviaWa() {
		$this->indViaWa = 1;
	}
	
	/**
	 * Define a flag para enviar para o sistema
	 */
	public function enviaSistema() {
		$this->indViaSistema = 1;
	}
	
	/**
	 * Define a flag para não enviar e-mail
	 */
	public function naoEnviaEmail() {
		$this->indViaEmail = 0;
	}
	
	/**
	 * Define a flag para não enviar Whatsapp
	 */
	public function naoEnviaWa() {
		$this->indViaWa = 0;
	}
	
	/**
	 * Define a flag para não enviar para o sistema
	 */
	public function naoEnviaSistema() {
		$this->indViaSistema = 0;
	}
	
	public function setAssunto($assunto) {
		$this->assunto = $assunto;
	}
	
	public function getAssunto() {
		return $this->assunto;
	}
	
	public function setMensagem($mensagem) {
		$this->mensagem = $mensagem;
	}
	
	public function getMensagem() {
		return $this->mensagem;
	}
	
	public function setEmail($email) {
		$this->email = $email;
	}
	
	public function getEmail() {
		return $this->email;
	}
	
	public function setNome($nome) {
		$this->nome = $nome;
	}
	
	public function getNome() {
		return $this->nome;
	}
	
	public function setCodRemetente($codRemetente) {
		$this->codRemetente = $codRemetente;
	}
	
	public function getCodRemetente() {
		return $this->codRemetente;
	}
	
	public function setCodTemplate($codTemplate) {
		$this->codTemplate = $codTemplate;
	}
	
	public function getCodTemplate() {
		return $this->codTemplate;
	}
	
	public function setCodTipoDestinatario($codTipoDestinatario) {
		$this->codTipoDestinatario = $codTipoDestinatario;
	}
	
	public function getCodTipoDestinatario() {
		return $this->codTipoDestinatario;
	}
	
	public function setCodTipoMensagem($codTipoMensagem) {
		$this->codTipoMensagem = $codTipoMensagem;
	}
	
	public function getCodTipoMensagem() {
		return $this->codTipoMensagem;
	}
	
	public function setCodSolicitacao($codSolicitacao) {
		$this->codSolicitacao = $codSolicitacao;
	}
	
	public function getCodSolicitacao() {
		return $this->codSolicitacao;
	}
	
	public function setCodTipoNotificacao($codTipoNotificacao) {
		$this->codTipoNotificacao = $codTipoNotificacao;
	}
	
	public function getCodTipoNotificacao() {
		return $this->codTipoNotificacao;
	}
	
	public function setIndViaSistema($indViaSistema) {
		$this->indViaSistema = $indViaSistema;
	}
	
	public function getIndViaSistema() {
		return $this->indViaSistema;
	}
	
	public function setIndViaEmail($indViaEmail) {
		$this->indViaEmail = $indViaEmail;
	}
	
	public function getIndViaEmail() {
		return $this->indViaEmail;
	}
	
	public function setIndViaWa($indViaWa) {
		$this->indViaWa = $indViaWa;
	}
	
	public function getIndViaWa() {
		return $this->indViaWa;
	}
}