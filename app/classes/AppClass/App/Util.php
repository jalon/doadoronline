<?php
namespace AppClass\App;
/**
 * Fun��es diversas
 *
 * @package \App\Util
 * @author Jalon V�tor Cerqueira Silva
 * @version 1.0.1
 * @created 17/07/2013
 */
class Util {
	
	/**
	 * Construtor privado, a classe deve ser usada de forma statica
	 */
	private function __construct() {
		
	}
	
	/**
	 * Resgatar o conte�do de um arquivo
	 * @param string $arquivo
	 * @return string 
	 */
	public static function getConteudoArquivo ($arquivo) {
		/** Checar se o arquivo existe **/
		if (file_exists($arquivo)) {
			try {
				/** Abre o arquivo somente para leitura **/
				$handle         = fopen($arquivo, "r");
	
				/** L� o conteudo do arquivo em uma variavel **/
				$conteudo       = fread ($handle, filesize ($arquivo));
	
				/** Fecha o arquivo **/
				fclose($handle);
	
				return($conteudo);
	
			} catch (\Exception $e) {
				\AppClass\App\Erro::halt('C�digo do Erro: "getConteudoArquivo": '.$e->getMessage());
			}
		}else{
			return null;
		}
	}
	
	/**
	 *
	 * Resgatar o caminho completo do arquivo por extensão
	 * @param string $arquivo
	 * @param string $extensao
	 * @param string $tipo
	 */
	public static function getCaminhoCorrespondente($caminho,$extensao,$tipo = \AppClass\App\ZWS::CAMINHO_ABSOLUTO) {
		global $log;
	
		/** define o tipo padrão **/
		if (!$tipo)     $tipo   = \AppClass\App\ZWS::CAMINHO_ABSOLUTO;
	
		/** Verifica se o caminho passado é uma URL ou um caminho **/
		if ($tipo == \AppClass\App\ZWS::CAMINHO_ABSOLUTO) {
			/** Caminho absoluto **/
			//$tipo   	= \AppClass\App\ZWS::CAMINHO_ABSOLUTO;
	
			/** Resgata o nome/dir base do arquivo **/
			$base   	= pathinfo($caminho,PATHINFO_BASENAME);
			$baseDir	= realpath(pathinfo($caminho,PATHINFO_DIRNAME)."/../");
	
			/** Resgata o nome do arquivo sem a extensão **/
			$base  		= substr($base,0,strpos($base,'.'));
	
		} else {
			/** Url **/
			//$tipo   = \AppClass\App\ZWS::CAMINHO_RELATIVO;
				
			/** Resgata o nome/dir base do arquivo **/
			$base		= pathinfo($caminho,PATHINFO_BASENAME);
			$dir		= pathinfo($caminho,PATHINFO_DIRNAME);
			$baseDir	= dirname($dir);
			$baseDir	= str_replace('\\',"/",$baseDir);
			$modPath	= str_replace('//',"/",MOD_PATH);
			$modPath	= str_replace('\\',"/",$modPath);
			$modulo		= str_replace($modPath,"",$baseDir);
			$baseDir	= ROOT_URL . "/$modulo/";
	
			/** Resgata o nome do arquivo sem a extensão **/
			$base   = substr($base,0,strpos($base,'.'));
		}
	
		//echo "Caminho: $caminho, Base: $base, Dir: $dir, BaseDir: $baseDir,ModPath: $modPath, Modulo: $modulo<br>";
	
	
		switch (strtolower($extensao)) {
			case \AppClass\App\ZWS::EXT_HTML:
				$dir	= "html";
				break;
			case \AppClass\App\ZWS::EXT_DP:
				$dir	= "dp";
				break;
			case \AppClass\App\ZWS::EXT_XML:
				$dir	= "xml";
				break;
			case \AppClass\App\ZWS::EXT_PHP:
				$dir	= "php";
				break;
			default:
				return ($arquivo);
				break;
		}
	
		if ($tipo == \AppClass\App\ZWS::CAMINHO_RELATIVO) {
			return ($baseDir . '/' . $base . "." . $extensao);
		}else{
			return ($baseDir . '/' . $dir . '/' . $base . "." . $extensao);
		}
	}
	
	/**
	 * Codifica uma string
	 * @param string $string
	 */
	public static function encodeUrl($string) {
		return(base64_encode($string));
	}
	
	/**
	 * Codifica uma string
	 * @param string $string
	 */
	public static function decodeUrl($string) {
		return(base64_decode($string));
	}
	
	/**
	 * Descompacta um id
	 * @param string $id
	 */
	public static function descompactaId($id) {
		if ($id != null) {
			$var    = base64_decode($id);
			$vars   = explode("&",$var);
			for ($i = 0; $i < sizeof($vars); $i++) {
				if ($vars[$i] != '') {
					list($variavel,$valor)  = explode('=',$vars[$i]);
					eval('global $'.$variavel.';');
					eval('$'.$variavel.' = "'.$valor.'";');
				}
			}
		}
	}
	
	/**
	 * Implementa��o de Anti inje��o de SQL
	 * @param string $string
	 * @return string
	 */
	public static function antiInjection($string) {
		
		if (is_array($string)) return $string;
	
		/** remove palavras que contenham sintaxe sql **/
		$string = preg_replace("/(from|select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/i","",$string);
	
		/** limpa espa�os vazio **/
		$string = trim($string);
	
		/** tira tags html e php **/
		$string = strip_tags($string);//
	
		/** Converte caracteres especiais para a realidade HTML **/
		$string = htmlspecialchars($string);
	
		if (!get_magic_quotes_gpc()) {
			$string = addslashes($string);
		}
	
		return ($string);
	}
	
	public static function formatData($formato, $data) {
	
		$format_data = ($data) ? date($formato, strtotime($data))  : "00/00/0000";
	
		return ($format_data);
	}
	
	/**
	 * Validador
	 * @see \Zend\Validator\ValidatorInterface::isValid()
	 */
	public static function isValidCPF($value)	{
		$isValid = true;
	
		if ((strlen($value) < 11) || (strlen($value) > 14)) {
			$isValid = false;
			return (false);
		}
		$d1 = 0;
		$d2 = 0;
		$cpf = preg_replace("/[^0-9]/", "", $value);
		$ignore_list = array(
				'01234567890',
				'11111111111',
				'22222222222',
				'33333333333',
				'44444444444',
				'55555555555',
				'66666666666',
				'77777777777',
				'88888888888',
				'99999999999'
		);
		if(strlen($cpf) != 11 || in_array($cpf, $ignore_list)){
			return false;
		} else {
			for($i = 0; $i < 9; $i++){
				$d1 += $cpf[$i] * (10 - $i);
			}
			$r1 = $d1 % 11;
			$d1 = ($r1 > 1) ? (11 - $r1) : 0;
			for($i = 0; $i < 9; $i++) {
				$d2 += $cpf[$i] * (11 - $i);
			}
			$r2 = ($d2 + ($d1 * 2)) % 11;
			$d2 = ($r2 > 1) ? (11 - $r2) : 0;
			return (substr($cpf, -2) == $d1 . $d2) ? true : false;
		}
	}
	
	public static function toNumber($str) {
		return preg_replace("/[^0-9]/", "", $str);
	}
	
	public static function toDate($date) {
		if (!empty($date)) {
			$date		= implode("-", array_reverse(explode("/", $date))); 
		}else{
			$date		= null;
		}
		return $date;
	}
	
	/**
	 * Formatar um CPF
	 *
	 * @param number
	 * @return string
	 */
	public static function formatCPF($cpf) {
		if ((strlen($cpf)) !== 11)  {
			return $cpf;
		}else{
			return (substr($cpf,0,3) . '.'.substr($cpf,3,3) . '.'.substr($cpf,6,3) . '-'.substr($cpf,9,3));
		}
	}
	
	/**
	 * Formatar um Telefone
	 *
	 * @param number
	 * @return string
	 */
	public static function formatPhone($phone) {
		if ((strlen($phone)) == 10)  {
			return ('(' .substr($phone,0,2) . ') ' .substr($phone,2,4) . '-'.substr($phone,6,4));
		}else if ((strlen($phone)) !== 11)  {
			return $phone;
		}else{
			return ('(' .substr($phone,0,2) . ') ' .substr($phone,2,5) . '-'.substr($phone,7,4));
		}
	}

	/**
	 * Gerar a codigo aleatória
	 */
	public static function _geraCodigoRandom() {
		return md5(time() . rand());
	}
	
	/**
	 * Gerar uma senha aleatória
	 */
	public static function _geraSenha($size = 6){
		$basic = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	
		$return= "";
	
		for($count= 0; $size > $count; $count++){
			//Gera um caracter aleatorio
			$return.= $basic[rand(0, strlen($basic) - 1)];
		}
	
		return $return;
	}
	
	function getNomeCompleto($str){
		$nome = explode(" ",$str);
		$primeiro_nome = $nome[0];
		$segundo_nome = $nome[1];
		unset($nome[0]);
		$resto = implode(" ", $nome);
	
		return array('primeiro_nome'=> $primeiro_nome, 'segundo_nome'=> $segundo_nome, 'resto_nome' => $resto);
	}
	
}