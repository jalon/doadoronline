<?php
namespace AppClass\Hem;
/**
 * Fun��es diversas
 *
 * @package \App\Util
 * @author Jalon V�tor Cerqueira Silva
 * @version 1.0.1
 * @created 17/07/2013
 */
class Sangue {
	
	const A_MAIS 	 = 1;
	const A_MENOS	 = 2;
	const B_MAIS 	 = 3;
	const B_MENOS	 = 4;
	const AB_MAIS	 = 5;
	const AB_MENOS	 = 6;
	const O_MAIS	 = 7;
	const O_MENOS	 = 8;
	const NAO_SEI	 = 9;
	
	/**
	 * Construtor privado, a classe deve ser usada de forma statica
	 */
	private function __construct() {
		
	}
	
	/**
	 * Resgatar sangue compativel
	 * @param string $arquivo
	 * @return string 
	 */
	public static function getPodeDoarPara ($tipoSangue) {
		
		if ($tipoSangue == self::A_MAIS){
			return array (self::A_MAIS, self::AB_MAIS);
		}else if ($tipoSangue == self::A_MENOS){
			return array (self::A_MAIS, self::A_MENOS, self::AB_MAIS, self::AB_MENOS);
		}else if ($tipoSangue == self::B_MAIS){
			return array (self::B_MAIS, self::AB_MAIS);
		}else if ($tipoSangue == self::B_MENOS){
			return array (self::B_MAIS, self::B_MENOS, self::AB_MAIS, self::AB_MENOS);
		}else if ($tipoSangue == self::AB_MAIS){
			return array (self::AB_MAIS);
		}else if ($tipoSangue == self::AB_MENOS){
			return array (self::AB_MAIS, self::AB_MENOS);
		}else if ($tipoSangue == self::O_MAIS){
			return array (self::A_MAIS, self::B_MAIS, self::O_MAIS, self::AB_MAIS);
		}else if ($tipoSangue == self::O_MENOS || $tipoSangue == self::NAO_SEI){
			return array (self::A_MAIS, self::A_MENOS, self::B_MAIS, self::B_MENOS, self::AB_MAIS, self::AB_MENOS, self::O_MAIS, self::O_MENOS);
		}
	}
	
	/**
	 * Resgatar sangue compativel
	 * @param string $arquivo
	 * @return string
	 */
	public static function getPodeReceberDe ($tipoSangue) {
		
		if ($tipoSangue == self::A_MAIS){
			return array (self::A_MAIS, self::A_MENOS, self::O_MAIS, self::O_MENOS);
		}else if ($tipoSangue == self::A_MENOS){
			return array (self::A_MENOS, self::O_MENOS);
		}else if ($tipoSangue == self::B_MAIS){
			return array (self::B_MAIS, self::B_MENOS, self::O_MAIS, self::O_MENOS);
		}else if ($tipoSangue == self::B_MENOS){
			return array ( self::B_MENOS, self::O_MENOS);
		}else if ($tipoSangue == self::AB_MAIS || $tipoSangue == self::NAO_SEI){
			return array (self::A_MAIS, self::A_MENOS, self::B_MAIS, self::B_MENOS, self::AB_MAIS, self::AB_MENOS, self::O_MAIS, self::O_MENOS);
		}else if ($tipoSangue == self::AB_MENOS){
			return array (self::A_MENOS, self::B_MENOS, self::AB_MENOS, self::O_MENOS);
		}else if ($tipoSangue == self::O_MAIS){
			return array (self::O_MAIS, self::O_MENOS);
		}else if ($tipoSangue == self::O_MENOS){
			return array (self::O_MENOS);
		}
	}
	
	/**
	 * Resgatar sangue compativel
	 * @param string $arquivo
	 * @return string
	 */
	public static function listaQuantidadeReceber ($tipoSangue) {
		global $db, $log, $system;
		
		$tipoSanguePer = implode(",", self::getPodeReceber($tipoSangue));
		
		$info	 = $db->extraiPrimeiro('SELECT COUNT(P.CODIGO) AS QUANTIDADE FROM `SLSEG_USUARIO` AS U 
					LEFT OUTER JOIN `SLSEG_PESSOA` P ON (U.COD_PESSOA = P.CODIGO)
					WHERE P.TIPO_SANGUINEO IN(:tipoSangue) AND U.CODIGO !=:codigo', 
				array(':tipoSangue' => $tipoSanguePer,':codigo' => $system->getCodUsuario()));
		
		return $info->QUANTIDADE;
	}
	
	/**
	 * Resgatar sangue compativel
	 * @param string $arquivo
	 * @return string
	 */
	public static function listaUsuariosPodeDoar ($tipoSangue, $codEstado = null) {
		global $db, $log, $system;
	
		$tipoSanguePer = implode(",", self::getPodeReceberDe($tipoSangue));
		
		$info	 = $db->extraiTodos('SELECT U.CODIGO AS COD_USUARIO, P.*, TS.DESCRICAO AS TIPO_SANGUE, C.NOME AS CIDADE FROM `SLSEG_USUARIO` AS U 
					LEFT OUTER JOIN `SLSEG_PESSOA` P ON (U.COD_PESSOA = P.CODIGO)
					LEFT OUTER JOIN `SLADM_TIPO_SANGUE` TS ON (TS.CODIGO = P.TIPO_SANGUINEO)
					LEFT OUTER JOIN `SLADM_CIDADE` C ON (C.CODIGO = P.COD_CIDADE)				
						WHERE P.TIPO_SANGUINEO IN('.$tipoSanguePer.') AND P.COD_ESTADO =:codEstado AND U.COD_STATUS = 1 AND P.COD_TIPO = "DOA" AND U.CODIGO !=:codigo
						AND (P.DATA_ULT_DOACAO IS NULL OR IF(P.COD_SEXO = "MAS", NOW() > P.DATA_ULT_DOACAO + INTERVAL 3 MONTH, NOW() > P.DATA_ULT_DOACAO + INTERVAL 2 MONTH) )',
							array(':codEstado' => $codEstado,':codigo' => $system->getCodUsuario()));
		
		$log->debug( $info );
		return $info;
	}
}