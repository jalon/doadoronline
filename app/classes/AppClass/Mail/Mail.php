<?php
namespace AppClass\Mail;

require (CLASS_PATH. "/AppClass/Mail/class.phpmailer.php");

#use PHPMailer;

/**
 * Implementação do Mail
 *
 * @package: Mail
 * @Author: Jalon Vitor Cerqueira Silva
 * @version: 1.0.1
 *
 */

class Mail  {
	
	public function __construct() {
		global $system, $log;

		$mail = new \PHPMailer();
		if (!$mail /*|| !$transport */) return 0;
		
		$log->debug($system->config["mail"]["charset"]);
		#################################################################################
		## Decriptar a senha
		#################################################################################
		$crypt				= new \AppClass\App\Crypt();
		$senhaSmtp			= $crypt->decrypt($system->config["mail"]["senhaSmtp"]);
		
		$mail->IsSMTP();
		$mail->CharSet 		= $system->config["mail"]["charset"];
		$mail->Host 		= $system->config["mail"]["servidorSmtp"];
		$mail->SMTPAuth		= $system->config["mail"]["smtpAuth"];
		$mail->Port 		= $system->config["mail"]["portaSmtp"];
		$mail->SMTPSecure 	= $system->config["mail"]["tipoCriptSmtp"];
		$mail->Username		= $system->config["mail"]["usuarioSmtp"];
		$mail->Password		= $senhaSmtp;
		
		return $mail;
	}
	
	public static function inicializa() {
		
	}
	
}