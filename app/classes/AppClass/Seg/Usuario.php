<?php

namespace AppClass\Seg;


/**
 * Usuário
 *
 * @package Usuario
 * @author Jalon V�tor Cerqueira Silva
 * @version 1.0.1
 */
class Usuario {

	/**
	 * Usuario
	 * @var unknown
	 */
	private $_usuario;
	
	/**
	 * Código Usuario postado no formulário
	 * @var unknown
	 */
	private $_codUsuario;
	
	/**
	 * Associação Usuário - Organizacao
	 * @var unknown
	 */
	private $_oUsuOrg;
	
	/**
	 * Indicador Endereço obrigatório
	 * @var unknown
	 */
	
	private $_indEndObrigatorio;
	
	/**
	 * Perfil
	 * @var unknown
	 */
	private $_perfil;
	
	/**
	 * Cod Organizacao
	 * @var unknown
	 */
	private $_codOrganizacao;
	
	/**
	 * Confirmação de envio de email
	 * @var unknown
	 */
	private $_enviarEmail;
	
	/**
	 * Ententidade do telefone
	 * @var unknown
	 */
	private $_entidadeTel;
	
	/**
	 * Telefones
	 * @var unknown
	 */
	private $_telefone;
	
	/**
	 * Codigo do tipo de telefone
	 * @var unknown
	 */
	private $_codTipoTel;
	
	/**
	 * Codigo do telefone
	 * @var unknown
	 */
	private $_codTelefone;
	
	/**
     * Construtor
     *
	 * @return void
	 */
	public function __construct() {
		global $log;
		$log->debug(__CLASS__.": nova instância");
		
	}
	
	public function _setCodUsuario($codigo) {
		$this->_codUsuario = $codigo;
	}
	
	public function _getCodUsuario() {
		return ($this->_codUsuario);
	}
	
	public function _setUsuOrg($usuOrg) {
		$this->_oUsuOrg = $usuOrg;
	}
	
	public function _getUsuOrg() {
		return ($this->_oUsuOrg);
	}
	
	public function _setIndEndObrigatorio($valor) {
		$this->_indEndObrigatorio = $valor;
	}
	
	public function _getIndEndObrigatorio() {
		return ($this->_indEndObrigatorio);
	}
	
	public function _setPerfil($codigo) {
		$this->_perfil = $codigo;
	}
	
	public function _getPerfil() {
		return ($this->_perfil);
	}
	
	public function _setCodOrganizacao($codigo) {
		$this->_codOrganizacao = $codigo;
	}
	
	public function _getCodOrganizacao() {
		return ($this->_codOrganizacao);
	}
	
	//GET-SET ENTIDADE TELEFONE
	public function _setEntidadeTel($entidade) {
		$this->_entidadeTel = $entidade;
	}
	
	public function _getEntidadeTel() {
		return ($this->_entidadeTel);
	}
	
	public function _setTelefone(array $telefone) {
		$this->_telefone = $telefone;
	}
	
	public function _getTelefone() {
		return ($this->_telefone);
	}
	
	public function _setCodTipoTel(array $tipoTelefone) {
		$this->_codTipoTel = $tipoTelefone;
	}
	
	public function _getCodTipoTel() {
		return ($this->_codTipoTel);
	}
	
	public function _setCodTelefone(array $codTelefone) {
		$this->_codTelefone = $codTelefone;
	}
	
	public function _getCodTelefone() {
		return ($this->_codTelefone);
	}
	
	public function _setEnviarEmail($enviarEmail) {
		$this->_enviarEmail = $enviarEmail;
	}
	
	public function _getEnviarEmail() {
		return ($this->_enviarEmail);
	}
	
    /**
     * Lista as empresas que o usuário tem acesso
     */
    public static function listaOrganizacaoAcesso ($codUsuario) {
    	global $em;
    	 
    	$qb 	= $em->createQueryBuilder();
    	
    	$qb->select('o')
    	->from('\Entidades\ZgadmOrganizacao','o')
    	->leftJoin('\Entidades\ZgsegUsuarioOrganizacao',	'uo',	\Doctrine\ORM\Query\Expr\Join::WITH, 'o.codigo 		= uo.codOrganizacao')
    	->leftJoin('\Entidades\ZgsegPerfil', 				'p', 	\Doctrine\ORM\Query\Expr\Join::WITH, 'p.codigo 		= uo.codPerfil')
    	->leftJoin('\Entidades\ZgadmOrganizacaoStatusTipo', 'st',	\Doctrine\ORM\Query\Expr\Join::WITH, 'o.codStatus 	= st.codigo')
    	->where($qb->expr()->andX(
    		$qb->expr()->eq('uo.codUsuario'			, ':codUsuario'),
   			$qb->expr()->eq('uo.codStatus'			, ':status'),
    		$qb->expr()->eq('p.indAtivo'			, '1'),
    		$qb->expr()->eq('st.indPermiteAcesso'	, '1')
    	))
    	->orderBy('o.identificacao', 'ASC')
    	->setParameter('codUsuario', $codUsuario)
    	->setParameter('status', 	"A");
    	 
    	$query 		= $qb->getQuery();
    	return($query->getResult());
    	
    }
    
    /**
     * Lista todos os usuarios de uma organizacao (retirando os cancelados)
     */
    public static function listaUsuarioOrganizacao ($codOrganizacao, $codTipo) {
    	global $em;
    
    	$qb 	= $em->createQueryBuilder();
    	 
    	$qb->select('uo')
    	->from('\Entidades\ZgsegUsuario','us')
    	->leftJoin('\Entidades\ZgsegUsuarioOrganizacao',		'uo',	\Doctrine\ORM\Query\Expr\Join::WITH, 'us.codigo 	= uo.codUsuario')
    	->leftJoin('\Entidades\ZgsegPerfil',					'p',	\Doctrine\ORM\Query\Expr\Join::WITH, 'uo.codPerfil	= p.codigo')
    	->where($qb->expr()->andX(
   			$qb->expr()->eq('uo.codOrganizacao'				, ':codOrganizacao'),
    		$qb->expr()->eq('p.codTipoUsuario'				, ':codTipoUsuario'),
    		$qb->expr()->not($qb->expr()->eq('uo.codStatus'	, ':codStatusExcluido'))
    	))
    	->orderBy('us.nome', 'ASC')
    	->setParameter('codOrganizacao', $codOrganizacao)
    	->setParameter('codStatusExcluido', 'C')
    	->setParameter('codTipoUsuario', $codTipo);
    	$query 		= $qb->getQuery();
    	return($query->getResult());
    	 
    }
    
    /**
     * Lista todos os usuarios ATIVOS de uma organizacao
     */
    public static function listaUsuarioOrganizacaoAtivo ($codOrganizacao, $codTipo) {
    	global $em;
    
    	$qb 	= $em->createQueryBuilder();
    
    	$qb->select('uo')
    	->from('\Entidades\ZgsegUsuario','us')
    	->leftJoin('\Entidades\ZgsegUsuarioOrganizacao',		'uo',	\Doctrine\ORM\Query\Expr\Join::WITH, 'us.codigo 	= uo.codUsuario')
    	->leftJoin('\Entidades\ZgsegPerfil',					'p',	\Doctrine\ORM\Query\Expr\Join::WITH, 'uo.codPerfil	= p.codigo')
    	->where($qb->expr()->andX(
    			$qb->expr()->eq('uo.codOrganizacao'		, ':codOrganizacao'),
    			$qb->expr()->eq('p.codTipoUsuario'		, ':codTipoUsuario'),
    			$qb->expr()->eq('uo.codStatus'			, ':codStatusAtivo'))
    	)
    	->orderBy('us.nome', 'ASC')
    	->setParameter('codOrganizacao', $codOrganizacao)
    	->setParameter('codStatusAtivo', 'A')
    	->setParameter('codTipoUsuario', $codTipo);
    	$query 		= $qb->getQuery();
    	return($query->getResult());
    
    }
	
    /**
     * Lista todas as organizações que o usuário tem acesso
     */
    public static function listaOrganizacoesAcesso ($usuario) {
    	global $em;
    
    	$qb 	= $em->createQueryBuilder();
    
    	$qb->select('o')
    	->from('\Entidades\ZgsegUsuario','us')
    	->leftJoin('\Entidades\ZgsegUsuarioOrganizacao',		'uo',	\Doctrine\ORM\Query\Expr\Join::WITH, 'us.codigo 	= uo.codUsuario')
    	->leftJoin('\Entidades\ZgadmOrganizacao',				'o',	\Doctrine\ORM\Query\Expr\Join::WITH, 'o.codigo 		= uo.codOrganizacao')
    	->leftJoin('\Entidades\ZgsegPerfil',					'p',	\Doctrine\ORM\Query\Expr\Join::WITH, 'uo.codPerfil	= p.codigo')
    	->where($qb->expr()->andX(
   			$qb->expr()->eq('us.usuario'			, ':usuario'),
   			$qb->expr()->eq('uo.codStatus'			, ':codStatusAtivo'))
    	)
    	->orderBy('o.nome', 'ASC')
    	->setParameter('usuario'			, $usuario)
    	->setParameter('codStatusAtivo'		, 'A');
    	$query 		= $qb->getQuery();
    	return($query->getResult());
    
    }
    
    
    
    /**
     * Lista os menus do usuário em uma determinada empresa
     */
    public static function listaMenusAcesso ($codUsuario) {
    	global $db,$log,$system;
    	try {
    		$info = $db->extraiTodos('SELECT M.CODIGO AS CODIGO, M.COD_PAI AS COD_PAI, M.NOME AS NOME, M.DESCRICAO AS DESCRICAO, M.COD_TIPO, M.LINK, M.ICONE, MP.ORDEM, MP.COD_TIPO_PERFIL, MP.COD_GRUPO_ACESSO, U.USUARIO
				FROM `SLAPP_MENU` M
					LEFT OUTER JOIN `SLAPP_MENU_PERFIL` MP ON (M.CODIGO = MP.COD_MENU) 
			        LEFT OUTER JOIN `SLADM_PERFIL_ACESSO` A ON (MP.COD_TIPO_PERFIL = A.CODIGO) 
			        LEFT OUTER JOIN `SLSEG_USUARIO` U ON (A.CODIGO = U.COD_PERFIL)
			        	WHERE U.CODIGO = :codUsuario AND U.COD_ORGANIZACAO = :codOrganizacao AND MP.COD_GRUPO_ACESSO IN(:codGrupoAcesso, "USU")
    						ORDER BY MP.ORDEM ASC', 
    				array(':codUsuario' => $codUsuario, 
    					':codOrganizacao' => $system->getCodOrganizacao(),
    					':codGrupoAcesso' => $system->getCodGrupoAcesso()
    				));
	    	return($info);
	    }catch (\Doctrine\ORM\ORMException $e) {
	    	\AppClass\App\Erro::halt($e->getMessage());
	    }
    	
    }
    
    /**
     * Lista os menus do usuário em uma determinada empresa
     */
    public static function listaGrupoAcesso () {
    	global $db,$log,$system;
    	try {
    		$info = $db->extraiTodos('SELECT GA.CODIGO AS CODIGO, GA.DESCRICAO AS DESCRICAO 
				FROM `SLAPP_MENU_PERFIL` MP
					LEFT OUTER JOIN `SLADM_GRUPO_ACESSO` GA ON (MP.COD_GRUPO_ACESSO = GA.CODIGO)
			        LEFT OUTER JOIN `SLADM_PERFIL_ACESSO` A ON (MP.COD_TIPO_PERFIL = A.CODIGO)
			        LEFT OUTER JOIN `SLSEG_USUARIO` U ON (A.CODIGO = U.COD_PERFIL)
			        	WHERE U.CODIGO = :codUsuario AND U.COD_ORGANIZACAO = :codOrganizacao
    						GROUP BY CODIGO',
    				array(':codUsuario' => $system->getCodUsuario(),
    						':codOrganizacao' => $system->getCodOrganizacao()
    				));
    		return($info);
    	}catch (\Doctrine\ORM\ORMException $e) {
    		\AppClass\App\Erro::halt($e->getMessage());
    	}
    	 
    }
    
    /**
     * Verifica se o usuário tem Acesso a organização
     */
    public static function temAcessoOrganizacao($codUsuario,$codOrganizacao) {
    	global $db,$system;
    	try {
    		$info = $db->extraiPrimeiro('SELECT * FROM `SLSEG_USUARIO` WHERE CODIGO = :codUsuario AND COD_ORGANIZACAO = :cod_Org', 
    				array(':codUsuario' => $codUsuario, ':cod_Org' => $codOrganizacao));

    	}catch (\Exception $e) {
			\AppClass\App\Erro::halt($e->getMessage());
		}
    	
    	if (!$info)	{
    		return false;
    	}elseif(!isset($info->COD_STATUS) || empty($info->COD_STATUS)) {
    		return false;
    	}elseif($info->COD_STATUS == 0) {
    		return false;
    	}
    	 
    	return true;
    }
    
    /**
     * Pegar identificação da organizacao
     */
    public static function getIdentificacaoOrg($codOrganizacao) {
    	global $db,$system;
    	try {
    		$info = $db->extraiPrimeiro('SELECT IDENTIFICACAO FROM `SLADM_ORGANIZACAO` WHERE CODIGO = :codigo',
    				array(':codigo' => $codOrganizacao));
    
    	}catch (\Exception $e) {
    		\AppClass\App\Erro::halt($e->getMessage());
    	}
    	 
    	if (!$info)	{
    		return false;
    	}elseif(isset($info->scalar) && $info->scalar == false) {
    		return false;
    	}
    
    	return $info->IDENTIFICACAO;
    }
    
    /**
     * Verifica se o email de usuário já está cadastrado
     */

    public static function verificaEmail($email){
    	global $db,$log,$system;
    	
    	try {
    		$info	 = $db->extraiPrimeiro('SELECT * FROM `SLSEG_USUARIO` WHERE USUARIO = :usuario',
    				array(':usuario' => $email ));
        		
    		if(!$info){
    			return false;
    		}elseif(!isset($info->USUARIO) || empty($info->USUARIO)) {
    			return false;
    		}else {
    			return true;
    		}
    	} catch ( \Exception $e ) {
    		return false;
    	}
    }
    
    /**
     * Verifica usuarios aptos a doar
     */
    
    public static function verificaAptosADoar(){
    	global $db,$log,$system;
    	 
    	try {
    		$info	 = $db->extraiTodos('SELECT U.CODIGO AS COD_USUARIO, U.USUARIO AS USUARIO, P.NOME, P.DATA_ULT_DOACAO, TS.DESCRICAO TIPO_SANGUE FROM `SLSEG_USUARIO` U
    				LEFT OUTER JOIN `SLSEG_PESSOA` P ON (P.CODIGO = U.COD_PESSOA)
    				LEFT OUTER JOIN `SLADM_TIPO_SANGUE` TS ON (TS.CODIGO = P.TIPO_SANGUINEO)
    					WHERE U.COD_ORGANIZACAO =:codOrg AND U.COD_STATUS =:codStatus AND U.COD_PERFIL =:codPerfil AND P.COD_TIPO =:codTipo AND NOW() > P.DATA_ULT_DOACAO + INTERVAL 3 MONTH',
    				array(':codOrg' => '1', ':codStatus' => '1', ':codPerfil' => "USU", ':codTipo' => "DOA" ));
    
    		if(!$info){
    			return false;
    		}elseif(isset($info->scalar) && $info->scalar == false) {
    			return false;
    		}else {
    			return $info;
    		}
    	} catch ( \Exception $e ) {
    		return false;
    	}
    }
    
    /**
     * Verifica usuarios aptos a doar, enviar notificação
     */
    
    public static function verificaAptosADoarNotif(){
    	global $db,$log,$system;
    
    	try {    		
    		$info	 = $db->extraiTodos('SELECT U.CODIGO AS COD_USUARIO, U.USUARIO AS USUARIO, P.NOME, P.DATA_ULT_DOACAO, TS.DESCRICAO TIPO_SANGUE FROM `SLSEG_USUARIO` U
    				LEFT OUTER JOIN `SLSEG_PESSOA` P ON (P.CODIGO = U.COD_PESSOA)
    				LEFT OUTER JOIN `SLADM_TIPO_SANGUE` TS ON (TS.CODIGO = P.TIPO_SANGUINEO)
    					WHERE U.COD_ORGANIZACAO =:codOrg AND U.COD_STATUS =:codStatus AND U.COD_PERFIL =:codPerfil AND P.COD_TIPO =:codTipo
    					AND IF(P.COD_SEXO = "MAS", NOW() > P.DATA_ULT_DOACAO + INTERVAL 3 MONTH, NOW() > P.DATA_ULT_DOACAO + INTERVAL 2 MONTH)
    				AND U.CODIGO NOT IN (
						SELECT NU.COD_USUARIO FROM `SLNOT_NOTIFICACAO_USUARIO` NU 
						LEFT OUTER JOIN `SLNOT_NOTIFICACAO` N ON (N.CODIGO = NU.COD_NOTIFICACAO)
						WHERE N.COD_TIPO = "A" AND NOW() < N.DATA_CRIADA + INTERVAL 1 MONTH GROUP BY NU.COD_USUARIO)
					GROUP BY COD_USUARIO',
    				array(':codOrg' => '1', ':codStatus' => '1', ':codPerfil' => "USU", ':codTipo' => "DOA" ));
    
    		return $info;
    		
    	} catch ( \Exception $e ) {
    		return false;
    	}
    }
    
    /**
     * Resgata a última organização que o usuário acessou 
     */
    public static function getUltimaOrganizacaoAcesso ($usuario) {
    	global $em;
    
    	$qb 	= $em->createQueryBuilder();
    	 
    	$qb->select('o')
    	->from('\Entidades\ZgadmOrganizacao','o')
    	->leftJoin('\Entidades\ZgsegUsuario','u',	\Doctrine\ORM\Query\Expr\Join::WITH, 'o.codigo 		= u.ultOrgAcesso')
    	->where($qb->expr()->andX(
   			$qb->expr()->eq('u.usuario'			, ':usuario')
    	))
    	->orderBy('o.identificacao', 'ASC')
    	->setParameter('usuario', $usuario);
    
    	$query 		= $qb->getQuery();
    	return($query->getOneOrNullResult());
    	 
    }
    
    
	/**
	 * Busca usuários
	 */
	public static function busca ($sBusca = null,$start = 0,$limite = 10, $colunaOrdem = null,$dirOrdem = null) {
		global $em,$tr,$system;
		 
		//$em->getRepository('Entidades\ZgsegUsuario')->findAll();
		$qb 	= $em->createQueryBuilder();
		
		try {
			$qb->select('u')
			->from('\Entidades\ZgsegUsuario','u')
	    	->leftJoin('\Entidades\ZgsegUsuarioStatusTipo'	,'st'	, \Doctrine\ORM\Query\Expr\Join::WITH, 'u.codStatus = st.codigo')
			->where($qb->expr()->eq('u.codOrganizacao'	, ':codOrg'))
			->setParameter('codOrg', $system->getCodOrganizacao());
			
			if ($colunaOrdem !== null) {
				$dir	= strtoupper($dirOrdem);
				if (!$dir)	$dir = "ASC";
				$qb->orderBy("u.".$colunaOrdem, $dir);
			}
			
/*			->orderBy('u.nome', 'ASC')
*/
			if ($sBusca) {
				$qb->andWhere($qb->expr()->orx(
					$qb->expr()->like($qb->expr()->upper('u.usuario'), ':busca'),
					$qb->expr()->like($qb->expr()->upper('u.nome'), ':busca'),
					$qb->expr()->like($qb->expr()->upper('u.email'), ':busca')
				))
				->setParameter('busca', '%'.strtoupper($sBusca).'%');
			}
				
			
			if ($start 	!== null) $qb->setFirstResult( $start );
			if ($limite	!== null) $qb->setMaxResults( $limite );
			 
		
			$query = $qb->getQuery();
			return ($query->getResult());
		
		}catch (\Doctrine\ORM\ORMException $e) {
			\AppClass\App\Erro::halt($e->getMessage());
		}
	
	}	
	
	public function _getCodigo() {
		return $this->_usuario->getCodigo();
	}
	
	public function _getUsuario() {
		return $this->_usuario;
	}
	
}
