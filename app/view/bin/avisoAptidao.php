<?php
#!/usr/bin/php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'includeNoAuth.php');
}else{
	include_once('../includeNoAuth.php');
}

echo '<meta charset="UTF-8">';

$info = \AppClass\Seg\Usuario::verificaAptosADoarNotif();
if ( !$info ) echo 'Não existe nenhum usuario <i>Apto a Doar</i>'; exit();
	
$html = null;

$html .= '<table border="1" class="table table-hover table-bordered">
			  <thead>
			    <tr>
			      <th>#</th>
				  <th>NOME</th>
				  <th>USUARIO</th>
			      <th>Sangue</th>
				  <th>Ultima Doacao</th>
				  <th>?</th>
			    </tr>
			  </thead>
				<tbody>';

#################################################################################
## Gerar a notificação
#################################################################################
$template		= $db->extraiPrimeiro('SELECT * FROM `SLNOT_NOTIFICACAO_TEMPLATE` WHERE TEMPLATE =:template', array(':template' => 'AVISO_APTIDAO'));
$notificacao	= new \AppClass\App\Notificacao(\AppClass\App\Notificacao::TIPO_MENSAGEM_TEMPLATE, \AppClass\App\Notificacao::TIPO_DEST_USUARIO, \AppClass\App\Notificacao::ALERTA_APTIDAO);
$notificacao->setAssunto("Você já está apto a doar.");

for ($i = 0; $i < sizeof($info); $i++) {
	$notificacao->associaUsuario( $info[$i]->COD_USUARIO );
	
	$html .="<tr>
		      <td>#". $info[$i]->COD_USUARIO ."</td>
		      <td>". $info[$i]->NOME ."</td>
		      <td>". $info[$i]->USUARIO ."</td>
		      <td>". $info[$i]->TIPO_SANGUE ."</td>
		      <td>".\AppClass\App\Util::formatData($system->config['data']['dateFormat'], $info[$i]->DATA_ULT_DOACAO)."</td>
		      <td>ENVIADO</td>
		     </tr>";
}

$notificacao->enviaEmail();
$notificacao->setCodTemplate($template->CODIGO);
$notificacao->salva();

$html .= '</tbody></table>';

echo $html;

?>