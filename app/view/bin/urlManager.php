<?php
global $log;
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'includeNoAuth.php');
}else{
	include_once('../includeNoAuth.php');
}

#################################################################################
## Verifica se a url foi passada
#################################################################################
if (isset($_GET['url'])) {
	$url = \AppClass\App\Util::antiInjection($_GET["url"]);
}/*else if (isset($_GET['pagina'])) {
	$pagina = \AppClass\App\Util::antiInjection($_GET["pagina"]);
}*/else{	
	include(BIN_PATH . 'notFoundExt.php');
	exit;
}

#################################################################################
## Formata a URL
#################################################################################
#$array = ['index.php','inicio','quem-somos','onde-doar','informacoes','faq','cadastro','contato','obrigado'];
$array = ['entrar', 'contato', 'sobre', '404'];

if ( $system->config["manutencao"] == 2 ) {
	include(DOC_ROOT . '/view/html/manutencao.html');
	exit;
}else if ( !$url || in_array($url, $array) ) {
	if ( !$url ) $url = 'index';
	include(DOC_ROOT . '/view/siteV2/'.$url.'.html');
	exit;
}else{
	#################################################################################
	## Tentar detectar se a Url é a identificação de uma Organização
	#################################################################################
	if (substr($url,-1) == "/") $url	= substr($url,0,-1);
	$pieces 	= explode("/", $url);
	
	if (sizeof($pieces) == 1) {
		$_ident			= $pieces[0];
		
		$ret			= \AppClass\Adm\Organizacao::buscaPorIdentificacao($_ident);
		
		if ($ret && sizeof($ret) > 0) {
			$_org		= $ret;
		}

		if ( $system->config["manutencao"] == 1 ) {
			include(DOC_ROOT . '/view/html/manutencao.html');
			exit;
		}else if (isset($_org) && !isset($_org->scalar)) {
			$log->debug("Organização encontrada: ".$_org->IDENTIFICACAO);
			include(DOC_ROOT . '/view/index.php');
			exit;
		}else{
			$modulo	= ucfirst($pieces[0]);
		}
	}else{
		$modulo	= ucfirst($pieces[0]);
	}
	
	if (!isset($pieces[1])) {
		$script	= null;
		$modulo	= null;
	}else{
		$script	= $pieces[1];
	}
	
	if (strlen($modulo) != 3) {
		include(BIN_PATH . 'notFoundExt.php');
		exit;
	}else{
		$aScr	= explode("?", $script);
		
		if (sizeof($aScr) > 1) {
			$script	= $aScr[0];
		} 
		
		if (file_exists(MOD_PATH . "/$modulo/php/$script")) {
			include_once MOD_PATH . "/$modulo/php/$script";
			exit;
		}elseif (file_exists(MOD_PATH . "/$modulo/dp/$script")) {
			include_once MOD_PATH . "/$modulo/dp/$script";
			exit;
		}else{
			$log->debug("Url não encontrada ($url)");
			include(BIN_PATH . 'notFoundExt.php');
			exit;
		}
		
	}
	
}

?>