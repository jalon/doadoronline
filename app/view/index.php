<?php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'includeNoAuth.php');
}else{
	include_once('../includeNoAuth.php');
}

#################################################################################
## Verificando se mudou de organiza��o
#################################################################################
if (isset($_GET['did'])) {
	$did = \AppClass\App\Util::antiInjection($_GET["did"]);
	\AppClass\App\Util::descompactaId($did);

	if (isset($_codGrupoAcesso) && isset($system) && is_object($system)) {
		/** Verifica se o usu�rio tem acesso a organiza��o **/
		$temAcesso			= \AppClass\Seg\Usuario::temAcessoOrganizacao($system->getCodUsuario(),$system->getCodOrganizacao());
		if ($temAcesso) {
			$_org 					= $db->extraiPrimeiro('SELECT * FROM `SLADM_ORGANIZACAO` WHERE CODIGO = :codOrg', array(':codOrg' => $system->getCodOrganizacao() ));
			$_SESSION['_codOrg']	= $_org->CODIGO;
			$system->setCodGrupoAcesso($_codGrupoAcesso);
		}else{
			$system->desautentica();
		}
	}
}

if (!isset($_org) && (!isset($_SESSION['_codOrg']))) {
	
	if ((isset($_POST['zgUsuario'])) && (isset($_POST['zgSenha']))) {
		/** Resgatar a �ltima organiza��o que o usu�rio acessou **/
		$_org	= \AppClass\Seg\Usuario::getUltimaOrganizacaoAcesso($_POST['zgUsuario']);
		
		/** Caso n�o tenha, ou seja, seja o primeiro acesso, resgatar a primeira organiza��o encontrada, que o usu�rio esteja ativo **/
		if (!$_org) {
			$orgs	= \AppClass\Seg\Usuario::listaOrganizacoesAcesso($_POST['zgUsuario']);
			if ($orgs && sizeof($orgs) > 0) {
				$_org	= $orgs[0];
			}else{
				die ("Sem acesso a nenhuma organiza��o !!!");
			}
		}
	}else{
		die ("Organiza��o n�o definida !!!");
	}
}

$system->setCodLang(1);

if ( isset($_org) ) {
	$system->setCodOrganizacao($_org->CODIGO);
}else{
	$_org = $db->extraiPrimeiro('SELECT * FROM `SLADM_ORGANIZACAO` WHERE CODIGO = :codOrg', array(':codOrg' => $_SESSION['_codOrg'] ));
	$system->setCodOrganizacao($_SESSION['_codOrg']);
}

/** Define a organiza��o **/
$db->setOrganizacao($system->getCodOrganizacao());
	
//$log->debug("C�digo da Organiza��o: ".$system->getCodOrganizacao());
include_once(BIN_PATH . 'auth.php');

#################################################################################
## Resgata a vari�vel ID que est� criptografada
#################################################################################
if (isset($_GET['id'])) {
	$id = \AppClass\App\Util::antiInjection($_GET["id"]);
}elseif (isset($_POST['id'])) {
	$id = \AppClass\App\Util::antiInjection($_POST["id"]);
}else{
	$id	= null;
}

#################################################################################
## Descompacta o ID
#################################################################################
\AppClass\App\Util::descompactaId($id);

#################################################################################
## Descobre o m�dulo que ser� iniciado
#################################################################################
if (isset($_codModulo_)) {
	$system->selecionaModulo($_codModulo_);
	$codModulo	= $_codModulo_;
}else{
	$codModulo	= null;
	$urlInicial	= ROOT_URL . "/App/modulo.php?id=";
}

#################################################################################
## Verifica se o m�dulo tem o Dashboard
#################################################################################
/*if (isset($_mod) && is_object($_mod)) {
	if (file_exists(MOD_PATH . "/".$_mod->getApelido().'/php/dashboard.php')) {
		$urlInicial	= ROOT_URL . "/" . $_mod->getApelido()."/dashboard.php?id=".$id;
	}else{
		$urlInicial	= "#";
	}
}else{
	$urlInicial	= "#";
}*/

#################################################################################
## Verificar se � para trocar senha do usu�rio
#################################################################################
/*$urlTrocaSenha		= ROOT_URL . "/App/alteraSenha.php?id=".$id;

if ((isset($_user)) && ($_user->getIndTrocarSenha() == 1)) {
	$urlInicial			= $urlTrocaSenha;
	$indTrocarSenha		= 1;
}else{
	$indTrocarSenha		= 0;
}*/

#################################################################################
## Abrir o index
#################################################################################
$urlInicial 	= \AppClass\App\Menu\Tipo::montaUrlCompleta(6); // Codigo Perfil

#################################################################################
## Define a constante do site inicial
#################################################################################
$system->setHomeUrl($_SERVER['REQUEST_URI']);

#################################################################################
## Define o nome do iframe Central
#################################################################################
$system->setDivCentral("divContent");

#################################################################################
## Cria o objeto do Menu
#################################################################################
$tipoMenu	= \AppClass\App\Menu\Tipo::TIPO2;

$menu	= \AppClass\App\Menu::criar($tipoMenu);
$menu->setTarget($system->getDivCentral());

#################################################################################
## Carrega os menus fixos
#################################################################################
/*$menus 	= $em->getRepository('Entidades\ZgappMenu')->findBy(array('indFixo' => '1'));

#################################################################################
## Adiciona os menus fixos
#################################################################################
foreach ($menus as $dados) {
	$url 	= \AppClass\App\Menu\Tipo::montaUrlCompleta($dados->getCodigo());
	$menu->adicionaLinkFixo($dados->getCodigo(), $dados->getNome(), $dados->getIcone(), $url, $dados->getDescricao());
}
*/

#################################################################################
## Carrega os menus do m�dulo
#################################################################################
$menus 	= \AppClass\Seg\Usuario::listaMenusAcesso($_user->CODIGO);

#################################################################################
## Adiciona os menus no objeto
#################################################################################
if ($menus) {
	foreach ($menus as $dados) {
		if ($dados->COD_TIPO == "M") {
			$codMenuPai	= $dados->COD_PAI ? $dados->COD_PAI : null;  
			$menu->adicionaPasta($dados->CODIGO, $dados->NOME, $dados->ICONE, $codMenuPai);
		}elseif ($dados->COD_TIPO == "L") {
			$url 	= \AppClass\App\Menu\Tipo::montaUrlCompleta($dados->CODIGO);
			$codMenuPai	= $dados->COD_PAI ? $dados->COD_PAI : null;
			$menu->adicionaLink($dados->CODIGO, $dados->NOME, $dados->ICONE, $url, $dados->DESCRICAO, $codMenuPai);
		}else{
			die('Tipo de Menu desconhecido');
		}
	}
}

/*
#################################################################################
## Gera o c�digo javascript das m�scaras
#################################################################################
$mascaras	= $em->getRepository('Entidades\ZgappMascara')->findAll();
$htmlMask		= "";
for ($i = 0; $i < sizeof($mascaras); $i++) {
	if ($mascaras[$i]->getIndReversa() == 1) {
		$reverse	= ",reverse: true";
	}else{
		$reverse	= "";
	}
	
	if ($mascaras[$i]->getIndTamanhoFixo() === 0) {
		$maxLen	= ",maxlength: false";
	}else{
		$maxLen	= "";
	}
	
	$htmlMask	.= "'".strtolower($mascaras[$i]->getNome())."': { mascara: '".$mascaras[$i]->getMascara()."' $reverse $maxLen},";
}
$htmlMask = substr($htmlMask, 0 , -1);
*/

#################################################################################
## Carregando o template html
#################################################################################
$tpl	= new \AppClass\App\Template();
$tpl->load(HTML_PATH . "/index.html");

#################################################################################
## Define os valores das vari�veis
#################################################################################
$tpl->set('CODE_HTML'			,$menu->getHtml());
$tpl->set('DIVCENTRAL'			,$system->getDivCentral());
$tpl->set('URLINICIAL'			,$urlInicial);

/*$tpl->set('URL_FORM'			,$_SERVER['REQUEST_URI']);
$tpl->set('URLINICIAL'			,$urlInicial);
$tpl->set('IND_TROCAR_SENHA'	,$indTrocarSenha);
$tpl->set('TROCA_SENHA_URL'		,$urlTrocaSenha);
$tpl->set('MASCARAS'			,$htmlMask);
$tpl->set('TITULO'				,$titulo);
*/
#################################################################################
## Por fim exibir a p�gina HTML
#################################################################################
$tpl->show();

?>