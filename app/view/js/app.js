function CriaSpanErro(pMsg) {
	return '<small class="help-block">' + pMsg + '</small>';
}

function CriaSpanInfo(pMsg) {
	//return '<span class="popover-info" title="Aviso" data-rel="zg-popover" data-placement="top" data-trigger="hover" data-content="'+pMsg+'"><i class="fa fa-info-circle blue bigger-150"></i></span><script> $("[data-rel=zg-popover]").popover({html:true,template: \\'<div class="popover" style="width: 240px;" role="tooltip"><div class="arrow"></div><div class="popover-title"></div><div class="popover-content small smaller"></div></div>\\' }).on("show.bs.popover", function () { $(this).data("bs.popover").tip().css("max-width", "600px"); });<\\/script>';
}

function CriaSpanOK() {
	//return '<span><i class="fa fa-check-circle green bigger-150"></i></span>';
}

function CriaToolTipErro(pMsg) {
	return '<i class="fa fa-exclamation-circle" style="color:red" data-toggle="tooltip" data-placement="top" title="' +pMsg+ '"></i>';
}

function toNumber(sText) {
	var numsStr = sText.replace(/[^0-9]/g,'');
    return parseInt(numsStr);
}

function checaRetornoOK (mensagem) {
	if (mensagem.charAt(0) == "0") {
		return true;
	}else{
		return false;
	}
}

function GetMsgRetorno (pStr) {
	var vTemp,vArray;
	
	vTemp	= $.base64.decode(pStr.substring(1));
	vArray	= vTemp.split("|");
	vMsg	= vArray[2];
	
	return vMsg;
}

function getMsgRetornoNoEncode (pStr) {
	var vTemp,vArray;
	
	vTemp	= pStr.substring(1);
	vArray	= vTemp.split("|");
	vMsg	= vArray[1];
	
	return vMsg;
}

function GetCodRetorno (pStr,pIndex) {
	var vTemp,vArray,vIndex;
	
	if (pIndex === undefined) {
		vIndex	= 1;
	}else{
		vIndex  = pIndex;
	}
	
	vTemp	= $.base64.decode(pStr.substring(1));
	vArray	= vTemp.split("|");
	vCod	= vArray[vIndex];
	
	return vCod;
}

function EncodeUrl(pStr) {
	return $.base64.encode(pStr);
}

function DecodeUrl(pStr) {
	return $.base64.decode(pStr);
}