
function CriaSpanErro(pMsg) {
	return '<i class="fa fa-exclamation-circle" style="color:red" data-toggle="tooltip" data-placement="top" title="' +pMsg+ '"></i>';
}

function checaRetornoOK (mensagem) {
	if (mensagem.charAt(0) == "0") {
		return true;
	}else{
		return false;
	}
}

function getMsgRetorno (pStr) {
	var vTemp,vArray;
	
	vTemp	= pStr.substring(1);
	vArray	= vTemp.split("|");
	vMsg	= vArray[1];
	
	return vMsg;
}

function isNumeric(sText) {
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;

   for (i = 0; i < sText.length && IsNumber == true; i++) { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) {
         IsNumber = false;
      }
   }
   return IsNumber;
}

function isTeclaNumber(num) {
    var arrayNumber = [48,49,50,51,52,53,54,55,56,57, 96,97,98,99,100,101,102,103,104,105];

    if (arrayNumber.indexOf(num) == -1) {
		return false;
	}else{
		return true;
	}
}

//valida telefone
function ValidaTelefone(tel){	
	var reFone11 = new RegExp(/\(\d{2}\) \d{5}-\d{4}/);
	var reFone10 = new RegExp(/\(\d{2}\) \d{4}-\d{4}/);
	
	if(tel.match(reFone11)) {
		return true;
	}else if(tel.match(reFone10)) {
		return true;
	}else{
		return false;
	}
    
}