<?php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'includeNoAuth.php');
}else{
	include_once('../includeNoAuth.php');
}

#################################################################################
## Resgata as variáveis postadas
#################################################################################
if (isset($_GET['q']))					$q				= \AppClass\App\Util::antiInjection($_GET["q"]);
if (isset($_GET['codEstado']))			$codEstado		= \AppClass\App\Util::antiInjection($_GET["codEstado"]);
if (isset($_GET['codCidade']))			$codCidade		= \AppClass\App\Util::antiInjection($_GET["codCidade"]);
if (isset($_GET['indHemocentro']))		$indHemocentro	= \AppClass\App\Util::antiInjection($_GET["indHemocentro"]);

if (isset($codEstado) && !empty($codEstado) && isset($indHemocentro) && $indHemocentro == 1) {
	$cidades    = $db->extraiTodos('SELECT E.* FROM `SLADM_ORGANIZACAO` AS O 
					LEFT OUTER JOIN `SLADM_CIDADE` E ON (O.COD_CIDADE = E.CODIGO)
						WHERE O.COD_ESTADO =:cod_uf GROUP BY E.CODIGO
							ORDER BY E.NOME ASC', array(':cod_uf' => $codEstado));
}else if (isset($codCidade) && !empty($codCidade)) {
	$cidades    = $db->extraiTodos('SELECT * FROM `SLADM_CIDADE` WHERE CODIGO =:codigo ORDER BY NOME ASC', 
			array(':codigo' => $codCidade));
}elseif (isset($codEstado) && !empty($codEstado)) {
	$cidades    = $db->extraiTodos('SELECT * FROM `SLADM_CIDADE` WHERE COD_UF =:cod_uf ORDER BY NOME ASC', 
			array(':cod_uf' => $codEstado));
}

$array		= array();

//$js	= ( !empty($indHemocentro) ) ? 'onChange="buscarHemocentro();"' : null;
$select	= ( !empty($codEstado) && !empty($codCidade) ) ? 'selected="selected"' : null;
	
if(!$cidades) {
	//$html  = '<select id="codCidadeID" name="codCidade" disabled data-width="100%" data-live-search="true" class="selectpicker show-tick show-menu-arrow">';
	$html = '<option value="">Nenhuma cidade encontrada para esse Estado.</option>';
//}elseif($cidades && !empty($codCidade)) {
	//$html  = '<select id="codCidadeID" name="codCidade" data-width="100%" data-live-search="true" class="selectpicker show-tick show-menu-arrow">';
}else{
	//$html  = '<select id="codCidadeID" name="codCidade" onChange="buscarHemocentro();" data-width="100%" data-live-search="true" class="selectpicker show-tick show-menu-arrow">';
	$html = '<option value="">Selecione a cidade...</option>';
}

for ($i = 0; $i < sizeof($cidades); $i++) {
	//$array[$i]["id"]		= $cidades[$i]->CODIGO;
	//$array[$i]["text"]		= $cidades[$i]->COD_UF . ' / '.$cidades[$i]->NOME;
	
	$html .= '<option value="'.$cidades[$i]->CODIGO.'" '.$select.'>'.$cidades[$i]->NOME.'</option>';
}

$html .= '</select>';
echo $html;
//echo json_encode($array);
