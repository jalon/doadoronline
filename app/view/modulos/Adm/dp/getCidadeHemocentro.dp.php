<?php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'includeNoAuth.php');
}else{
	include_once('../includeNoAuth.php');
}

#################################################################################
## Resgata as variáveis postadas
#################################################################################
if (isset($_GET['q']))					$q				= \AppClass\App\Util::antiInjection($_GET["q"]);
if (isset($_GET['codEstado']))			$codEstado		= \AppClass\App\Util::antiInjection($_GET["codEstado"]);
if (isset($_GET['codCidade']))			$codCidade		= \AppClass\App\Util::antiInjection($_GET["codCidade"]);

if (isset($codEstado) && !empty($codEstado) && isset($codCidade) && !empty($codCidade)) {
	$hemocentros    = $db->extraiTodos('SELECT * FROM `SLADM_ORGANIZACAO` 
					WHERE COD_ESTADO =:codEstado AND COD_CIDADE =:codCidade AND CODIGO != 1 ORDER BY NOME ASC', 
						array(':codEstado' => $codEstado, ':codCidade' => $codCidade));
}else if (isset($codEstado) && !empty($codEstado) ) {
	$hemocentros    = $db->extraiTodos('SELECT * FROM `SLADM_ORGANIZACAO` 
					WHERE COD_ESTADO =:codEstado AND CODIGO != 1 ORDER BY NOME ASC', 
						array(':codEstado' => $codEstado));
}

$array		= array();

if(!$hemocentros) {
	//$html  = '<select id="codHemocentroID" name="codHemocentro" disabled data-width="100%" data-live-search="true" class="selectpicker show-tick show-menu-arrow">';
	$html = '<option value="0">Nenhum Hemocentro encontrado para essa Cidade.</option>';
}else{
	//$html  = '<select id="codHemocentroID" name="codHemocentro" onChange="buscarDescricao();" data-width="100%" data-live-search="true" class="selectpicker show-tick show-menu-arrow">';
	$html = '<option value="">Selecione o hemocentro...</option>';
}// Onde prefere receber as doações?

for ($i = 0; $i < sizeof($hemocentros); $i++) {
	//$array[$i]["id"]		= $cidades[$i]->CODIGO;
	//$array[$i]["text"]		= $cidades[$i]->COD_UF . ' / '.$cidades[$i]->NOME;
	
	$html .= '<option value='.$hemocentros[$i]->CODIGO.'>'.$hemocentros[$i]->NOME.'</option>';
}

$html .= '</select>';
echo $html;
//echo json_encode($array);
