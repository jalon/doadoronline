<?php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'includeNoAuth.php');
}else{
	include_once('../includeNoAuth.php');
}

#################################################################################
## Resgata as variáveis postadas
#################################################################################
if (isset($_GET['q']))					$q				= \AppClass\App\Util::antiInjection($_GET["q"]);
if (isset($_GET['codHemocentro']))		$codHemocentro	= \AppClass\App\Util::antiInjection($_GET["codHemocentro"]);
if (isset($_GET['codCidade']))			$codCidade		= \AppClass\App\Util::antiInjection($_GET["codCidade"]);

if (isset($codHemocentro) && !empty($codHemocentro)) {
	$hemocentros    = $db->extraiTodos('SELECT O.*, E.NOME AS ESTADO, C.NOME AS CIDADE FROM `SLADM_ORGANIZACAO` AS O 
				LEFT OUTER JOIN `SLADM_ESTADO` E ON (O.COD_ESTADO = E.COD_UF)
				LEFT OUTER JOIN `SLADM_CIDADE` C ON (O.COD_CIDADE = C.CODIGO)
					WHERE O.CODIGO =:codHemocentro ORDER BY O.NOME ASC', array(':codHemocentro' => $codHemocentro));
	$offset = "1";
}else{
	$hemocentros	 = $db->extraiTodos('SELECT O.*, E.NOME AS ESTADO, C.NOME AS CIDADE FROM `SLADM_ORGANIZACAO` AS O
				LEFT OUTER JOIN `SLADM_ESTADO` E ON (O.COD_ESTADO = E.COD_UF)
				LEFT OUTER JOIN `SLADM_CIDADE` C ON (O.COD_CIDADE = C.CODIGO)
					WHERE O.CODIGO !=:codigo AND C.CODIGO =:codCidade', array(':codigo' => 1, ':codCidade' => $codCidade));
	$offset = "1";
}

$array	   = array();
$html	   = null;

if(!$hemocentros) {
	$html .= 'Nenhum Hemocentro encontrado para essa cidade.';
}

$html .= '<h4 class="col-sm-9 col-md-offset-'.$offset.'"><strong>'.$hemocentros[0]->CIDADE.'</strong></h4>';

for ($i = 0; $i < sizeof($hemocentros); $i++) {
	$codigo		 = ($hemocentros[$i]->CODIGO) ? $hemocentros[$i]->CODIGO : null;
	$nome		 = ($hemocentros[$i]->NOME) ? $hemocentros[$i]->NOME : null;
	$descricao	 = ($hemocentros[$i]->DESCRICAO) ? $hemocentros[$i]->DESCRICAO : null;
	$estado		 = ($hemocentros[$i]->ESTADO) ? $hemocentros[$i]->ESTADO : null;
	$cidade		 = ($hemocentros[$i]->CIDADE) ? $hemocentros[$i]->CIDADE : null;
	$endereco	 = ($hemocentros[$i]->ENDERECO) ? $hemocentros[$i]->ENDERECO : null;
	$obs		 = ($hemocentros[$i]->OBSERVACAO) ? '<strong><i class="fa fa-clock-o"></i> Atendimento:</strong> '.$hemocentros[$i]->OBSERVACAO : null;
	
	$telefones	 = $db->extraiTodos('SELECT * FROM `SLADM_ORGANIZACAO_CONTATO` 
						WHERE COD_ORGANIZACAO =:codOrganizacao', array(':codOrganizacao' => $codigo));

	$numero = null;
	for ($j = 0; $j < sizeof($telefones); $j++) {
		if ($j >= 1) $numero .= ' / ';
		$numero .= ($telefones[$j]->NUMERO) ? \AppClass\App\Util::formatPhone( $telefones[$j]->NUMERO ) : null;
	}
	
	$html .= '<div class="form-group col-sm-12">
				<div class="notes col-sm-9 col-md-offset-'.$offset.'">
					<p style="text-align: justify">
						<strong>'.$nome.' - '.$descricao.'</strong>
					</p>
					<p style="text-align: justify">
						<strong><i class="fa fa-map-marker"></i> Endereço:</strong> '.$endereco.'
					</p>
					<p>'.$obs.'</p>
					<p style="text-align: justify">
						<strong><i class="fa fa-phone"></i> Telefone:</strong> '.$numero.'
					</p>
				 </div>
			  </div>';
}

echo $html;
//echo json_encode($array);
