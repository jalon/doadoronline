<?php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'include.php');
}else{
 	include_once('../include.php');
}
 
#################################################################################
## Resgata os parâmetros passados pelo formulário
#################################################################################
if (isset($_POST['codParceiro']))		$codParceiro		= \AppClass\App\Util::antiInjection($_POST['codParceiro']);
if (isset($_POST['ident']))				$ident				= \AppClass\App\Util::antiInjection($_POST['ident']);
if (isset($_POST['nome']))				$nome				= \AppClass\App\Util::antiInjection($_POST['nome']);
if (isset($_POST['codEstado']))			$codEstado			= \AppClass\App\Util::antiInjection($_POST['codEstado']);
if (isset($_POST['codCidade']))			$codCidade			= \AppClass\App\Util::antiInjection($_POST['codCidade']);
if (isset($_POST['endereco']))			$endereco			= \AppClass\App\Util::antiInjection($_POST['endereco']);
if (isset($_POST['observacao']))		$observacao			= \AppClass\App\Util::antiInjection($_POST['observacao']);

#################################################################################
## Limpar a variável de erro
#################################################################################
$err	= false;

#################################################################################
## Fazer validação dos campos
#################################################################################
/** IDENTIFICACAO **/
if ((empty($ident))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Campo IDENTIFICAÇÂO é obrigatório"));
	$err	= 1;
}

/** NOME **/
if ((empty($nome))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Campo NOME é obrigatório"));
	$err	= 1;
}

/*
$oCargo	= $em->getRepository('Entidades\ZgrhuFuncionarioCargo')->findOneBy(array('descricao' => $cargo));

if($oCargo != null && ($oCargo->getCodigo() != $codParceiro)){
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Este CARGO já foi cadastrado!"));
	$err	= 1;
}*/

if ($err != null) {
	echo '1'.\AppClass\App\Util::encodeUrl('||'.htmlentities($err));
 	exit;
}
 
#################################################################################
## Salvar no banco
#################################################################################
try {
	$params = array();
	
	$params += array('ident' => $ident,
			':nome' => $nome,
			':codEstado' => $codEstado,
			':codCidade' => $codCidade,
			':endereco' => $endereco,
			':observacao' => $observacao
	);
	
	if (isset($codParceiro) && (!empty($codParceiro))) {
		$params += array(':codigo' => $codParceiro);
 		$oParceiro	= $db->Executa('UPDATE `SLADM_ORGANIZACAO` SET `IDENTIFICACAO`=:ident, `NOME`=:nome,`COD_ESTADO`=:codEstado,`COD_CIDADE`=:codCidade,`ENDERECO`=:endereco,`OBSERVACAO`=:observacao WHERE `CODIGO`=:codigo', 
 				$params);
 	}else{
 		$oParceiro	= $db->Executa('INSERT INTO `SLADM_ORGANIZACAO`(`IDENTIFICACAO`, `NOME`, `COD_ESTADO`, `COD_CIDADE`, `ENDERECO`, `OBSERVACAO`) VALUES (:ident,:nome,:codEstado,:codCidade,:endereco,:observacao)', 
 				$params);
 	}
 		
} catch (\Exception $e) {
 	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$e->getMessage());
 	echo '1'.\AppClass\App\Util::encodeUrl('||'.htmlentities($e->getMessage()));
 	exit;
}
 
$system->criaAviso(\AppClass\App\Aviso\Tipo::INFO,"Informações salvas com sucesso");
echo '0'.\AppClass\App\Util::encodeUrl('|'.$oParceiro);