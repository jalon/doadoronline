<?php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'include.php');
}else{
	include_once('../include.php');
}

#################################################################################
## Resgata os parâmetros passados pelo formulario
#################################################################################
if (isset($_POST['codParceiro'])) 		$codParceiro		= \AppClass\App\Util::antiInjection($_POST['codParceiro']);

#################################################################################
## Limpar a variável de erro
#################################################################################
$err	= false;

#################################################################################
## Verificar se existe e excluir
#################################################################################
try {

	if (!isset($codParceiro) || (!$codParceiro)) {
		$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans('Parâmetro não informado'));
		die('1'.\AppClass\App\Util::encodeUrl('||'));
	}
	
	$oParceiro	 = $db->Executa('DELETE FROM `SLADM_ORGANIZACAO` WHERE CODIGO = :codigo', array(':codigo' => $codParceiro));
	
	/*if (!$oParceiro) {
		$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans('Usuário não encontrado'));
		die('1'.\AppClass\App\Util::encodeUrl('||'));
	}*/
	
} catch (\Exception $e) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$e->getMessage());
	die('1'.\AppClass\App\Util::encodeUrl('||'));
	exit;
}

$system->criaAviso(\AppClass\App\Aviso\Tipo::INFO,$tr->trans("Parceiro excluído com sucesso"));
echo '0'.\AppClass\App\Util::encodeUrl('|'.$oParceiro.'|');