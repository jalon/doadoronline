<?php
################################################################################
# Includes
################################################################################
if (defined ( 'DOC_ROOT' )) {
	include_once (DOC_ROOT . 'include.php');
} else {
	include_once ('../include.php');
}

################################################################################
# Resgata a variável ID que está criptografada
################################################################################
if (isset ( $_GET ['id'] )) {
	$id = \AppClass\App\Util::antiInjection ( $_GET ["id"] );
} elseif (isset ( $_POST ['id'] )) {
	$id = \AppClass\App\Util::antiInjection ( $_POST ["id"] );
} elseif (isset ( $id )) {
	$id = \AppClass\App\Util::antiInjection ( $id );
} else {
	\AppClass\App\Erro::halt ( 'Falta de Parâmetros' );
}

################################################################################
# Descompacta o ID
################################################################################
\AppClass\App\Util::descompactaId ( $id );

################################################################################
# Verifica se o usuário tem permissão no menu
################################################################################
$system->checaPermissao ( $_codMenu_ );

################################################################################
# Resgata as informações do banco
################################################################################
if ($codParceiro) {
	try {
		$info	 = \AppClass\Adm\Organizacao::buscaPorCodigo($codParceiro);
	} catch ( \Exception $e ) {
		\AppClass\App\Erro::halt ( $e->getMessage () );
	}
	
	$ident			 = ($info->IDENTIFICACAO) ? $info->IDENTIFICACAO : null;
	$nome		 	 = ($info->NOME) ? $info->NOME : null;
	$codEstado		 = ($info->COD_ESTADO) ? $info->COD_ESTADO : null;
	$codCidade		 = ($info->COD_CIDADE) ? $info->COD_CIDADE : null;
	$endereco		 = ($info->ENDERECO) ? $info->ENDERECO : null;
	$observacao		 = ($info->OBSERVACAO) ? $info->OBSERVACAO : null;
	$hidden			 = "";
} else {
	$ident	  	 	 = null;
	$nome	 	 	 = null;
	$codEstado		 = null;
	$codCidade		 = null;
	$endereco		 = null;
	$observacao		 = null;
	$hidden			 = "hidden";
}

################################################################################
# Select de Estado
################################################################################
try {
	$aEstado	 = $db->extraiTodos('SELECT * FROM `SLADM_ESTADO`', array());
	$oEstado 	 = $system->geraHtmlCombo ( $aEstado, 'COD_UF', 'NOME', $codEstado, 'Selecione o Estado');
} catch ( \Exception $e ) {
	\AppClass\App\Erro::halt ( $e->getMessage (), __FILE__, __LINE__ );
}

################################################################################
# Url Voltar
################################################################################
$urlVoltar = ROOT_URL . "/Adm/parceiroLis.php?id=" . $id;

################################################################################
# Url Novo
################################################################################
$uid = \AppClass\App\Util::encodeUrl ( '_codMenu_=' . $_codMenu_ . '&_icone_=' . $_icone_ . '&codParceiro=' );
$urlNovo = ROOT_URL . "/Adm/parceiroCad.php?id=" . $uid;

################################################################################
# Carregando o template html
################################################################################
$tpl = new \AppClass\App\Template ();
$tpl->load ( \AppClass\App\Util::getCaminhoCorrespondente ( __FILE__, \AppClass\App\ZWS::EXT_HTML ) );

################################################################################
# Define os valores das variáveis
################################################################################
$tpl->set ( 'URL_FORM'			   , $_SERVER ['SCRIPT_NAME'] );
$tpl->set ( 'URLVOLTAR'			   , $urlVoltar );
$tpl->set ( 'URLNOVO'		 	   , $urlNovo );
$tpl->set ( 'ID'				   , $id );
$tpl->set ( 'COD_PARCEIRO'		   , $codParceiro);
$tpl->set ( 'IDENTIFICACAO'		   , $ident);
$tpl->set ( 'NOME'				   , $nome);
$tpl->set ( 'COD_ESTADO'		   , $oEstado);
$tpl->set ( 'COD_CIDADE'	 	   , $codCidade);
$tpl->set ( 'ENDERECO'		 	   , $endereco);
$tpl->set ( 'OBSERVACAO'		   , $observacao);
$tpl->set ( 'HIDDEN'		 	   , $hidden);

$tpl->set ( 'DP', \AppClass\App\Util::getCaminhoCorrespondente ( __FILE__, \AppClass\App\ZWS::EXT_DP, \AppClass\App\ZWS::CAMINHO_RELATIVO ) );
################################################################################
# Por fim exibir a página HTML
################################################################################
$tpl->show ();

