<?php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'include.php');
}else{
	include_once('../include.php');
}

#################################################################################
## Resgata a variável ID que está criptografada
#################################################################################
if (isset($_GET['id'])) {
	$id = \AppClass\App\Util::antiInjection($_GET["id"]);
}elseif (isset($_POST['id'])) {
	$id = \AppClass\App\Util::antiInjection($_POST["id"]);
}elseif (isset($id)) 	{
	$id = \AppClass\App\Util::antiInjection($id);
}else{
	\AppClass\App\Erro::halt('Falta de Parâmetros');
}

#################################################################################
## Descompacta o ID
#################################################################################
\AppClass\App\Util::descompactaId($id);

#################################################################################
## Verifica se o usuário tem permissão no menu
#################################################################################
$system->checaPermissao($_codMenu_);

#################################################################################
## Resgata os parâmetros passados pelo formulario de pesquisa
#################################################################################
if (!isset($codParceiro)) 		{
	\AppClass\App\Erro::halt($tr->trans('Falta de Parâmetros').' (COD_PARCEIRO)');
}

#################################################################################
## Resgata as informações do banco
#################################################################################
try {
	$info	 = $db->extraiPrimeiro('SELECT * FROM `SLADM_ORGANIZACAO` WHERE CODIGO = :codigo', array(':codigo' => $codParceiro));
	
	if (!$info) 	{
		\AppClass\App\Erro::halt($tr->trans('Parceiro não existe'));
	}
	
	//$infoFuncao = 	$em->getRepository ( 'Entidades\ZgrhuFuncionarioFuncao' )->findOneBy (array ('codCargo em ' => $codCargo));
	
	/*if (\AppClass\Rhu\Cargo::estaEmUso($codCargo)) {
		$podeRemover	= 'disabled';
		$mensagem		= $tr->trans('Cargo "%s" está em uso e não pode ser excluído (FUNCIONARIO)',array('%s' => $info->getDescricao()));
		$classe			= "text-danger";
	}else{*/
		$podeRemover	= null;
		$mensagem		= $tr->trans('Deseja realmente excluir o parceiro').': <em><b>'.$info->NOME.'</b></em> ?';
		$classe			= "text-warning";
	//}
	
} catch (\Exception $e) {
	\AppClass\App\Erro::halt($e->getMessage());
}

#################################################################################
## Url do Botão Voltar
#################################################################################
$urlVoltar			= ROOT_URL."/Adm/parceiroLis.php?id=".$id;

#################################################################################
## Carregando o template html
#################################################################################
$tpl	= new \AppClass\App\Template();
$tpl->load(HTML_PATH . '/templateExc.html');

#################################################################################
## Define os valores das variáveis
#################################################################################
$tpl->set('URL_FORM'			,$_SERVER['SCRIPT_NAME']);
$tpl->set('URLVOLTAR'			,$urlVoltar);
$tpl->set('PODE_REMOVER'		,$podeRemover);
$tpl->set('TITULO'				,$tr->trans('Exclusão de Parceiro'));
$tpl->set('ID'					,$id);
$tpl->set('TEXTO'				,$mensagem);
$tpl->set('MENSAGEM'			,$mensagem);
$tpl->set('CLASSE'				,$classe);
$tpl->set('VAR'					,'codParceiro');
$tpl->set('VAR_VALUE'			,$info->CODIGO);
$tpl->set('DP'					,\AppClass\App\Util::getCaminhoCorrespondente(__FILE__,\AppClass\App\ZWS::EXT_DP,\AppClass\App\ZWS::CAMINHO_RELATIVO));


#################################################################################
## Por fim exibir a página HTML
#################################################################################
$tpl->show();

