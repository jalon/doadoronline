<?php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'include.php');
}else{
	include_once('../include.php');
}

#################################################################################
## Resgata a variável ID que está criptografada
#################################################################################
if (isset($_GET['id'])) {
	$id = \AppClass\App\Util::antiInjection($_GET["id"]);
}elseif (isset($_POST['id'])) {
	$id = \AppClass\App\Util::antiInjection($_POST["id"]);
}elseif (isset($id)) 	{
	$id = \AppClass\App\Util::antiInjection($id);
}else{
	\AppClass\App\Erro::halt('Falta de Parâmetros');
}

#################################################################################
## Descompacta o ID
#################################################################################
\AppClass\App\Util::descompactaId($id);

#################################################################################
## Verifica se o usuário tem permissão no menu
#################################################################################
$system->checaPermissao($_codMenu_);

#################################################################################
## Resgata a url desse script
#################################################################################
$url		= ROOT_URL . '/Adm/'. basename(__FILE__);

#################################################################################
## Resgata os dados do grid
#################################################################################
try {
	$parceiro	 = \AppClass\Adm\Organizacao::listarTodas();
} catch (\Exception $e) {
	\AppClass\App\Erro::halt($e->getMessage());
}
	
#################################################################################
## Cria o objeto do Grid (bootstrap)
#################################################################################
$grid			= \AppClass\App\Grid::criar(\AppClass\App\Grid\Tipo::TP_BOOTSTRAP,"");
$grid->adicionaTexto($tr->trans('IDENTIFICAÇÃO'),	 		10, $grid::CENTER	,'IDENTIFICACAO');
$grid->adicionaTexto($tr->trans('NOME'),	 				10, $grid::CENTER	,'NOME');
$grid->adicionaTexto($tr->trans('ESTADO'),	 				10, $grid::CENTER	,'ESTADO');
$grid->adicionaTexto($tr->trans('CIDADE'),	 				10, $grid::CENTER	,'CIDADE');
$grid->adicionaTexto($tr->trans('ENDEREÇO'),	 			15, $grid::CENTER	,'ENDERECO');
$grid->adicionaTexto($tr->trans('OBSERVAÇÃO'),	 			15, $grid::CENTER	,'OBSERVACAO');
$grid->adicionaTexto($tr->trans('CONTATO'),	 				10, $grid::CENTER	,'');
$grid->adicionaBotao(\AppClass\App\Grid\Coluna\Botao::MOD_EDIT);
$grid->adicionaBotao(\AppClass\App\Grid\Coluna\Botao::MOD_REMOVE);
$grid->importaDadosDoctrine($parceiro);

#################################################################################
## Popula os valores dos botões
#################################################################################
for ($i = 0; $i < sizeof($parceiro); $i++) {
	$uid		= \AppClass\App\Util::encodeUrl('_codMenu_='.$_codMenu_.'&_icone_='.$_icone_.'&codParceiro='.$parceiro[$i]->CODIGO.'&url='.$url);
	
	$grid->setValorCelula($i,6,\AppClass\App\Util::formatPhone( $parceiro[$i]->NUMERO ) );
	
	$grid->setUrlCelula($i,7,ROOT_URL.'/Adm/parceiroCad.php?id='.$uid);
	$grid->setUrlCelula($i,8,'javascript:AbreModal(\'/Adm/parceiroExc.php?id='.$uid.'\');"');
}

#################################################################################
## Gerar o código html do grid
#################################################################################
try {
	$htmlGrid	= $grid->getHtmlCode();
} catch (\Exception $e) {
	\AppClass\App\Erro::halt($e->getMessage());
}

#################################################################################
## Gerar a url de adicão
#################################################################################
$urlAdd			= ROOT_URL.'/Adm/parceiroCad.php?id='.\AppClass\App\Util::encodeUrl('_codMenu_='.$_codMenu_.'&_icone_='.$_icone_.'&codParceiro=');

#################################################################################
## Carregando o template html
#################################################################################
$tpl	= new \AppClass\App\Template();
$tpl->load(HTML_PATH . 'templateLis.html');

#################################################################################
## Define os valores das variáveis
#################################################################################
$tpl->set('GRID'			,$htmlGrid);
$tpl->set('NOME'			,$tr->trans('Parceiros'));
$tpl->set('URLADD'			,$urlAdd);
$tpl->set('IC'				,$_icone_);

#################################################################################
## Por fim exibir a página HTML
#################################################################################
$tpl->show();
