<?php
use AppClass\App\AppClass;
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'includeNoAuth.php');
}else{
	include_once('../includeNoAuth.php');
}

#################################################################################
## Resgata as variáveis postadas
#################################################################################
if (isset($_GET['q']))					$q			= \AppClass\App\Util::antiInjection($_GET["q"]);
if (isset($_GET['codSangue']))			$codSangue	= \AppClass\App\Util::antiInjection($_GET["codSangue"]);
if (isset($_GET['codEstado']))			$codEstado	= \AppClass\App\Util::antiInjection($_GET["codEstado"]);

if (isset($codSangue) && isset($codEstado)) {
	$sangues    = \AppClass\Hem\Sangue::listaUsuariosPodeDoar($codSangue, $codEstado);
}

$html = null;

if ( sizeof($sangues) != 0 ){
	$html .= '<table border="1" class="table table-hover table-bordered">
				  <thead>
				    <tr>
					  <th></th>
				      <th>#</th>
					  <th>Cidade</th>
				      <th>Sangue</th>
					  <th>Ultima Doação</th>
				    </tr>
				  </thead>
					<tbody>';
	
	for ($i = 0; $i < sizeof($sangues); $i++) {	
	$html .="<tr>
			  <td> <input type='checkbox' name='codUsuario[]' value='{$sangues[$i]->COD_USUARIO}'/> </td>
		      <td>#{$sangues[$i]->CODIGO}</td>
		      <td id='codCidade' value='{$sangues[$i]->COD_CIDADE}'>{$sangues[$i]->CIDADE}</td>
		      <td>{$sangues[$i]->TIPO_SANGUE}</td>
		      <td>".\AppClass\App\Util::formatData($system->config['data']['dateFormat'], $sangues[$i]->DATA_ULT_DOACAO)."</td>
		    </tr>";
	}
	
	$html .= '</tbody></table>';
} else {
	$html .= '<br /><strong>Não foi encontrado nenhum <i>Doador</i> compatível para sua região!';
}

$array	= array();

$array["quantidade"]	= sizeof($sangues);
$array["text"]			= $html;

echo json_encode($array);
