<?php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'include.php');
}else{
 	include_once('../include.php');
}
 
#################################################################################
## Resgata os parâmetros passados pelo formulário
#################################################################################
if (isset($_POST['codUsuario']))		$codUsuario			= \AppClass\App\Util::antiInjection($_POST['codUsuario']);
if (isset($_POST['assunto']))			$assunto			= \AppClass\App\Util::antiInjection($_POST['assunto']);
if (isset($_POST['mensagem']))			$mensagem			= \AppClass\App\Util::antiInjection($_POST['mensagem']);

#################################################################################
## Limpar a variável de erro
#################################################################################
$err		  = false;

#################################################################################
## Fazer validação dos campos
#################################################################################
/** COD_USUARIO **/
if ((empty($codUsuario))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("COD_USUARIO é obrigatório"));
	$err	= 1;
}

/** Assunto **/
if ((empty($assunto))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Campo ASSUNTO é obrigatório"));
	$err	= 1;
}

/** MENSAGEM **/
if ((empty($mensagem))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Campo MENSAGEM é obrigatório"));
	$err	= 1;
}

if ($err != null) {
	echo '1'.\AppClass\App\Util::encodeUrl('||'.htmlentities($err));
 	exit;
}
 
#################################################################################
## Salvar no banco
#################################################################################
try {

	#################################################################################
	## Gerar a notificação
	#################################################################################
	$notificacao	= new \AppClass\App\Notificacao(\AppClass\App\Notificacao::TIPO_MENSAGEM_TEXTO, \AppClass\App\Notificacao::TIPO_DEST_USUARIO, \AppClass\App\Notificacao::MENSAGEM);
	$notificacao->setAssunto( $assunto );
	$notificacao->setMensagem( $mensagem );
	
	$notificacao->setCodRemetente( $system->getCodUsuario() );	
	$notificacao->associaUsuario( $codUsuario );
		
	//$notificacao->enviaEmail();
	$notificacao->enviaSistema();
	$notificacao->salva();
	
} catch (\Exception $e) {
 	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$e->getMessage());
 	echo '1'.\AppClass\App\Util::encodeUrl('||'.htmlentities($e->getMessage()));
 	exit;
}
 
$system->criaAviso(\AppClass\App\Aviso\Tipo::INFO,"Mensagem enviada com sucesso.");