<?php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'include.php');
}else{
 	include_once('../include.php');
}
 
#################################################################################
## Resgata os parâmetros passados pelo formulário
#################################################################################
if (isset($_POST['nome']))				$nome				= \AppClass\App\Util::antiInjection($_POST['nome']);
if (isset($_POST['telefone']))			$telefone			= \AppClass\App\Util::antiInjection($_POST['telefone']);
if (isset($_POST['dataNascimento']))	$dataNascimento		= \AppClass\App\Util::antiInjection($_POST['dataNascimento']);
if (isset($_POST['codTipoSangue']))		$tipoSangue			= \AppClass\App\Util::antiInjection($_POST['codTipoSangue']);
if (isset($_POST['codSexo']))			$codSexo			= \AppClass\App\Util::antiInjection($_POST['codSexo']);

if (isset($_POST['antigaSenha']))		$antigaSenha		= \AppClass\App\Util::antiInjection($_POST['antigaSenha']);
if (isset($_POST['novaSenha']))			$novaSenha			= \AppClass\App\Util::antiInjection($_POST['novaSenha']);
if (isset($_POST['confirmeSenha']))		$confirmeSenha		= \AppClass\App\Util::antiInjection($_POST['confirmeSenha']);

#################################################################################
## Limpar a variável de erro
#################################################################################
$alterarSenha = false;
$err		  = false;

#################################################################################
## Fazer validação dos campos
#################################################################################
/** NOME **/
if ((empty($nome))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Campo NOME é obrigatório"));
	$err	= 1;
}

/** Telefone **/
if ((empty($telefone))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Campo TELEFONE é obrigatório"));
	$err	= 1;
}

/** Data Nascimetno **/
if ((empty($dataNascimento))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Campo DATA NASCIMENTO é obrigatório"));
	$err	= 1;
}

/** Tipo Sangue **/
if ((empty($tipoSangue))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Campo TIPO SANGUINEO é obrigatório"));
	$err	= 1;
}

/** CodSexo **/
if ((empty($codSexo))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Campo SEXO é obrigatório"));
	$err	= 1;
}

/** Senha **/
if ( !empty($antigaSenha) && empty($novaSenha) ) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("NOVA SENHA deve ser preenchida!"));
	$err	= 1;
}else if ( !empty($antigaSenha) && !empty($novaSenha) && empty($confirmeSenha)) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("CONFIRME SENHA deve ser preenchida!"));
	$err	= 1;
}else if (!empty($antigaSenha) && !empty($novaSenha) && !empty($confirmeSenha)) {
	$info	 = $db->extraiPrimeiro('SELECT SENHA, USUARIO FROM `SLSEG_USUARIO` WHERE CODIGO = :codigo', array(':codigo' => $system->getCodUsuario()));
	
	if( $antigaSenha != $info->SENHA ){
		$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("SENHA antiga incorreta!"));
		$err	= 1;
	}else if( $novaSenha != $confirmeSenha ){
		$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("SENHAS não conferem!"));
		$err	= 1;
	}else{
		$usuario	  = $info->USUARIO;
		$alterarSenha = true;
	}
}

if ($err != null) {
	echo '1'.\AppClass\App\Util::encodeUrl('||'.htmlentities($err));
 	exit;
}
 
#################################################################################
## Salvar no banco
#################################################################################
try {
	$params = array();
	
	if (!empty($dataNascimento)) {
		$dtNasc		= $data = implode("-",array_reverse(explode("/", $dataNascimento)));
	}else{
		$dtNasc		= null;
	}
	
	$params += array(':codigo' => $system->getCodPessoa(),
			':nome' => $nome,
			':telefone' => $telefone,
			':dataNascimento' => $dtNasc,
			':tipoSangue' => $tipoSangue,
			':codSexo' => $codSexo
	);
	
	$oPerfil	= $db->Executa('UPDATE `SLSEG_PESSOA` SET `NOME`=:nome,`TELEFONE`=:telefone,`DAT_NASCIMENTO`=:dataNascimento,`TIPO_SANGUINEO`=:tipoSangue,`COD_SEXO`=:codSexo WHERE `CODIGO`=:codigo',
			$params);
		
 	if($alterarSenha == true){
 		$senhaCrypt = \AppClass\App\Crypt::crypt($usuario, $novaSenha);
 	
 		$oSenha		= $db->Executa('UPDATE `SLSEG_USUARIO` SET `SENHA`=:senha WHERE `CODIGO`=:codigo',
 			array('codigo' => $system->getCodUsuario(), 'senha' => $senhaCrypt));
 	}
 		
} catch (\Exception $e) {
 	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$e->getMessage());
 	echo '1'.\AppClass\App\Util::encodeUrl('||'.htmlentities($e->getMessage()));
 	exit;
}
 
$system->criaAviso(\AppClass\App\Aviso\Tipo::INFO,"Informações salvas com sucesso.");
echo '0'.\AppClass\App\Util::encodeUrl('|'.$oPerfil);