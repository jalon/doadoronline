<?php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'include.php');
}else{
 	include_once('../include.php');
}
 
#################################################################################
## Resgata os parâmetros passados pelo formulário
#################################################################################
if (isset($_POST['codUsuario']))		$codUsuario			= \AppClass\App\Util::antiInjection($_POST['codUsuario']);
if (isset($_POST['nome']))				$nome				= \AppClass\App\Util::antiInjection($_POST['nome']);
if (isset($_POST['email']))				$email				= \AppClass\App\Util::antiInjection($_POST['email']);
if (isset($_POST['telefone']))			$telefone			= \AppClass\App\Util::antiInjection($_POST['telefone']);
if (isset($_POST['dataNascimento']))	$dataNascimento		= \AppClass\App\Util::antiInjection($_POST['dataNascimento']);
if (isset($_POST['codSexo']))			$codSexo			= \AppClass\App\Util::antiInjection($_POST['codSexo']);
if (isset($_POST['codTipoSangue']))		$codTipoSangue		= \AppClass\App\Util::antiInjection($_POST['codTipoSangue']);
if (isset($_POST['hemocentro']))		$hemocentro			= \AppClass\App\Util::antiInjection($_POST['hemocentro']);

#################################################################################
## Limpar a variável de erro
#################################################################################
$err	= false;

#################################################################################
## Fazer validação dos campos
#################################################################################
/** Cargo **/
if ((empty($nome))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Campo NOME é obrigatório"));
	$err	= 1;
}

$info	 = $db->extraiPrimeiro('SELECT * FROM `SLSEG_PESSOA` WHERE EMAIL = :email', array(':email' => $email));

if($info != null && (isset($info->CODIGO) != $codUsuario)){
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Este EMAIL já foi cadastrado!"));
	$err	= 1;
}

if ($err != null) {
	echo '1'.\AppClass\App\Util::encodeUrl('||'.htmlentities($err));
 	exit;
}
 
#################################################################################
## Salvar no banco
#################################################################################
try {
	$db->transaction();
	
	$params = array();
	
	if (!empty($dataNascimento)) {
		$dtNasc		= $data = implode("-",array_reverse(explode("/",$dataNascimento)));
	}else{
		$dtNasc		= null;
	}
	
	$params += array(':nome' => $nome,
			':email' => $email,
			':telefone' => $telefone,
			':dataNascimento' => $dtNasc,
			':codSexo' => $codSexo,
			':tipoSangue' => $codTipoSangue,
			':hemocentro' => $hemocentro
	);
	
	if (isset($codUsuario) && (!empty($codUsuario))) {
		$params += array(':codigo' => $codUsuario);
 		$oPessoa	= $db->Executa('UPDATE `SLSEG_PESSOA` SET `NOME`=:nome,`SOBRENOME`=:sobrenome,`EMAIL`=:email,`TELEFONE`=:telefone,`DAT_NASCIMENTO`=:dataNascimento,`COD_SEXO`=:codSexo,`TIPO_SANGUINEO`=:tipoSangue,`HEMOCENTRO`=:hemocentro WHERE `CODIGO`=:codigo', 
 				$params);
 	}else{
 		$oPessoa	= $db->Executa('INSERT INTO `SLSEG_PESSOA`(`NOME`, `SOBRENOME`, `EMAIL`, `TELEFONE`, `DAT_NASCIMENTO`, `COD_SEXO`, `TIPO_SANGUINEO`, `HEMOCENTRO`) VALUES (:nome,:sobrenome,:email,:telefone,:dataNascimento,:codSexo,:tipoSangue,:hemocentro)', 
 				$params);
 			
 		$senha		= \AppClass\App\Util::_geraSenha();
 		$senhaCrypt = \AppClass\App\Crypt::crypt($email, $senha);
 		
 		/** ### Cricacao do usuario ### **/
 		$paramUsu = array(':codOrg' => $system->getCodOrganizacao(),
 				':codPessoa' => $oPessoa,
 				':usuario' => $email,
 				':senha' => $senhaCrypt,
 				':codStatus' => 0,
 				':codPerfil' => "ADM"
 		);
 		
 		$oUsuario	= $db->Executa('INSERT INTO `SLSEG_USUARIO`(`COD_ORGANIZACAO`, `COD_PESSOA`, `USUARIO`, `SENHA`, `COD_STATUS`, `COD_PERFIL`) VALUES (:codOrg,:codPessoa,:usuario,:senha,:codStatus,:codPerfil)',
 				$paramUsu);
 	}
 	
 	$db->commit();
 	
} catch (\Exception $e) {
	$db->rollBack();
	
 	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO, "Ocorreu um erro ao salvar o usuário. " . $e->getMessage());
 	echo '1'.\AppClass\App\Util::encodeUrl('||'.htmlentities($e->getMessage()));
 	exit;
}
 
$system->criaAviso(\AppClass\App\Aviso\Tipo::INFO,"Informações salvas com sucesso");
echo '0'.\AppClass\App\Util::encodeUrl('|'.$oPessoa);