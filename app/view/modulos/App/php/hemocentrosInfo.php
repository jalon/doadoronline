<?php
################################################################################
# Includes
################################################################################
if (defined ( 'DOC_ROOT' )) {
	include_once (DOC_ROOT . 'include.php');
} else {
	include_once ('../include.php');
}

################################################################################
# Resgata a variável ID que está criptografada
################################################################################
if (isset ( $_GET ['id'] )) {
	$id = \AppClass\App\Util::antiInjection ( $_GET ["id"] );
} elseif (isset ( $_POST ['id'] )) {
	$id = \AppClass\App\Util::antiInjection ( $_POST ["id"] );
} elseif (isset ( $id )) {
	$id = \AppClass\App\Util::antiInjection ( $id );
} else {
	\AppClass\App\Erro::halt ( 'Falta de Parâmetros' );
}

################################################################################
# Descompacta o ID
################################################################################
\AppClass\App\Util::descompactaId ( $id );

################################################################################
# Verifica se o usuário tem permissão no menu
################################################################################
$system->checaPermissao ( $_codMenu_ );

################################################################################
# Resgata as informações do banco
################################################################################
try {
	$info	 = $db->extraiTodos('SELECT O.*, E.NOME AS ESTADO, C.NOME AS CIDADE FROM `SLADM_ORGANIZACAO` AS O
				LEFT OUTER JOIN `SLADM_ESTADO` E ON (O.COD_ESTADO = E.COD_UF) 
				LEFT OUTER JOIN `SLADM_CIDADE` C ON (O.COD_CIDADE = C.CODIGO) 	
					WHERE O.CODIGO !=:codigo', array(':codigo' => 1));
} catch ( \Exception $e ) {
	\AppClass\App\Erro::halt ( $e->getMessage () );
}

$html = null;
for ($i = 0; $i < sizeof($info); $i++) {
	$codigo		 = ($info[$i]->CODIGO) ? $info[$i]->CODIGO : null;
	$nome		 = ($info[$i]->NOME) ? $info[$i]->NOME : null;
	$descricao	 = ($info[$i]->DESCRICAO) ? $info[$i]->DESCRICAO : null;
	$estado		 = ($info[$i]->ESTADO) ? $info[$i]->ESTADO : null;
	$cidade		 = ($info[$i]->CIDADE) ? $info[$i]->CIDADE : null;
	$endereco	 = ($info[$i]->ENDERECO) ? $info[$i]->ENDERECO : null;
	$obs		 = ($info[$i]->OBSERVACAO) ? '<strong>Observação:</strong> '.$info[$i]->OBSERVACAO : null;
	
	$telefones	 = $db->extraiTodos('SELECT * FROM `SLADM_ORGANIZACAO_CONTATO` 
						WHERE COD_ORGANIZACAO =:codOrganizacao', array(':codOrganizacao' => $codigo));

	$numero = null;
	for ($j = 0; $j < sizeof($telefones); $j++) {
		if ($j >= 1) $numero .= ' / ';
		$numero .= ($telefones[$j]->NUMERO) ? \AppClass\App\Util::formatPhone( $telefones[$j]->NUMERO ) : null;
	}
	
	$html .= '<div class="form-group col-sm-12">
				<div class="notes col-sm-9 col-md-offset-1">
					<h4><strong>'.$estado.'</strong>  / '.$cidade.'</h4>
					<p style="text-align: justify">
						'.$nome.' - '.$descricao.'
					</p>
					<p style="text-align: justify">
						<strong>Endereço:</strong> '.$endereco.'
					</p>
					<p>'.$obs.'</p>
					<p style="text-align: justify">
						<strong>Telefone:</strong> '.$numero.'
					</p>
				 </div>
			  </div>';
	
	
}

################################################################################
# Select de Estado
################################################################################
try {
	$aEstado	 = $db->extraiTodos('SELECT E.* FROM `SLADM_ORGANIZACAO` AS O 
					LEFT OUTER JOIN `SLADM_ESTADO` E ON (O.COD_ESTADO = E.COD_UF)
						GROUP BY O.COD_ESTADO', array());
	$oEstado	 = $system->geraHtmlCombo ( $aEstado, 'COD_UF', 'NOME', null, 'Selecione o estado');
} catch ( \Exception $e ) {
	\AppClass\App\Erro::halt ( $e->getMessage (), __FILE__, __LINE__ );
}

################################################################################
# Carregando o template html
################################################################################
$tpl = new \AppClass\App\Template ();
$tpl->load ( \AppClass\App\Util::getCaminhoCorrespondente ( __FILE__, \AppClass\App\ZWS::EXT_HTML ) );

################################################################################
# Define os valores das variáveis
################################################################################
$tpl->set ( 'URL_FORM'			   , $_SERVER ['SCRIPT_NAME'] );
$tpl->set ( 'ID'				   , $id );
$tpl->set ( 'COD_ESTADO'		   , $oEstado);
$tpl->set ( 'DISABLED'			   , "disabled");
$tpl->set ( 'HTML'				   , $html);

$tpl->set ( 'DP', \AppClass\App\Util::getCaminhoCorrespondente ( __FILE__, \AppClass\App\ZWS::EXT_DP, \AppClass\App\ZWS::CAMINHO_RELATIVO ) );
################################################################################
# Por fim exibir a página HTML
################################################################################
$tpl->show ();

