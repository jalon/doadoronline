<?php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'include.php');
}else{
 	include_once('../include.php');
}
 
#################################################################################
## Resgata os parâmetros passados pelo formulário
#################################################################################
//if (isset($_POST['codUsuario']))		$codUsuario			= \AppClass\App\Util::antiInjection($_POST['codUsuario']);
if (isset($_POST['pacienteTipo']))		$pacienteTipo		= \AppClass\App\Util::antiInjection($_POST['pacienteTipo']);
if (isset($_POST['nome']))				$nome				= \AppClass\App\Util::antiInjection($_POST['nome']);
if (isset($_POST['codEstado']))			$codEstado			= \AppClass\App\Util::antiInjection($_POST['codEstado']);
if (isset($_POST['codCidade']))			$codCidade			= \AppClass\App\Util::antiInjection($_POST['codCidade']);
if (isset($_POST['codTipoSangue']))		$codTipoSangue		= \AppClass\App\Util::antiInjection($_POST['codTipoSangue']);
if (isset($_POST['dataPrazo']))			$dataPrazo			= \AppClass\App\Util::antiInjection($_POST['dataPrazo']);
if (isset($_POST['relato']))			$relato				= \AppClass\App\Util::antiInjection($_POST['relato']);

#################################################################################
## Limpar a variável de erro
#################################################################################
$err	= false;

#################################################################################
## Fazer validação dos campos
#################################################################################
/** Nome **/
if ((empty($nome))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Campo NOME é obrigatório"));
	$err	= 1;
}

/** Estado **/
if ((empty($codEstado))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Campo ESTADO é obrigatório"));
	$err	= 1;
}

/** Cidade **/
if ((empty($codCidade))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Campo CIDADE é obrigatório"));
	$err	= 1;
}

/** Tipo Sanguineo **/
if ((empty($codTipoSangue))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Campo TIPO SANGUÍNEO é obrigatório"));
	$err	= 1;
}

/** DataPrazo **/
if ((empty($dataPrazo))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Campo DATA PRAZO é obrigatório"));
	$err	= 1;
}

if ($err != null) {
	echo '1'.\AppClass\App\Util::encodeUrl('||'.htmlentities($err));
 	exit;
}
 
#################################################################################
## Salvar no banco
#################################################################################
try {
	//$db->transaction();
	
	$params = array();
	
	if (!empty($dataPrazo)) {
		$dataPrazo		= implode("-",array_reverse(explode("/",$dataPrazo)));
	}else{
		$dataPrazo		= null;
	}

	$params += array(':nome' => $nome,
			':pacienteTipo' => $pacienteTipo,
			':codEstado' => $codEstado,
			':codCidade' => $codCidade,
			':codTipoSangue' => $codTipoSangue,
			':dataSolicitacao' => date('Y-m-d'),
			':dataPrazo' => $dataPrazo,
			':relato' => $relato
	);
	
	if (isset($codUsuario) && (!empty($codUsuario))) {
		$params += array(':codigo' => $codUsuario);
 		$oDoacao	= $db->Executa('UPDATE `SLDOA_DOACAO` SET `COD_PACIENTE_TIPO`=:pacienteTipo, `NOME`=:nome,`COD_ESTADO`=:codEstado,`COD_CIDADE`=:codCidade,`COD_TIPO_SANGUE`=:codTipoSangue,`RELATO`=:relato,`DATA_SOLICITACAO`=:dataSolicitacao,`DATA_PRAZO`=:dataPrazo WHERE `CODIGO`=:codigo', 
 				$params);
 	}else{
 		$oDoacao	= $db->Executa('INSERT INTO `SLDOA_DOACAO`(`COD_PACIENTE_TIPO`, `NOME`, `COD_ESTADO`, `COD_CIDADE`, `COD_TIPO_SANGUE`, `RELATO`, `DATA_SOLICITACAO`, `DATA_PRAZO`) 
 				VALUES (:pacienteTipo,:nome,:codEstado,:codCidade,:codTipoSangue,:relato,:dataSolicitacao,:dataPrazo)', $params);
 	}
 	
 	if (stristr($oDoacao,'[ERR]')) throw new Exception();
 	/*if($db->commit()){
	 	#################################################################################
		## Carregando o template html
		#################################################################################
		$tpl	= new \AppClass\App\Template();
		$tpl->load(TPL_PATH . '/' . 'tplUsuarioCadastro.html');
		
		$texto		= "Segue abaixo seus dados de acesso: ";
		$assunto	= "Cadastro de Usuário";
		
		$tpl->set('ASSUNTO'		, $assunto);
		$tpl->set('TEXTO'		, $texto);
		$tpl->set('NOME'		, $nome);
		$tpl->set('LOGIN'		, $email);
		$tpl->set('SENHA'		, $senha);
		
		#################################################################################
		## Por fim exibir a página HTML
		#################################################################################
		$mensagem	= $tpl->getHtml();
		
		#################################################################################
		## Criar os objeto do email ,transporte e validador
		#################################################################################
		$mail 			= \AppClass\App\Mail::getMail();
		$transport 		= \AppClass\App\Mail::getTransport();
		$validator 		= new \Zend\Validator\EmailAddress();
		$htmlMail 		= new \Zend\Mime\Part($mensagem);
		$htmlMail->type = "text/html";
		$body 			= new \Zend\Mime\Message();
		$bodyArray		= array();
		$bodyArray[]	= $htmlMail;
		
		if (!$mail || !$transport) return 0;
		
		#################################################################################
		## Definir o conteúdo do e-mail
		#################################################################################
		$body->setParts($bodyArray);
		$mail->setBody($body);
		$mail->setSubject("<DoadorOnline> ".$assunto);
		
		$mail->addTo($email);
		
		try {
			$indOK		= true;
			$log->debug("Vou enviar o e-mail");
			$transport->send($mail);
			$log->debug("E-mail enviado com sucesso !!!");
			$erro		= null;
		} catch (\Exception $e) {
			$log->err("Erro ao enviar o e-mail:". $e->getTraceAsString());
			$indOK		= false;
			$erro		= $e->getTraceAsString();
			throw new Exception($erro);
		}
		 		
 	}else{
 		throw new Exception();
 	}*/
 	
} catch (\Exception $e) {
	//$db->rollBack();
 	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO, "Ocorreu um erro ao realizar solicitação. " . $e->getMessage());
 	echo '1'.\AppClass\App\Util::encodeUrl('||'.htmlentities($e->getMessage()));
 	exit;
}
 
$system->criaAviso(\AppClass\App\Aviso\Tipo::INFO,"Solicitação de doação realizada com sucesso");
echo '0'.\AppClass\App\Util::encodeUrl('|'.$oDoacao);