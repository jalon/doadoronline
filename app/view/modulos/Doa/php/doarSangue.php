<?php
################################################################################
# Includes
################################################################################
if (defined ( 'DOC_ROOT' )) {
	include_once (DOC_ROOT . 'include.php');
} else {
	include_once ('../include.php');
}

################################################################################
# Resgata a variável ID que está criptografada
################################################################################
if (isset ( $_GET ['id'] )) {
	$id = \AppClass\App\Util::antiInjection ( $_GET ["id"] );
} elseif (isset ( $_POST ['id'] )) {
	$id = \AppClass\App\Util::antiInjection ( $_POST ["id"] );
} elseif (isset ( $id )) {
	$id = \AppClass\App\Util::antiInjection ( $id );
} else {
	\AppClass\App\Erro::halt ( 'Falta de Parâmetros' );
}

################################################################################
# Descompacta o ID
################################################################################
\AppClass\App\Util::descompactaId ( $id );

################################################################################
# Verifica se o usuário tem permissão no menu
################################################################################
$system->checaPermissao ( $_codMenu_ );

################################################################################
# Resgata as informações do banco
################################################################################
/*if ($codUsuario) {
	try {
		$info	 = $db->extraiPrimeiro('SELECT * FROM `SLSEG_PESSOA` WHERE CODIGO = :codigo', array(':codigo' => $codUsuario));
	} catch ( \Exception $e ) {
		\AppClass\App\Erro::halt ( $e->getMessage () );
	}
	
	$nome			 = ($info->NOME) ? $info->NOME : null;
	$sobrenome		 = ($info->SOBRENOME) ? $info->SOBRENOME : null;
	$email			 = ($info->EMAIL) ? $info->EMAIL : null;
	$telefone		 = ($info->TELEFONE) ? $info->TELEFONE : null;
	$dtNascimento	 = ($info->DAT_NASCIMENTO) ? \AppClass\App\Util::formatData($system->config["data"]["dateFormat"], $info->DAT_NASCIMENTO) : null;
	$cpf			 = ($info->CPF) ? $info->CPF : null;
	$codSexo		 = ($info->COD_SEXO) ? $info->COD_SEXO : null;
} else {*/
	$nome	  	 	 = null;
	$sobrenome 	 	 = null;
	$email			 = null;
	$telefone		 = null;
	$dtNascimento	 = null;
	$cpf			 = null;
	$codSexo		 = null;
//}

################################################################################
# Select de Estado
################################################################################
try {
	$aEstado	 = $db->extraiTodos('SELECT E.* FROM `SLADM_ORGANIZACAO` AS O 
					LEFT OUTER JOIN `SLADM_ESTADO` E ON (O.COD_ESTADO = E.COD_UF)
						GROUP BY O.COD_ESTADO', array());
	$oEstado = $system->geraHtmlCombo ( $aEstado, 'COD_UF', 'NOME', null, 'Selecione o Estado');
} catch ( \Exception $e ) {
	\AppClass\App\Erro::halt ( $e->getMessage (), __FILE__, __LINE__ );
}

################################################################################
# Select de Estado
################################################################################
try {
	$aTipoSangue	 = $db->extraiTodos('SELECT * FROM `SLADM_TIPO_SANGUE`', array());
	$oTipoSangue = $system->geraHtmlCombo ( $aTipoSangue, 'CODIGO', 'DESCRICAO', null, 'Selecione o tipo sanguíneo');
} catch ( \Exception $e ) {
	\AppClass\App\Erro::halt ( $e->getMessage (), __FILE__, __LINE__ );
}

################################################################################
# Url Voltar
################################################################################
$urlVoltar = ROOT_URL . "/Doa/doarSangue.php?id=" . $id;

################################################################################
# Url Novo
################################################################################
$uid = \AppClass\App\Util::encodeUrl ( '_codMenu_=' . $_codMenu_ . '&_icone_=' . $_icone_ . '&codDoacao=' );
$urlNovo = ROOT_URL . "/Doa/doarSangue.php?id=" . $uid;

################################################################################
# Carregando o template html
################################################################################
$tpl = new \AppClass\App\Template ();
$tpl->load ( \AppClass\App\Util::getCaminhoCorrespondente ( __FILE__, \AppClass\App\ZWS::EXT_HTML ) );
//$tpl->load(HTML_PATH . 'doarSangueConf.html');

################################################################################
# Define os valores das variáveis
################################################################################
$tpl->set ( 'URL_FORM'			   , $_SERVER ['SCRIPT_NAME'] );
$tpl->set ( 'URLVOLTAR'			   , $urlVoltar );
$tpl->set ( 'URLNOVO'		 	   , $urlNovo );
$tpl->set ( 'ID'				   , $id );
//$tpl->set ( 'COD_DOACAO'		   , $codDoacao);

$tpl->set ( 'NOME'				   , $_user->NOME);
/*$tpl->set ( 'SOBRENOME'			   , $sobrenome);
$tpl->set ( 'EMAIL'			 	   , $email);
$tpl->set ( 'TELEFONE'		 	   , $telefone);
$tpl->set ( 'DATA_NASCIMENTO'	   , $dtNascimento);
$tpl->set ( 'CPF'	 			   , $cpf);*/
$tpl->set ( 'COD_ESTADO'		   , $oEstado);
$tpl->set ( 'COD_TIPO_SANGUE'	   , $oTipoSangue);

$tpl->set ( 'DP', \AppClass\App\Util::getCaminhoCorrespondente ( __FILE__, \AppClass\App\ZWS::EXT_DP, \AppClass\App\ZWS::CAMINHO_RELATIVO ) );
################################################################################
# Por fim exibir a página HTML
################################################################################
$tpl->show ();

