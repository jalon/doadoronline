<?php
################################################################################
# Includes
################################################################################
if (defined ( 'DOC_ROOT' )) {
	include_once (DOC_ROOT . 'include.php');
} else {
	include_once ('../include.php');
}

################################################################################
# Resgata a variável ID que está criptografada
################################################################################
if (isset ( $_GET ['id'] )) {
	$id = \AppClass\App\Util::antiInjection ( $_GET ["id"] );
} elseif (isset ( $_POST ['id'] )) {
	$id = \AppClass\App\Util::antiInjection ( $_POST ["id"] );
} elseif (isset ( $id )) {
	$id = \AppClass\App\Util::antiInjection ( $id );
} else {
	\AppClass\App\Erro::halt ( 'Falta de Parâmetros' );
}

################################################################################
# Descompacta o ID
################################################################################
\AppClass\App\Util::descompactaId ( $id );

################################################################################
# Verifica se o usuário tem permissão no menu
################################################################################
$system->checaPermissao ( $_codMenu_ );

################################################################################
# Resgata as informações do banco
################################################################################
if ($codDoacao) {
	try {
		$info	 = $db->extraiPrimeiro('SELECT DO.*, P.DESCRICAO AS PACIENTE_TIPO FROM `SLDOA_DOACAO` DO
				LEFT OUTER JOIN `SLDOA_PACIENTE_TIPO` P ON (DO.COD_PACIENTE_TIPO = P.CODIGO)
				WHERE DO.CODIGO = :codigo', array(':codigo' => $codDoacao));
	} catch ( \Exception $e ) {
		\AppClass\App\Erro::halt ( $e->getMessage () );
	}
	
	$nome			 = ($info->NOME) ? $info->NOME : null;
	$paciente		 = ($info->PACIENTE_TIPO) ? $info->PACIENTE_TIPO : null;
	$hemocentro		 = "Hemopac";
	$dataSolicitacao = ($info->DATA_SOLICITACAO) ? \AppClass\App\Util::formatData($system->config["data"]["dateFormat"], $info->DATA_SOLICITACAO) : null;
} else {
	$nome	  	 	 = null;
	$paciente 	 	 = null;
	$hemocentro		 = null;
	$dataSolicitacao = null;
}

################################################################################
# Carregando o template html
################################################################################
$tpl = new \AppClass\App\Template ();
$tpl->load ( \AppClass\App\Util::getCaminhoCorrespondente ( __FILE__, \AppClass\App\ZWS::EXT_HTML ) );
//$tpl->load(HTML_PATH . 'doarSangueConf.html');

################################################################################
# Define os valores das variáveis
################################################################################
$tpl->set ( 'URL_FORM'			   , $_SERVER ['SCRIPT_NAME'] );
$tpl->set ( 'ID'				   , $id );
//$tpl->set ( 'COD_DOACAO'		   , $codDoacao);

$tpl->set ( 'CODIGO'			   , $codDoacao);
$tpl->set ( 'NOME'				   , $nome);
$tpl->set ( 'PACIENTE_TIPO'	  	   , $paciente);
$tpl->set ( 'HEMOCENTRO'	 	   , $hemocentro);
$tpl->set ( 'DATA_SOLICITACAO'     , $dataSolicitacao);

$tpl->set ( 'DP', \AppClass\App\Util::getCaminhoCorrespondente ( __FILE__, \AppClass\App\ZWS::EXT_DP, \AppClass\App\ZWS::CAMINHO_RELATIVO ) );
################################################################################
# Por fim exibir a página HTML
################################################################################
$tpl->show ();

