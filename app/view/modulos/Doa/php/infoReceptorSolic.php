<?php
################################################################################
# Includes
################################################################################
if (defined ( 'DOC_ROOT' )) {
	include_once (DOC_ROOT . 'include.php');
} else {
	include_once ('../include.php');
}

################################################################################
# Resgata a variável ID que está criptografada
################################################################################
if (isset ( $_GET ['id'] )) {
	$id = \AppClass\App\Util::antiInjection ( $_GET ["id"] );
} elseif (isset ( $_POST ['id'] )) {
	$id = \AppClass\App\Util::antiInjection ( $_POST ["id"] );
} elseif (isset ( $id )) {
	$id = \AppClass\App\Util::antiInjection ( $id );
} else {
	\AppClass\App\Erro::halt ( 'Falta de Parâmetros' );
}

################################################################################
# Descompacta o ID
################################################################################
\AppClass\App\Util::descompactaId ( $id );

################################################################################
# Verifica se o usuário tem permissão no menu
################################################################################
$system->checaPermissao ( $_codMenu_ );

################################################################################
# Resgata as informações do banco
################################################################################
if($codSolicitacao){
	try {
		$info	 = $db->extraiPrimeiro('SELECT P.EMAIL, P.TELEFONE, S.CODIGO AS COD_SOLICITACAO, S.NOME, TS.DESCRICAO AS TIPO_SANGUE, S.COD_ESTADO, C.NOME AS CIDADE, S.COD_HEMOCENTRO, O.NOME AS HEMOCENTRO, S.RELATO, S.DATA_SOLICITACAO, S.DATA_PRAZO, SS.DESCRICAO AS STATUS
		 FROM `SLREC_SOLICITACAO` AS S
			LEFT OUTER JOIN `SLADM_TIPO_SANGUE` TS ON (S.COD_TIPO_SANGUE = TS.CODIGO)
			LEFT OUTER JOIN `SLREC_SOLICITACAO_STATUS` SS ON (S.COD_STATUS = SS.CODIGO)
			LEFT OUTER JOIN `SLADM_ORGANIZACAO` O ON (S.COD_HEMOCENTRO = O.CODIGO)
			LEFT OUTER JOIN `SLADM_ESTADO` E ON (S.COD_ESTADO = E.COD_UF)
			LEFT OUTER JOIN `SLADM_CIDADE` C ON (S.COD_CIDADE = C.CODIGO)	
			LEFT OUTER JOIN `SLSEG_USUARIO` U ON (S.COD_USUARIO = U.CODIGO)			
			LEFT OUTER JOIN `SLSEG_PESSOA` P ON (U.COD_PESSOA = P.CODIGO)				
				WHERE S.CODIGO = :codigo', array(':codigo' => $codSolicitacao));
	} catch ( \Exception $e ) {
		\AppClass\App\Erro::halt ( $e->getMessage () );
	}
	
	#Pessoal
	$nome	 			 = ($info->NOME) ? $info->NOME : null;
	$email	 			 = ($info->EMAIL) ? $info->EMAIL : null;
	$telefone 			 = ($info->TELEFONE) ? $info->TELEFONE : null;
	
	#Solicitacaos
	$tipoSangue	 		 = ($info->TIPO_SANGUE) ? $info->TIPO_SANGUE : null;
	$relato		 		 = ($info->RELATO) ? $info->RELATO : null;
	$dataSolicitacao	 = ($info->DATA_SOLICITACAO) ? $info->DATA_SOLICITACAO : null;
	$dataPrazo			 = ($info->DATA_PRAZO) ? $info->DATA_PRAZO : null;
	$hemocentro  		 = ($info->HEMOCENTRO) ? $info->HEMOCENTRO : null;
	$estado		  		 = ($info->COD_ESTADO) ? $info->COD_ESTADO : null;
	$cidade		  		 = ($info->CIDADE) ? $info->CIDADE : null;
	$status				 = ($info->STATUS) ? $info->STATUS : null;

	if($status == "SOLICITADO"){
		$status = '<span class="label label-warning">Solicitado</span>';
	}else if($status == "ACEITO"){
		$status = '<span class="label label-success">Aceito</span>';
	}
}else{
	#Pessoal
	$nome			 = null;
	$email			 = null;
	$telefone		 = null;
	
	#Solicitacao
	$tipoSangue		 = null;
	$relato			 = null;
	$dataSolicitacao = null;
	$dataPrazo		 = null;
	$hemocentro      = null;
	$estado     	 = null;
	$cidade  	     = null;
	$status			 = null;
}

################################################################################
# Carregando o template html
################################################################################
$tpl = new \AppClass\App\Template ();
$tpl->load ( \AppClass\App\Util::getCaminhoCorrespondente ( __FILE__, \AppClass\App\ZWS::EXT_HTML ) );

################################################################################
# Define os valores das variáveis
################################################################################
$tpl->set ( 'URL_FORM'			   , $_SERVER ['SCRIPT_NAME'] );
$tpl->set ( 'ID'				   , $id );

$tpl->set ( 'NOME'			   	   , $nome);
$tpl->set ( 'EMAIL'	   			   , $email);
$tpl->set ( 'TELEFONE'			   ,\AppClass\App\Util::formatPhone( $telefone ) );

## Solicitacao
$tpl->set ( 'COD_SOLICITACAO'	   , $codSolicitacao);
$tpl->set ( 'TIPO_SANGUE'		   , $tipoSangue);
$tpl->set ( 'RELATO'			   , $relato);
$tpl->set ( 'DATA_SOLICITACAO'	   , \AppClass\App\Util::formatData($system->config["data"]["dateFormat"], $dataSolicitacao));
$tpl->set ( 'DATA_PRAZO'	  	   , \AppClass\App\Util::formatData($system->config["data"]["dateFormat"], $dataPrazo));
$tpl->set ( 'HEMOCENTRO'	   	   , $hemocentro);
$tpl->set ( 'ESTADO'		   	   , $estado);
$tpl->set ( 'CIDADE'		   	   , $cidade);
$tpl->set ( 'STATUS'	   		   , $status);

$tpl->set ( 'DP', \AppClass\App\Util::getCaminhoCorrespondente ( __FILE__, \AppClass\App\ZWS::EXT_DP, \AppClass\App\ZWS::CAMINHO_RELATIVO ) );
################################################################################
# Por fim exibir a página HTML
################################################################################
$tpl->show ();

