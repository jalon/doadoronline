<?php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'include.php');
}else{
	include_once('../include.php');
}

#################################################################################
## Resgata a variável ID que está criptografada
#################################################################################
if (isset($_GET['id'])) {
	$id = \AppClass\App\Util::antiInjection($_GET["id"]);
}elseif (isset($_POST['id'])) {
	$id = \AppClass\App\Util::antiInjection($_POST["id"]);
}elseif (isset($id)) 	{
	$id = \AppClass\App\Util::antiInjection($id);
}else{
	\AppClass\App\Erro::halt('Falta de Parâmetros');
}

#################################################################################
## Descompacta o ID
#################################################################################
\AppClass\App\Util::descompactaId($id);

#################################################################################
## Verifica se o usuário tem permissão no menu
#################################################################################
$system->checaPermissao($_codMenu_);

#################################################################################
## Resgata a url desse script
#################################################################################
$url		= ROOT_URL . '/Rec/'. basename(__FILE__);

#################################################################################
## Resgata os dados do grid
#################################################################################
try {
	$doacao = $db->extraiTodos('SELECT D.*, E.NOME AS ESTADO, C.NOME AS CIDADE, TS.DESCRICAO AS TIPO_SANGUE, O.NOME AS HEMOCENTRO, DS.DESCRICAO AS STATUS
			 FROM `SLDOA_DOACAO` D
			        LEFT OUTER JOIN `SLADM_ESTADO` E ON (D.COD_ESTADO = E.COD_UF)
			        LEFT OUTER JOIN `SLADM_CIDADE` C ON (D.COD_CIDADE = C.CODIGO)
					LEFT OUTER JOIN `SLADM_TIPO_SANGUE` TS ON (D.COD_TIPO_SANGUE = TS.CODIGO)
					LEFT OUTER JOIN `SLADM_ORGANIZACAO` O ON (D.COD_HEMOCENTRO = O.CODIGO)
					LEFT OUTER JOIN `SLDOA_DOACAO_STATUS`DS ON (D.COD_STATUS = DS.CODIGO)
						WHERE D.COD_USUARIO =:codUsuario',
    				array( ':codUsuario' => $system->getCodUsuario() ) 
			);
} catch (\Exception $e) {
	\AppClass\App\Erro::halt($e->getMessage());
}
	
#################################################################################
## Cria o objeto do Grid (bootstrap)
#################################################################################
$grid			= \AppClass\App\Grid::criar(\AppClass\App\Grid\Tipo::TP_BOOTSTRAP, "");
//$grid->adicionaTexto($tr->trans('PACIENTE'),	 		5, $grid::CENTER	,'PACIENTE_TIPO');
$grid->adicionaTexto($tr->trans('NOME'),	 			15, $grid::CENTER	,'NOME');
$grid->adicionaTexto($tr->trans('HEMOCENTRO'),	 		10, $grid::CENTER	,'HEMOCENTRO');
$grid->adicionaTexto($tr->trans('ESTADO'),	 			10, $grid::CENTER	,'ESTADO');
$grid->adicionaTexto($tr->trans('CIDADE'),	 			10, $grid::CENTER	,'CIDADE');
$grid->adicionaTexto($tr->trans('SANGUE'),	 			5, $grid::CENTER	,'TIPO_SANGUE');
$grid->adicionaTexto($tr->trans('DATA SOLICITADO'),		10, $grid::CENTER	,'DATA_SOLICITACAO');
$grid->adicionaTexto($tr->trans('DATA DOAÇÃO'),			10, $grid::CENTER	,'DATA_DOACAO');
$grid->adicionaTexto($tr->trans('STATUS'),				10, $grid::CENTER	,'');

//$grid->adicionaBotao(\AppClass\App\Grid\Coluna\Botao::MOD_EDIT);
//$grid->adicionaBotao(\AppClass\App\Grid\Coluna\Botao::MOD_REMOVE);
$grid->importaDadosDoctrine($doacao);

#################################################################################
## Popula os valores dos botões
#################################################################################
for ($i = 0; $i < sizeof($doacao); $i++) {
	//$uid		= \AppClass\App\Util::encodeUrl('_codMenu_='.$_codMenu_.'&_icone_='.$_icone_.'&codSolicitacao='.$doacao[$i]->CODIGO.'&url='.$url);
	
	$grid->setValorCelula($i,5,\AppClass\App\Util::formatData($system->config["data"]["dateFormat"], $doacao[$i]->DATA_SOLICITACAO));
	$grid->setValorCelula($i,6,\AppClass\App\Util::formatData($system->config["data"]["dateFormat"], $doacao[$i]->DATA_DOACAO));
	
	if ( $doacao[$i]->COD_STATUS == "R" ){
		$status = '<span class="label label-success">'. $doacao[$i]->STATUS .'</span>';
	}else if ( $doacao[$i]->COD_STATUS == "A" ){
		$status = '<span class="label label-warning">'. $doacao[$i]->STATUS .'</span>';
	}else if ( $doacao[$i]->COD_STATUS == "N" ){
		$status = '<span class="label label-info">'. $doacao[$i]->STATUS .'</span>';
	}else if ( $doacao[$i]->COD_STATUS == "C" ){
		$status = '<span class="label label-danger">'. $doacao[$i]->STATUS .'</span>';
	}
	
	$grid->setValorCelula($i,7, $status );
	//$grid->setUrlCelula($i,4,'javascript:AbreModal(\'/App/notificacaoLer.php?id='.$uid.'\');"');
	//$grid->setUrlCelula($i,5,'javascript:AbreModal(\'/App/meusDadosExc.php?id='.$uid.'\');"');
}

#################################################################################
## Gerar o código html do grid
#################################################################################
try {
	$htmlGrid	= $grid->getHtmlCode();
} catch (\Exception $e) {
	\AppClass\App\Erro::halt($e->getMessage());
}

#################################################################################
## Carregando o template html
#################################################################################
$tpl	= new \AppClass\App\Template();
$tpl->load(HTML_PATH . 'templateLis.html');

#################################################################################
## Define os valores das variáveis
#################################################################################
$tpl->set('GRID'			,$htmlGrid);
$tpl->set('URLADD'			,'');
$tpl->set('NOME'			,$tr->trans('Minhas Doações'));
$tpl->set('IC'				,$_icone_);

#################################################################################
## Por fim exibir a página HTML
#################################################################################
$tpl->show();
