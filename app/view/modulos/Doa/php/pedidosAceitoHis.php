<?php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'include.php');
}else{
	include_once('../include.php');
}

#################################################################################
## Resgata a variável ID que está criptografada
#################################################################################
if (isset($_GET['id'])) {
	$id = \AppClass\App\Util::antiInjection($_GET["id"]);
}elseif (isset($_POST['id'])) {
	$id = \AppClass\App\Util::antiInjection($_POST["id"]);
}elseif (isset($id)) 	{
	$id = \AppClass\App\Util::antiInjection($id);
}else{
	\AppClass\App\Erro::halt('Falta de Parâmetros');
}

#################################################################################
## Descompacta o ID
#################################################################################
\AppClass\App\Util::descompactaId($id);

#################################################################################
## Verifica se o usuário tem permissão no menu
#################################################################################
$system->checaPermissao($_codMenu_);

#################################################################################
## Resgata a url desse script
#################################################################################
$url		= ROOT_URL . '/Rec/'. basename(__FILE__);

#################################################################################
## Resgata os dados do grid
#################################################################################
try {
	$info = $db->extraiTodos('SELECT * FROM `SLDOA_SOLICITACAO_ACEITA` SA
			LEFT OUTER JOIN `SLREC_SOLICITACAO` S ON (S.CODIGO = SA.COD_SOLICITACAO) 
				WHERE SA.COD_USUARIO_ACEITO =:codUsuario',
    				array( ':codUsuario' => $system->getCodUsuario() ) 
			);
} catch (\Exception $e) {
	\AppClass\App\Erro::halt($e->getMessage());
}
	
#################################################################################
## Cria o objeto do Grid (bootstrap)
#################################################################################
$grid			= \AppClass\App\Grid::criar(\AppClass\App\Grid\Tipo::TP_BOOTSTRAP, "");
$grid->adicionaTexto($tr->trans('PEDIDO'),	 			5, $grid::CENTER	,'');
$grid->adicionaTexto($tr->trans('NOME'),		 		15, $grid::CENTER	,'NOME');
$grid->adicionaTexto($tr->trans('DATA ACEITO'),	 		15, $grid::CENTER	,'');

$grid->adicionaIcone(null, 'fa fa-info-circle', "+ Informações");
//$grid->adicionaBotao(\AppClass\App\Grid\Coluna\Botao::MOD_EDIT);
//$grid->adicionaBotao(\AppClass\App\Grid\Coluna\Botao::MOD_REMOVE);
$grid->importaDadosDoctrine($info);

#################################################################################
## Popula os valores dos botões
#################################################################################
for ($i = 0; $i < sizeof($info); $i++) {
	$uid		= \AppClass\App\Util::encodeUrl('_codMenu_='.$_codMenu_.'&_icone_='.$_icone_.'&codSolicitacao='.$info[$i]->COD_SOLICITACAO.'&url='.$url);
	
	$grid->setValorCelula($i,0, '#' . $info[$i]->COD_SOLICITACAO);
	
	$grid->setValorCelula($i,2,\AppClass\App\Util::formatData($system->config["data"]["dateFormat"], $info[$i]->DATA_ACEITO));
	
	$grid->setUrlCelula($i,3,'javascript:AbreModal(\'/Doa/infoReceptorSolic.php?id='.$uid.'\');"');
	//$grid->setUrlCelula($i,5,'javascript:AbreModal(\'/App/meusDadosExc.php?id='.$uid.'\');"');
}

#################################################################################
## Gerar o código html do grid
#################################################################################
try {
	$htmlGrid	= $grid->getHtmlCode();
} catch (\Exception $e) {
	\AppClass\App\Erro::halt($e->getMessage());
}

#################################################################################
## Carregando o template html
#################################################################################
$tpl	= new \AppClass\App\Template();
$tpl->load(HTML_PATH . 'templateLis.html');

#################################################################################
## Define os valores das variáveis
#################################################################################
$tpl->set('GRID'			,$htmlGrid);
$tpl->set('URLADD'			,'');
$tpl->set('NOME'			,$tr->trans('Pedidos de doações Aceitos'));
$tpl->set('IC'				,$_icone_);

#################################################################################
## Por fim exibir a página HTML
#################################################################################
$tpl->show();
