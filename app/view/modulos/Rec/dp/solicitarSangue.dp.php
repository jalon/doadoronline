<?php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'include.php');
}else{
 	include_once('../include.php');
}
 
#################################################################################
## Resgata os parâmetros passados pelo formulário
#################################################################################
if (isset($_POST['pacienteTipo']))		$pacienteTipo		= \AppClass\App\Util::antiInjection($_POST['pacienteTipo']);
if (isset($_POST['nome']))				$nome				= \AppClass\App\Util::antiInjection($_POST['nome']);
if (isset($_POST['codEstado']))			$codEstado			= \AppClass\App\Util::antiInjection($_POST['codEstado']);
if (isset($_POST['codCidade']))			$codCidade			= \AppClass\App\Util::antiInjection($_POST['codCidade']);
if (isset($_POST['codTipoSangue']))		$codTipoSangue		= \AppClass\App\Util::antiInjection($_POST['codTipoSangue']);
if (isset($_POST['dataPrazo']))			$dataPrazo			= \AppClass\App\Util::antiInjection($_POST['dataPrazo']);
if (isset($_POST['relato']))			$relato				= \AppClass\App\Util::antiInjection($_POST['relato']);
if (isset($_POST['codHemocentro']))		$codHemocentro		= \AppClass\App\Util::antiInjection($_POST['codHemocentro']);
if (isset($_POST['quantidade']))		$quantidade			= \AppClass\App\Util::antiInjection($_POST['quantidade']);
if (isset($_POST['codUsuario']))		$codUsuario			= \AppClass\App\Util::antiInjection($_POST['codUsuario']);

#################################################################################
## Limpar a variável de erro
#################################################################################
$err	= false;

#################################################################################
## Fazer validação dos campos
#################################################################################
/** Nome **/
if ((empty($nome))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Campo NOME é obrigatório"));
	$err	= 1;
}

/** Estado **/
if ((empty($codEstado))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Campo ESTADO é obrigatório"));
	$err	= 1;
}

/** Cidade **/
if ((empty($codCidade))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Campo CIDADE é obrigatório"));
	$err	= 1;
}

/** Tipo Sanguineo **/
if ((empty($codTipoSangue))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Campo TIPO SANGUÍNEO é obrigatório"));
	$err	= 1;
}

/** Hemocentro **/
if ((empty($codHemocentro))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Campo HEMOCENTRO é obrigatório"));
	$err	= 1;
}

/** DataPrazo **/
if ((empty($dataPrazo))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Campo DATA PRAZO é obrigatório"));
	$err	= 1;
}

/** Doadores n encontrado **/
if ( isset($codUsuario) && sizeof($codUsuario) <= 0 ) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Não existe dadores compativeis no momento!"));
	$err	= 1;
}

/** Doadores **/
if ( isset($codUsuario) && sizeof($codUsuario) > 5) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Você só pode marcar até 5(cinco) doadores"));
	$err	= 1;
}

if ($err != null) {
	echo '1'.\AppClass\App\Util::encodeUrl('||'.htmlentities($err));
 	exit;
}
 
#################################################################################
## Salvar no banco
#################################################################################
try {
	$db->transaction();
	
	$params = array();
	
	if (!empty($dataPrazo)) {
		$dataPrazo		= implode("-",array_reverse(explode("/",$dataPrazo)));
	}else{
		$dataPrazo		= null;
	}

	if ( \AppClass\Hem\Sangue::listaQuantidadeReceber($codTipoSangue) == 0 ){
		$codStatus = "E";
	}else{
		$codStatus = "S";
	}
		
	$params += array(':codUsuario' => $system->getCodUsuario(),
			':nome' => $nome,
			':pacienteTipo' => $pacienteTipo,
			':codEstado' => $codEstado,
			':codCidade' => $codCidade,
			':codTipoSangue' => $codTipoSangue,
			':codHemocentro' => $codHemocentro,
			':dataSolicitacao' => date('Y-m-d'),
			':dataPrazo' => $dataPrazo,
			':relato' => $relato,
			':codStatus' => $codStatus
	);
	
	$oSolicitacao	= $db->Executa('INSERT INTO `SLREC_SOLICITACAO`(`COD_USUARIO`, `COD_PACIENTE_TIPO`, `NOME`, `COD_ESTADO`, `COD_CIDADE`, `COD_TIPO_SANGUE`, `COD_HEMOCENTRO`, `RELATO`, `DATA_SOLICITACAO`, `DATA_PRAZO`, `COD_STATUS`) 
 				VALUES (:codUsuario,:pacienteTipo,:nome,:codEstado,:codCidade,:codTipoSangue,:codHemocentro,:relato,:dataSolicitacao,:dataPrazo,:codStatus)', $params);
 	
 	//$usuarios 		= \AppClass\Hem\Sangue::listaUsuariosPodeDoar($codTipoSangue);
 	
 	if($db->commit() & sizeof($codUsuario) > 0){
		#################################################################################
		## Gerar a notificação
		#################################################################################
		$template		= $db->extraiPrimeiro('SELECT * FROM `SLNOT_NOTIFICACAO_TEMPLATE` WHERE TEMPLATE =:template', array(':template' => 'SOLICITACAO_SANGUE'));
		$notificacao	= new \AppClass\App\Notificacao(\AppClass\App\Notificacao::TIPO_MENSAGEM_TEMPLATE, \AppClass\App\Notificacao::TIPO_DEST_USUARIO, \AppClass\App\Notificacao::SOLICITACAO_SANGUE);
		$notificacao->setAssunto("Solicitação de doador de sangue.");
		$notificacao->setCodRemetente($system->getCodUsuario());
		$notificacao->setCodSolicitacao($oSolicitacao);
		
		for ($i = 0; $i < sizeof($codUsuario); $i++) {
			$notificacao->associaUsuario( $codUsuario[$i] );
		}
	
		$notificacao->enviaEmail();
		$notificacao->enviaSistema();
		$notificacao->setCodTemplate($template->CODIGO);
		$notificacao->salva();
 	}else{
 		throw new Exception();
 	}
 	
} catch (\Exception $e) {
	$db->rollBack();
 	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO, "Ocorreu um erro ao realizar solicitação. " . $e->getMessage());
 	echo '1'.\AppClass\App\Util::encodeUrl('||'.htmlentities($e->getMessage()));
 	exit;
}
 
$system->criaAviso(\AppClass\App\Aviso\Tipo::INFO,"Solicitação de doação realizada com sucesso");
echo '0'.\AppClass\App\Util::encodeUrl('|'.$oSolicitacao);