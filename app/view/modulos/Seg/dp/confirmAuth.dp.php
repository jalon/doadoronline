<?php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'includeNoAuth.php');
}else{
	include_once('includeNoAuth.php');
}
 
#################################################################################
## Resgata os parâmetros passados pelo formulário
#################################################################################
if (isset($_POST['codUsuario']))		$codUsuario			= \AppClass\App\Util::antiInjection($_POST['codUsuario']);
if (isset($_POST['codPessoa']))			$codPessoa			= \AppClass\App\Util::antiInjection($_POST['codPessoa']);
if (isset($_POST['hashCode']))			$hashCode			= \AppClass\App\Util::antiInjection($_POST['hashCode']);
if (isset($_POST['novaSenha']))			$novaSenha			= \AppClass\App\Util::antiInjection($_POST['novaSenha']);
if (isset($_POST['confirmeSenha']))		$confirmeSenha		= \AppClass\App\Util::antiInjection($_POST['confirmeSenha']);

#################################################################################
## Limpar a variável de erro
#################################################################################
$err	 = false;
$msgErr  = null;

#################################################################################
## Verificar se os usuário já existe e se já está ativo
#################################################################################
$_oAuth	 = $db->extraiPrimeiro('SELECT U.* FROM `SLSEG_USUARIO` U
								LEFT OUTER JOIN `SLSEG_PESSOA` P ON (P.CODIGO = U.COD_PESSOA)
									WHERE U.CODIGO =:codUsuario AND P.CODIGO =:codPessoa', array(':codUsuario' => $codUsuario, ':codPessoa' => $codPessoa ));

if (!$_oAuth || ( isset($_oAuth->scalar) && $_oAuth->scalar == false ) ) {
	$msgErr .= '<li>Usuario não existe, COD_ERRO: 01</li>';
	$err	= 1;
}
if ($_oAuth->COD_STATUS != 0){
	$msgErr .= '<li>Esta conta já está ativada, COD_ERRO: 02</li>';
	$err	= 1;
}
if ($_oAuth->HASH != $hashCode){
	$msgErr .= '<li>KEY de alteração não corresponde, COD_ERRO: 03</li>';
	$err	= 1;
}

#################################################################################
## Fazer validação dos campos
#################################################################################
/** Nova Senha **/
if (empty($novaSenha)) {
	$msgErr .= '<li>Campo SENHA é obrigatório</li>';
	$err	= 1;
}

/** Confirme Senha **/
if (empty($confirmeSenha)) {
	$msgErr .= '<li>Campo CONFIRME SENHA é obrigatório</li>';
	$err	= 1;
}

if (!empty($novaSenha) && !empty($confirmeSenha)) {	
	if( $novaSenha != $confirmeSenha ){
		$msgErr .= '<li>SENHAS não conferem! </li>';
		$err	= 1;
	}
}

if ($err != null) {
	echo '1| '.$msgErr;
 	exit;
}
 
#################################################################################
## Salvar no banco
#################################################################################
try {	
	## Gerar Senha do Usuario
	$senhaCrypt = \AppClass\App\Crypt::crypt($_oAuth->USUARIO, $novaSenha);
	
	$oSenha	= $db->Executa('UPDATE `SLSEG_USUARIO` SET `SENHA`=:senha, `COD_STATUS`=:codStatus, `HASH`=:hash WHERE CODIGO =:codigo',
			array(':codigo' => $_oAuth->CODIGO,
					':senha' => $senhaCrypt,
					':codStatus' => 1,
					':hash' => null
			)
	);
	
	#################################################################################
	## Urls
	#################################################################################
	//$urlRedirecionar	= ROOT_URL . strtolower( \AppClass\Seg\Usuario::getIdentificacaoOrg($_oAuth->COD_ORGANIZACAO) );
	
} catch (\Exception $e) {	
 	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO, "Ocorreu um erro ao salvar sua senha." . $e->getMessage());
 	echo '1'.\AppClass\App\Util::encodeUrl('||'.htmlentities($e->getMessage()));
 	exit;
}
 	
echo '1| <li>Senha criada com sucesso, agora você pode entrar na plataforma e salvar vidas!</li>';
exit;