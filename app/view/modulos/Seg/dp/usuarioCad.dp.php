<?php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'include.php');
}else{
 	include_once('../include.php');
}
 
#################################################################################
## Resgata os parâmetros passados pelo formulário
#################################################################################
if (isset($_POST['codUsuario']))		$codUsuario			= \AppClass\App\Util::antiInjection($_POST['codUsuario']);
if (isset($_POST['nome']))				$nome				= \AppClass\App\Util::antiInjection($_POST['nome']);
if (isset($_POST['email']))				$email				= \AppClass\App\Util::antiInjection($_POST['email']);
if (isset($_POST['telefone']))			$telefone			= \AppClass\App\Util::antiInjection($_POST['telefone']);
if (isset($_POST['dataNascimento']))	$dataNascimento		= \AppClass\App\Util::antiInjection($_POST['dataNascimento']);
if (isset($_POST['codSexo']))			$codSexo			= \AppClass\App\Util::antiInjection($_POST['codSexo']);
if (isset($_POST['codTipoSangue']))		$codTipoSangue		= \AppClass\App\Util::antiInjection($_POST['codTipoSangue']);
if (isset($_POST['codPerfil']))			$codPerfil			= \AppClass\App\Util::antiInjection($_POST['codPerfil']);
if (isset($_POST['hemocentro']))		$hemocentro			= \AppClass\App\Util::antiInjection($_POST['hemocentro']);

#################################################################################
## Limpar a variável de erro
#################################################################################
$err	= false;

#################################################################################
## Fazer validação dos campos
#################################################################################
/** Cargo **/
if ((empty($nome))) {
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Campo NOME é obrigatório"));
	$err	= 1;
}

$info	 = $db->extraiPrimeiro('SELECT * FROM `SLSEG_PESSOA` WHERE EMAIL = :email', array(':email' => $email));

if($info != null &&(!empty($info->CODIGO) != $codUsuario)){
	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO,$tr->trans("Este EMAIL já foi cadastrado!"));
	$err	= 1;
}

if ($err != null) {
	echo '1'.\AppClass\App\Util::encodeUrl('||'.htmlentities($err));
 	exit;
}
 
#################################################################################
## Salvar no banco
#################################################################################
try {
	$db->transaction();
	
	$params = array();
	
	if (!empty($dataNascimento)) {
		$dtNasc		= $data = implode("-",array_reverse(explode("/",$dataNascimento)));
	}else{
		$dtNasc		= null;
	}
	
	$params += array(':nome' => $nome,
			':email' => $email,
			':telefone' => $telefone,
			':dataNascimento' => $dtNasc,
			':codSexo' => $codSexo,
			':codTipoSangue' => $codTipoSangue,
			':hemocentro' => $hemocentro
	);
	
	if (isset($codUsuario) && (!empty($codUsuario))) {
		$info	 	= $db->extraiPrimeiro('SELECT COD_PESSOA FROM `SLSEG_USUARIO` WHERE CODIGO = :codigo', array(':codigo' => $codUsuario));
		
		$params 	+= array(':codigo' => $info->COD_PESSOA);
		
 		$oPessoa	= $db->Executa('UPDATE `SLSEG_PESSOA` SET `NOME`=:nome,`EMAIL`=:email,`TELEFONE`=:telefone,`DAT_NASCIMENTO`=:dataNascimento,`COD_SEXO`=:codSexo,`TIPO_SANGUINEO`=:codTipoSangue,`HEMOCENTRO`=:hemocentro WHERE `CODIGO`=:codigo', 
 				$params);
 		
 		/** ### Cricacao do usuario ### **/
 		$paramUsu = array(':codigo' => $codUsuario,
 				':codPerfil' => $codPerfil
 		);
 			
 		$oUsuario	= $db->Executa('UPDATE `SLSEG_USUARIO` SET `COD_PERFIL`=:codPerfil WHERE `CODIGO`=:codigo',
 				$paramUsu);
 		
 		$enviarEmail = 0;
 	}else{
 		$oPessoa	= $db->Executa('INSERT INTO `SLSEG_PESSOA`(`NOME`, `EMAIL`, `TELEFONE`, `DAT_NASCIMENTO`, `COD_SEXO`, `TIPO_SANGUINEO`, `HEMOCENTRO`) VALUES (:nome,:email,:telefone,:dataNascimento,:codSexo,:codTipoSangue,:hemocentro)', 
 				$params);
 			
 		$senha		= \AppClass\App\Util::_geraSenha(8);
 		$senhaCrypt = \AppClass\App\Crypt::crypt($email, $senha);
 		
 		/** ### Cricacao do usuario ### **/
 		$paramUsu = array(':codOrg' => $system->getCodOrganizacao(),
 				':codPessoa' => $oPessoa,
 				':usuario' => $email,
 				':senha' => $senhaCrypt,
 				':codStatus' => 0,
 				':codPerfil' => $codPerfil
 		);
 		
 		$oUsuario	= $db->Executa('INSERT INTO `SLSEG_USUARIO`(`COD_ORGANIZACAO`, `COD_PESSOA`, `USUARIO`, `SENHA`, `COD_STATUS`, `COD_PERFIL`) VALUES (:codOrg,:codPessoa,:usuario,:senha,:codStatus,:codPerfil)',
 				$paramUsu);
 		
 		$enviarEmail = 1;
 	}
 	
 	if($db->commit()){
	 	#################################################################################
		## Gerar a notificação
		#################################################################################
 		$nomeUsu		= \AppClass\App\Util::getNomeCompleto($nome);
		$template		= $db->extraiPrimeiro('SELECT * FROM `SLNOT_NOTIFICACAO_TEMPLATE` WHERE TEMPLATE =:template', array(':template' => 'USUARIO_CADASTRO'));
		$notificacao	= new \AppClass\App\Notificacao(\AppClass\App\Notificacao::TIPO_MENSAGEM_TEMPLATE, \AppClass\App\Notificacao::TIPO_DEST_ANONIMO, \AppClass\App\Notificacao::SOLICITACAO_SANGUE);
		$notificacao->setAssunto("Bem-vindo ao DoadorOnline, ".$nomeUsu["primeiro_nome"]."!");
		$notificacao->setCodRemetente($system->getCodUsuario());
		
		/*for ($i = 0; $i < sizeof($usuarios); $i++) {
			$notificacao->associaUsuario($usuarios[$i]);
		}*/
	
		$notificacao->enviaEmail();
		$notificacao->setNome($nome);
		$notificacao->setEmail($email);
		$notificacao->setCodTemplate($template->CODIGO);
		$notificacao->adicionaVariavel("NOME", $nome);
		$notificacao->adicionaVariavel("EMAIL", $email);
		$notificacao->adicionaVariavel("SENHA", 'teste');
		$notificacao->salva();
		 		
 	}else if($enviarEmail == 1){
 		throw new Exception('Ocorreu um erro ao enviar email!');
 	}
 	
 	$oPessoa	= ($codUsuario) ? $codUsuario : $oPessoa;
 	
} catch (\Exception $e) {
	$db->rollBack();
	
 	$system->criaAviso(\AppClass\App\Aviso\Tipo::ERRO, "Ocorreu um erro ao salvar o usuário. " . $e->getMessage());
 	echo '1'.\AppClass\App\Util::encodeUrl('||'.htmlentities($e->getMessage()));
 	exit;
}
 
$system->criaAviso(\AppClass\App\Aviso\Tipo::INFO,"Informações salvas com sucesso");
echo '0'.\AppClass\App\Util::encodeUrl('|'.$oPessoa);