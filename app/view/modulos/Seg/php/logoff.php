<?php
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'includeNoAuth.php');
}else{
	include_once('../../../includeNoAuth.php');
}

#################################################################################
## Define a url para onde a tela de login vai 
#################################################################################
$url = ROOT_URL ;

#################################################################################
## limpar a variável de autenticação
#################################################################################
$system->desautentica();
$system->setCodEmpresa(null);
session_destroy();
unset($_SESSION);

header("Location: ".$url, TRUE, 303);
exit;
//include(DOC_ROOT . '/view/index.php');
//exit;

