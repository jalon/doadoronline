﻿<?php 
use AppClass\App\AppClass;
#################################################################################
## Includes
#################################################################################
if (defined('DOC_ROOT')) {
	include_once(DOC_ROOT . 'includeNoAuth.php');
}else{
	include_once('../../includeNoAuth.php');
}

if (isset($_POST['nome']))					$nome				= \AppClass\App\Util::antiInjection($_POST['nome']);
if (isset($_POST['email']))					$email				= \AppClass\App\Util::antiInjection($_POST['email']);
if (isset($_POST['telefone']))				$telefone			= \AppClass\App\Util::antiInjection($_POST['telefone']);
if (isset($_POST['codEstado']))				$codEstado			= \AppClass\App\Util::antiInjection($_POST['codEstado']);
if (isset($_POST['codCidade']))				$codCidade			= \AppClass\App\Util::antiInjection($_POST['codCidade']);
if (isset($_POST['dataNascimento']))		$data_nascimento	= \AppClass\App\Util::antiInjection($_POST['dataNascimento']);
if (isset($_POST['codSexo']))				$codSexo			= \AppClass\App\Util::antiInjection($_POST['codSexo']);
if (isset($_POST['codTipoSangue']))			$tipo_sangue		= \AppClass\App\Util::antiInjection($_POST['codTipoSangue']);
if (isset($_POST['tipoUsuario']))			$tipo_usuario		= \AppClass\App\Util::antiInjection($_POST['tipoUsuario']);
if (isset($_POST['dataUltDoacao']))			$dataUltDoacao		= \AppClass\App\Util::antiInjection($_POST['dataUltDoacao']);
if (isset($_POST['codHemocentro']))			$codHemocentro		= \AppClass\App\Util::antiInjection($_POST['codHemocentro']);

#################################################################################
## Limpar a variável de erro
#################################################################################
$err	 = false;
$msgErr  = null;

#################################################################################
## Fazer validação dos campos
#################################################################################
/** Nome **/
if (!isset($nome) || (empty($nome))) {
	$msgErr .= '<li>Campo NOME é obrigatório !!! </li>';
	$err	= 1;
}

/** Email **/
if (!isset($email) || (empty($email))) {
	$msgErr .= '<li>Campo EMAIL é obrigatório !!! </li>';
	$err	= 1;
}

/** Estado **/
if (!isset($codEstado) || (empty($codEstado))) {
	$msgErr .= '<li>Campo ESTADO é obrigatório !!! </li>';
	$err	= 1;
}

/** CIDADE **/
if (!isset($codCidade) || (empty($codCidade))) {
	$msgErr .= '<li>Campo CIDADE é obrigatório !!! </li>';
	$err	= 1;
}

/** TELEFONE **/
if (!isset($telefone) || (empty($telefone))) {
	$msgErr .= '<li>Campo TELEFONE é obrigatório !!! </li>';
	$err	= 1;
}

/** DtNascimento **/
if (!isset($data_nascimento) || (empty($data_nascimento))) {
	$msgErr .= '<li>Campo DATA DE NASCIMENTO é obrigatório !!! </li>';
	$err	= 1;
}

/** Sexo **/
if (!isset($codSexo) || (empty($codSexo))) {
	$msgErr .= '<li>Campo SEXO é obrigatório !!! </li>';
	$err	= 1;
}

/** TIPO_SANGUE **/
if (!isset($tipo_sangue) || (empty($tipo_sangue))) {
	$msgErr .= '<li>Campo TIPO SANGUÍNEO é obrigatório !!! </li>';
	$err	= 1;
}

/** TIPO_USUARIO **/
if (!isset($tipo_usuario) || (empty($tipo_usuario))) {
	$msgErr .= '<li>Campo SOU é obrigatório !!! </li>';
	$err	= 1;
}else if ( $tipo_usuario == "DOA" ){
	if ( (!isset($dataUltDoacao) || empty($dataUltDoacao)) ) {/** DATA_ULT_DOACAO **/
		$msgErr .= '<li>Campo DATA ÚLTIMA DOAÇÃO é obrigatório !!! </li>';
		$err	= 1;
	}
	
	if ( (!isset($codHemocentro) || empty($codHemocentro)) && $codHemocentro != 0) {/** Hemocentro **/
		$msgErr .= '<li>Campo ONDE DOA? é obrigatório !!! </li>';
		$err	= 1;
	} else {
		$codHemocentro = null;
	}
} else{
	$codHemocentro = null;
	$dataUltDoacao = null;
}

if ($err != null) {
	echo '1| '.$msgErr;
 	exit;
}

if( \AppClass\Seg\Usuario::verificaEmail($email) == false && !empty($email)){
	try {		
		$db->transaction();
		
		if (!empty($data_nascimento)) {
			$data_nascimento	   = implode("-",array_reverse(explode("/",$data_nascimento)));
		}else{
			$data_nascimento	   = null;
		}						
		
		if (!empty($dataUltDoacao)) {
			$dataUltDoacao	   = implode("-",array_reverse(explode("/",$dataUltDoacao)));
		}else{
			$dataUltDoacao	   = null;
		}

		$param = array(':nome' => $nome,
 				':email' => $email,
				':telefone' => $telefone,
				':codEstado' => $codEstado,
				':codCidade' => $codCidade,				
 				':data_nascimento' => $data_nascimento,
				':codSexo' => $codSexo,
 				':tipo_sangue' => $tipo_sangue,
				':tipo_usuario' => $tipo_usuario,
				':dataUltDoacao' => $dataUltDoacao,
				':data_cadastro' => date('Y-m-d H:i:s')
 		);
		
		$oCadastro	= $db->Executa('INSERT INTO `SLSEG_PESSOA`(`NOME`, `EMAIL`, `TELEFONE`, `COD_ESTADO`, `COD_CIDADE`, `DAT_NASCIMENTO`, `COD_SEXO`, `TIPO_SANGUINEO`, `COD_TIPO`, `DATA_ULT_DOACAO`, `DATA_CADASTRO`)
								VALUES (:nome,:email,:telefone,:codEstado,:codCidade,:data_nascimento,:codSexo,:tipo_sangue,:tipo_usuario,:dataUltDoacao,:data_cadastro)',
				$param);
		
		$hashCode = \AppClass\App\Util::_geraCodigoRandom();
				
		$oUsuario	= $db->Executa('INSERT INTO `SLSEG_USUARIO`(`COD_ORGANIZACAO`, `COD_PESSOA`, `USUARIO`, `SENHA`, `COD_STATUS`, `HASH`, `COD_PERFIL`)
				VALUES (:codOrganizacao,:codPessoa,:usuario,:senha,:codStatus,:hash,:codPerfil)',
				array(':codOrganizacao' => 1, 
						':codPessoa' => $oCadastro, 
						':usuario' => $email,
						':senha' => null,
						':codStatus' => 0,
						':hash' => $hashCode,
						':codPerfil' => 'USU',
					) 
				);
		
		if ( $tipo_usuario == "DOA" ){
			$oDoacao = $db->Executa('INSERT INTO `SLDOA_DOACAO`(`COD_USUARIO`, `COD_PACIENTE_TIPO`, `NOME`, `COD_ESTADO`, `COD_CIDADE`, `COD_TIPO_SANGUE`, `COD_HEMOCENTRO`, `DATA_DOACAO`, `DATA_SOLICITACAO`, `COD_STATUS`)
								VALUES (:codUsuario,:codPacienteTipo,:nome,:codEstado,:codCidade,:tipo_sangue,:codHemocentro,:dataDoacao,:dataSolicitacao,:codStatus)',
					array(':codUsuario' => $oUsuario,
							':codPacienteTipo' => 1,
							':nome' => $nome,
							':codEstado' => $codEstado,
							':codCidade' => $codCidade,
							':tipo_sangue' => $tipo_sangue,
							':codHemocentro' => $codHemocentro,
							':dataDoacao' => $dataUltDoacao,
							':dataSolicitacao' => $dataUltDoacao,
							':codStatus' => "R"
					)
			);
		}
		
		$cid 			= \AppClass\App\Util::encodeUrl('_key01='.$oCadastro.'&_key02='.$oUsuario.'&_key03='.$hashCode);
		$confirmUrl		= ROOT_URL . "/Seg/confirmAuth.php?cid=".$cid;
		
		if($db->commit()){
			#################################################################################
			## Gerar a notificação
			#################################################################################
			$template		= $db->extraiPrimeiro('SELECT * FROM `SLNOT_NOTIFICACAO_TEMPLATE` WHERE TEMPLATE =:template', array(':template' => 'USUARIO_CADASTRO'));
			$notificacao	= new \AppClass\App\Notificacao(\AppClass\App\Notificacao::TIPO_MENSAGEM_TEMPLATE, \AppClass\App\Notificacao::TIPO_DEST_ANONIMO, \AppClass\App\Notificacao::CONFIRMACAO_CADASTRO);
			$notificacao->setAssunto("Confirmação de Cadastro Doador Online.");
		
			$notificacao->enviaEmail();
			$notificacao->setEmail($email); 
			$notificacao->setCodTemplate($template->CODIGO);
			$notificacao->adicionaVariavel("NOME"		, $nome);
			$notificacao->adicionaVariavel("EMAIL"		, $email);
			$notificacao->adicionaVariavel("CONFIRM_URL", $confirmUrl);
			$notificacao->salva();
	 	}else{
	 		$db->rollBack();
	 		throw new Exception();
	 	}

	 	echo '0| Cadastro realizado com sucesso!';
	} catch (\Exception $e) {
		$db->rollBack();
		echo '1| Ocorreu um erro ao realizar seu cadastro, tente novamente mais tarde ou entre em contato com a equipe.';
	}
}else if( \AppClass\Seg\Usuario::verificaEmail($email) == true && !empty($email)){
	echo '1| Esse email já foi cadastrado!';
}else{
	echo '1| Ocorreu um erro ao realizar seu cadastro, tente novamente mais tarde ou entre em contato com a equipe.';
}

?>