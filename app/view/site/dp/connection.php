<?php 

class Conexao {
    public static $conn;
   
    private function __construct() {
    }
   
    public static function conectar() {
        if (!isset(self::$conn)) {
        	try {
	            self::$conn = new PDO('mysql:host=localhost;dbname=teste', 'root', 'root', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	            self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	            self::$conn->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_EMPTY_STRING);
       		}
       		catch ( PDOException $e ) {
            	echo 'Falha na conexao contate um administrador!';
            	return false;
            }
        }
        return self::$conn;
    }
    
    public static function close() {
    	self::$conn = NULL;
    }
	
	public static function antiInjection($string) {
	
		if (is_array($string)) return $string;

		/** remove palavras que contenham sintaxe sql **/
		$string = preg_replace("/(from|select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/i","",$string);

		/** limpa espa�os vazio **/
		$string = trim($string);

		/** tira tags html e php **/
		$string = strip_tags($string);//

		/** Converte caracteres especiais para a realidade HTML **/
		$string = htmlspecialchars($string);

		if (!get_magic_quotes_gpc()) {
			$string = addslashes($string);
		}

		return ($string);
	}
}

?>