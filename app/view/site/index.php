﻿
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Doador Online - Banco de Sangue Virtual</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Banco de Sangue Virtual" />
	<meta name="keywords" content="doador, virtual, doação, sangue, doadoronline, banco de sangue" />
	<!-- <meta name="author" content="Jalon Vítor Cerqueira Silva" /> -->

  <!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content="https://doadoronline.com.br/imgs/web/logo/Logo_no_name.png" />
	<meta property="og:url" content="https://doadoronline.com.br"/>
	<meta property="og:site_name" content="Doador Online"/>
	<meta property="og:description" content="Somos um banco de sangue virtual, disponível a qualquer momento para suprir as demandas de doação de sangue dos pacientes submetidos às cirurgias cardíacas, partos, transplantes e afins."/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="/imgs/web/logo/Logo - Only 33x33.png">

	<!-- Google Webfonts -->
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	
	<!-- Font Awesome -->
	<link rel="stylesheet" href="fonts/font-awesome-4.6.2/css/font-awesome.min.css">
	
	<!-- Icheck -->
	<link href="packages/jquery-icheck/skins/all.css" rel="stylesheet" />
	
	<!-- DatePicker -->
	<link href="packages/assets/libs/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" />
	
	<!-- SelectPicker -->
	<link href="packages/assets/libs/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
	
	<!-- DatePicker -->
	<link href="packages/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
	
	<!-- Tooltip -->
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	
	<!-- Notes.css -->
	<link rel="stylesheet" href="css/notes.css">
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">
	<!-- Theme Style -->
	<link rel="stylesheet" href="css/style.css">
	<!-- Modernizr JS -->
	<script src="js/web/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/web/respond.min.js"></script>
	<![endif]-->
	<!-- jQuery -->
	<script src="js/web/jquery.min.js"></script>
	
	</head>
	<body>
		
	<header id="fh5co-header" role="banner">
		<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header"> 
				<!-- Mobile Toggle Menu Button -->
				<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#fh5co-navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
				<a href="inicio" style="clear: left; float: left; margin-bottom: 1em; margin-top: 1em;">
					<img src="imgs/web/logo/Logo%20-%20Horizontal.png">
				</a>
				</div>
				<div id="fh5co-navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-right">
						<li <?php if (isset($_GET['url']) && $_GET['url'] == "inicio"){ echo "class=\"active\"";}  ?> ><a href="inicio"><span>Início <span class="border"></span></span></a></li>
						<li <?php if (isset($_GET['url']) && $_GET['url'] == "quem-somos"){ echo "class=\"active\"";} ?> ><a href="quem-somos"><span>Quem Somos <span class="border"></span></span></a></li>
						<li <?php if (isset($_GET['url']) && $_GET['url'] == "onde-doar"){ echo "class=\"active\"";} ?> ><a href="onde-doar"><span>Onde Doar <span class="border"></span></span></a></li>
						<li <?php if (isset($_GET['url']) && $_GET['url'] == "informacoes"){ echo "class=\"active\"";} ?> ><a href="informacoes"><span>Informações <span class="border"></span></span></a></li>
						<li <?php if (isset($_GET['url']) && $_GET['url'] == "faq"){ echo "class=\"active\"";} ?> ><a href="faq"><span>FAQ <span class="border"></span></span></a></li>
						<li <?php if (isset($_GET['url']) && $_GET['url'] == "contato"){ echo "class=\"active\"";} ?> ><a href="contato"><span>Contato <span class="border"></span></span></a></li>
						<li <?php if (isset($_GET['url']) && $_GET['url'] == "painel"){ echo "class=\"active\"";} ?> ><a href="painel"><span>Login <span class="border"></span></span></a></li>						
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<!-- END .header -->
	
	<!-- Begin Content -->
	
	<?php
	
	if (isset($_GET['url']) && $_GET['url'] != '') {
		if (file_exists(DOC_ROOT . '/view/site/php/' . $_GET['url'] . ".php")) {
			include "php/" . $_GET['url'] . ".php"; 
		} else {
			include "/view/bin/notFound.php";
		}
	} else {
		include DOC_ROOT . "/view/site/php/inicio.php";
	}
	?>
	
	<!-- End Content -->
	<footer id="fh5co-footer">
		
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<br />
					<!--<div class="fh5co-footer-widget">
						<h2 class="fh5co-footer-logo">Sobre</h2>
						<p align="justify">
							Somos um banco de sangue virtual disponível a qualquer momento para suprir as demandas por doação de
							sangue aos pacientes submetidos a cirurgias cardíacas, transplante de fígado, entre outras.
						</p>
					</div>-->
					<div class="fh5co-footer-widget">
						<ul class="fh5co-social">
							<li><a href="https://fb.com/doadoronline/" target="_blank"><i class="icon-facebook"></i></a></li>
							<!--<li><a href="#"><i class="icon-twitter"></i></a></li>
							<li><a href="#"><i class="icon-instagram"></i></a></li>
							<li><a href="#"><i class="icon-linkedin"></i></a></li>
							<li><a href="#"><i class="icon-youtube"></i></a></li>-->
						</ul>
					</div>
				</div>
				<!-- 
				<div class="col-md-2 col-sm-6">
					<div class="fh5co-footer-widget top-level">
						<h4 class="fh5co-footer-lead ">Company</h4>
						<ul>
							<li><a href="#">About</a></li>
							<li><a href="#">Contact</a></li>
							<li><a href="#">News</a></li>
							<li><a href="#">Support</a></li>
							<li><a href="#">Career</a></li>
						</ul>
					</div>
				</div>
				
				<div class="visible-sm-block clearfix"></div>

				<div class="col-md-2 col-sm-6">
					<div class="fh5co-footer-widget top-level">
						<h4 class="fh5co-footer-lead">Features</h4>
						<ul class="fh5co-list-check">
							<li><a href="#">Lorem ipsum dolor.</a></li>
							<li><a href="#">Ipsum mollitia dolore.</a></li>
							<li><a href="#">Eius similique in.</a></li>
							<li><a href="#">Aspernatur similique nesciunt.</a></li>
							<li><a href="#">Laboriosam ad commodi.</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-2 col-sm-6">
					<div class="fh5co-footer-widget top-level">
						<h4 class="fh5co-footer-lead ">Products</h4>
						<ul class="fh5co-list-check">
							<li><a href="#">Lorem ipsum dolor.</a></li>
							<li><a href="#">Ipsum mollitia dolore.</a></li>
							<li><a href="#">Eius similique in.</a></li>
							<li><a href="#">Aspernatur similique nesciunt.</a></li>
							<li><a href="#">Laboriosam ad commodi.</a></li>
						</ul>
					</div>
				</div> -->
			</div>
			<div class="visible-sm-block clearfix"></div>
			
			<div class="row fh5co-row-padded fh5co-copyright">
				<div class="col-md-5">
					<p><small>2016 &copy; Doador Online. Todos Direitos Reservados.</small></p>
				</div>
			</div>
		</div>

	</footer>
	
	<!-- jQuery Easing -->
	<script src="js/web/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/web/bootstrap.min.js"></script>
	<!-- Owl carousel -->
	<script src="js/web/owl.carousel.min.js"></script>
	<!-- Waypoints -->
	<script src="js/web/jquery.waypoints.min.js"></script>
	<!-- Magnific Popup -->
	<script src="js/web/jquery.magnific-popup.min.js"></script>
	<!-- Main JS -->
	<script src="js/web/main.js"></script>
	<!-- Icheck -->
	<script src="packages/jquery-icheck/icheck.min.js"></script>
	<!-- DatePicker 
	<script src="packages/assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>-->
	<!-- SelectPicker -->
	<script src="packages/assets/libs/bootstrap-select/bootstrap-select.min.js"></script>
	<!-- DatePicker -->
	<script src="packages/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="packages/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js"></script>
	<!-- JqueryMask -->
	<script src="packages/jquery-mask/jquery.mask.min.js"></script>
	<!-- Mail Validator -->
	<script src="packages/mailgun-validator/mailgun_validator.js"></script>
	<!-- Functions -->
	<script src="js/web/doWeb.js"></script>

	</body>
</html>
