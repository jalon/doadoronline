<?php

################################################################################
# Select de Estado
################################################################################
try {
	$aEstado	 = $db->extraiTodos('SELECT * FROM `SLADM_ESTADO`', array());
	$oEstado 	 = $system->geraHtmlCombo ( $aEstado, 'COD_UF', 'NOME', null, 'Selecione o estado');
} catch ( \Exception $e ) {
	\AppClass\App\Erro::halt ( $e->getMessage (), __FILE__, __LINE__ );
}

################################################################################
# Select de Sexo
################################################################################
try {
	$aSexo	 = $db->extraiTodos('SELECT * FROM `SLSEG_SEXO`', array());
	$oSexo   = $system->geraHtmlCombo ( $aSexo, 'CODIGO', 'DESCRICAO', null, 'Selecione o sexo');
} catch ( \Exception $e ) {
	\AppClass\App\Erro::halt ( $e->getMessage (), __FILE__, __LINE__ );
}

################################################################################
# Select de TipoSangue
################################################################################
try {
	$aTipoSangue	= $db->extraiTodos('SELECT * FROM `SLADM_TIPO_SANGUE`', array());
	$oTipoSangue 	= $system->geraHtmlCombo ( $aTipoSangue, 'CODIGO', 'DESCRICAO', null, 'Selecione o tipo sanguíneo');
} catch ( \Exception $e ) {
	\AppClass\App\Erro::halt ( $e->getMessage (), __FILE__, __LINE__ );
}

################################################################################
# Select de TipoSangue
################################################################################
try {
	$aOrganizacao	= $db->extraiTodos('SELECT * FROM `SLADM_ORGANIZACAO` WHERE CODIGO != 1', array());
	$oOrganizacao 	= $system->geraHtmlCombo ( $aOrganizacao, 'CODIGO', 'NOME', null, 'Selecione o hemocentro');
} catch ( \Exception $e ) {
	\AppClass\App\Erro::halt ( $e->getMessage (), __FILE__, __LINE__ );
}

################################################################################
# Carregando o template html
################################################################################
$tpl = new \AppClass\App\Template ();
$tpl->load ( \AppClass\App\Util::getCaminhoCorrespondente ( __FILE__, \AppClass\App\ZWS::EXT_HTML ) );

################################################################################
# Define os valores das variáveis
################################################################################
$tpl->set ( 'URL_FORM'			   , $_SERVER ['SCRIPT_NAME'] );

$tpl->set ( 'COD_ESTADO'		   , $oEstado);
$tpl->set ( 'COD_SEXO'			   , $oSexo);
$tpl->set ( 'COD_TIPO_SANGUE'	   , $oTipoSangue);
$tpl->set ( 'COD_HEMOCENTRO'	   , $oOrganizacao);

$tpl->set ( 'DP', \AppClass\App\Util::getCaminhoCorrespondente ( __FILE__, \AppClass\App\ZWS::EXT_DP ) );
################################################################################
# Por fim exibir a página HTML
################################################################################
$tpl->show ();

