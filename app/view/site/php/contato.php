<aside class="fh5co-page-heading">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="fh5co-page-heading-lead">
					Contato
					<span class="fh5co-border"></span>
				</h1>
				
			</div>
		</div>
	</div>
</aside>

<div id="fh5co-main">	
	<div class="container">
		<div class="row">
			<div class="col-md-9 fh5co-feature-border">	
				<div class="row">
					<form id="FormID" action="#" method="post">
						<input type="hidden" id="validEmailID">
						<div class="col-md-6">
							<div id="divNomeID" class="form-group">
								<label for="name">Nome</label>
								<div class="pull-right" id="divHelpNomeID"></div>
								<input placeholder="Nome" id="nomeID" name="nome" type="text" class="form-control input-lg">
							</div>	
						</div>
						<div class="col-md-6">
							<div id="divEmailID" class="form-group">
								<label for="email">Email</label>
								<div class="pull-right" id="divHelpEmailID"></div>
								<input placeholder="Email" id="emailID" name="email" type="text" class="form-control input-lg">
							</div>	
						</div>
						<div class="col-md-12">
							<div id="divMensagemID" class="form-group">
								<label for="message">Mensagem</label>
								<div class="pull-right" id="divHelpMensagemID"></div>
								<textarea placeholder="Mensagem" id="mensagemID" name="mensagem" class="form-control input-lg" rows="3"></textarea>
							</div>	
						</div>
						
						<div class="col-md-12">
							<div id="divAlertID" class="hidden">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<div id="alertMsgID"></div>
							</div>
						</div>
						
						<div class="col-md-6">
							<div class="form-group">
								<button type="button" id="btnSubmitID" class="btn btn-primary">
									<i class="fa fa-send bigger-110"></i> Enviar
								</button>
								<input type="reset" class="btn btn-outline" id="cancel" value="Apagar">
							</div>	
						</div>
					</form>	
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="view">
					<p align="Justify"><b>Email:</b> suporte@doadoronline.com.br</p>
					<p align="Justify"><b>Informações:</b> Se você está com dúvidas à respeito do serviço, entre em contato agora mesmo. Estaremos dando um retorno em no máximo 48h.</p>
					<small>Equipe <cite title="Source Title">DoadorOnline™</cite></small>
				</div>
			</div>
			
		</div>
	</div>
</div>
<div class="fh5co-spacer fh5co-spacer-md"></div>

<script type="text/javascript">
$(function() {

	// capture all enter and do nothing
	$('#emailID').keypress(function(e) {
	  if(e.which == 13) {
		$('#emailID').trigger('focusout');
		return false;
	  }
	});

	// capture clicks on validate and do nothing
	$("#btnSubmitID").click(function() {
	  return false;
	});
		
	// attach jquery plugin to validate address
	$('#emailID').mailgun_validator({
	  api_key: 'pubkey-a356fcec44f99197a58f4e24a949f3bb', // replace this with your Mailgun public API key
	  in_progress: validation_in_progress,
	  success: validation_success,
	  error: validation_error,
	});
	
});

$('#btnSubmitID').on("click", function(e) {
	
	var vOK 	= validaContato();
	
	if(vOK == false) {
		return false;
	}else{
		$('#FormID').submit();
	}
});

//Submit formulário
$('#FormID').submit(function() {
	$('#btnSubmitID').html('Aguarde...  <i class="ace-icon fa fa-spinner fa-spin orange bigger-125"></i>');
	$('#btnSubmitID').attr("disabled","disabled");
	$('#cancel').attr("disabled","disabled");
	
	$.ajax({
		type:	"POST", 
		url:	"packages/phpmailer/mailgun.php",
		data:	$('#FormID').serialize(),
	}).done(function( data, textStatus, jqXHR) {
		
		if (checaRetornoOK(data) == true) {
			$('#divAlertID').removeClass('alert alert-danger');
			$('#divAlertID').addClass('alert alert-success');
			$('#divAlertID').removeClass('hidden');
			$('#alertMsgID').html(getMsgRetorno(data));
		}else{		
			$('#divAlertID').removeClass('alert alert-success');
			$('#divAlertID').addClass('alert alert-danger');
			$('#divAlertID').removeClass('hidden');
			$('#alertMsgID').html(getMsgRetorno(data));	
		}
		
		//Voltar o botão de salvar
		$('#btnSubmitID').html('<i class="fa fa-send bigger-110"></i> Enviar ');
		$('#btnSubmitID').attr("disabled",false);
		$('#cancel').attr("disabled",false);
		
	}).fail(function( jqXHR, textStatus, errorThrown) {
		$('#divAlertID').removeClass('alert alert-success');
		$('#divAlertID').addClass('alert alert-danger');
		$('#divAlertID').removeClass('hidden');
		$('#alertMsgID').html(getMsgRetorno('Ocorreu um erro ao enviar seu contato, tente novamente mais tarde ou entre em contato direto pelo email: suporte@doadoronline.com.br.'));
			
		$('#btnSubmitID').html('<i class="fa fa-send bigger-110"></i> Enviar ');
		$('#btnSubmitID').attr("disabled",false);
		$('#cancel').attr("disabled",false);
	});
	
	return false; 
});


function validaContato(){
	var vNome 		  = $('#nomeID').val();	
	var vEmail 		  = $('#emailID').val();
	var vMensagem	  = $('#mensagemID').val();
	
	var vValidEmail	  = $('#validEmailID').val();
	var vOK			  = true;

	// Nome
	if (!vNome) {
		$('#divNomeID').addClass('has-error');
		$('#divHelpNomeID').html(CriaSpanErro('Nome deve ser preenchido !!!'));
		vOK		= false;
	}else{
		$('#divNomeID').removeClass('has-error');
		$('#divHelpNomeID').html('');		
	}
	
	// Email
	if (!vEmail) {		
		$('#divEmailID').addClass('has-error');
		$('#divHelpEmailID').html(CriaSpanErro('Email deve ser preenchido !!!'));
		vOK		= false;
	}else if (vValidEmail == 0) {	
		$('#divEmailID').addClass('has-error');	
		vOK		= false;
	}else{
		$('#divEmailID').removeClass('has-error');
		$('#divHelpEmailID').html('');		
	}
	
	// Mensagem
	if (!vMensagem) {
		$('#divMensagemID').addClass('has-error');
		$('#divHelpMensagemID').html(CriaSpanErro('Mensagem deve ser preenchido !!!'));
		vOK		= false;
	}else{
		$('#divMensagemID').removeClass('has-error');
		$('#divHelpMensagemID').html('');		
	} 
	
	$('[data-toggle="tooltip"]').tooltip(); 
	
	if (vOK == true) {
		return true ;
	}else{
		return false;
	}
}


// Email Validator
// enquanto a pesquisa está a efectuar
function validation_in_progress() {
	$('#divHelpEmailID').html("<img src='packages/mailgun-validator/loading.gif' height='16'/>");
	$('#btnSubmitID').html('Aguarde...  <i class="ace-icon fa fa-spinner fa-spin orange bigger-125"></i>');
	$('#btnSubmitID').attr("disabled","disabled");
}

// Se e-mail validado com sucesso
function validation_success(data) {
	if( get_suggestion_str(data['is_valid'], data['did_you_mean']) == true){
		$('#divHelpEmailID').html( ' ' );
		$('#validEmailID').val(1);
	}else if( get_suggestion_str(data['is_valid'], data['did_you_mean']) ){ //Sugestao
		$('#divHelpEmailID').html( CriaSpanErro( get_suggestion_str(data['is_valid'], data['did_you_mean']) ) );
		$('[data-toggle="tooltip"]').tooltip(); 
		$('#validEmailID').val(1);
	}
	
	$('#btnSubmitID').html('<i class="fa fa-check bigger-110"></i> Salvar');
	$('#btnSubmitID').attr("disabled",false);
}

// Se e-mail é invalido
function validation_error(error_message) {
	$('#divHelpEmailID').html( CriaSpanErro(error_message) );
	$('[data-toggle="tooltip"]').tooltip(); 
	$('#validEmailID').val(0);
	
	$('#btnSubmitID').html('<i class="fa fa-check bigger-110"></i> Salvar');
	$('#btnSubmitID').attr("disabled",false);
}

// Sugestão de email
function get_suggestion_str(is_valid, alternate) {
	if (is_valid) {
	  if (alternate) {
		return 'Você quis dizer: ' + alternate + '?';
	  }else{
		return true;
	  }	  
	} else if (alternate) {
	  return 'Você quis dizer: ' +  alternate + '?';
	} else {
	  validation_error( 'Email inválido' );
	}
}
</script>
