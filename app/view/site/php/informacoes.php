﻿<aside class="fh5co-page-heading">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="fh5co-page-heading-lead">
					Para ser um doador, além de ter uma atitude altruísta, 
						é preciso que cumpra alguns requisitos:
					<span class="fh5co-border"></span>
				</h2>
				
			</div>
		</div>
	</div>
</aside>

<div id="fh5co-main">	
	<div class="container">
		<div class="row">
			<div class="widget-content padding">
				<div class="panel-group accordion-toggle" id="accordiondemo">
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordiondemo" href="#accordion1">
					          1. Identificação
					        </a>
					      </h4>
					    </div>
					    <div id="accordion1" class="panel-collapse collapse in">
					      <div class="panel-body">
							É obrigatória em todas as doações a apresentação de um documento oficial do Brasil, com foto.
					      </div>
					    </div>
					  </div>
					  
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordiondemo" href="#accordion2">
					          2. Idade
					        </a>
					      </h4>
					    </div>
					    <div id="accordion2" class="panel-collapse collapse">
					      <div class="panel-body">
							O doador deve ter, no mínimo, 16 anos completos e no máximo 69 anos. Para os doadores menores de 18 anos é necessário a 
							presença e o acompanhamento de um dos pais ou responsável legal, durante o processo de doação. Caso o doador opte por 
							realizar a sua primeira doação após os 60 anos, será necessária uma avaliação e aprovação prévia do seu médico acompanhante.
					      </div>
					    </div>
					  </div>
					  
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordiondemo" href="#accordion3">
					          3. Peso
					        </a>
					      </h4>
					    </div>
					    <div id="accordion3" class="panel-collapse collapse">
					      <div class="panel-body">
					      	O doador deve pesar no mínimo 50 kg.
					      </div>
					    </div>
					  </div>
					  
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordiondemo" href="#accordion4">
					          4. Pulso
					        </a>
					      </h4>
					    </div>
					    <div id="accordion4" class="panel-collapse collapse">
					      <div class="panel-body">
					      	Características normais, com frequência entre 60 e 100 bpm.
					      </div>
					    </div>
					  </div>
					  
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordiondemo" href="#accordion5">
					          5. Menstruação
					        </a>
					      </h4>
					    </div>
					    <div id="accordion5" class="panel-collapse collapse">
					      <div class="panel-body">
					      	Não contra-indica a doação. As mulheres que apresentarem sangramentos menstruais anormais 
					      	serão avaliadas pelos triagistas e só serão liberadas se tiverem condições para tal.
					      </div>
					    </div>
					  </div>
					  
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordiondemo" href="#accordion6">
					          6. Alimentação
					        </a>
					      </h4>
					    </div>
					    <div id="accordion6" class="panel-collapse collapse">
					      <div class="panel-body">
					      	O doador não deve doar em jejum, nem após uma alimentação copiosa (gordurosa). Deve fazer uma refeição 
					      	leve antes da doação. Não pode doar quem estiver em jejum prolongado (mais de 12 horas).
					      </div>
					    </div>
					  </div>
					  
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordiondemo" href="#accordion7">
					          7. Saúde
					        </a>
					      </h4>
					    </div>
					    <div id="accordion7" class="panel-collapse collapse">
					      <div class="panel-body">
					      	É importante que o doador esteja se sentindo bem e que não esteja doente.
					      </div>
					    </div>
					  </div>
					  
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordiondemo" href="#accordion8">
					          8. Atividades
					        </a>
					      </h4>
					    </div>
					    <div id="accordion8" class="panel-collapse collapse">
					      <div class="panel-body">
					      	O candidato que for piloto de avião ou helicóptero, motorista de carretas ou ônibus, trabalhar em andaimes 
					      	ou praticar paraquedismo ou mergulho e não puder interromper, por pelo menos 12 horas, as suas atividades, 
					      	não pode doar sangue.
					      </div>
					    </div>
					  </div>
					  
				</div>
				<small>Obs.: Qualquer critério para a doação pode ser alterado pelo triagista, a quem cabe, de 
						acordo com a análise do candidato à doação, a autorização final.
				</small>
			</div>
		</div>
	</div>
</div>

