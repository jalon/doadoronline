<style>
.mobileimage {
    background: rgba(0, 0, 0, 0) url("imgs/web/logo/Logo%20-%20Only%20154x154.png") no-repeat scroll 8px 5px;
    display: inline-block;
	opacity:0.65;
    margin-left: 5px;
    margin-top: 200px;
	height: 192px;
    width: 192px;
}
</style>
<!-- Begin Slide -->
<div class="fh5co-slider">
	<div class="owl-carousel owl-carousel-fullwidth">
		<div class="item" style="background-image:url(imgs/web/slide_1.png)"> 
			<div class="fh5co-overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="fh5co-owl-text-wrap">
							<div class="fh5co-owl-text text-center to-animate">
								<h1 class="fh5co-lead"></h1>	
								<div class="mobileimage"></div>								
								<h2 class="fh5co-sub-lead">Você sabia que a sua doação, pode salvar até 4 vidas?</h2>
								<a href="cadastro" class="btt" title="Cadastre-se">Cadastre-se</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="item" style="background-image:url(imgs/web/slide_5.jpg)">
			<div class="fh5co-overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="fh5co-owl-text-wrap">
							<div class="fh5co-owl-text text-center to-animate">
								<h1 class="fh5co-lead"></h1>
								<div class="mobileimage"></div>					
								<h2 class="fh5co-sub-lead">Doe sangue. Doe plasma. Doe plaqueta. Salve vidas! </h2>
								<a href="cadastro" class="btt" title="Cadastre-se">Cadastre-se</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="item" style="background-image:url(imgs/web/slide_4.png)">
			<div class="fh5co-overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="fh5co-owl-text-wrap">
							<div class="fh5co-owl-text text-center to-animate">
								<h1 class="fh5co-lead"></h1>
								<div class="mobileimage"></div>					
								<h2 class="fh5co-sub-lead">Doador de sangue e medula óssea têm direito a isenção nas taxas de concursos públicos: federais, estaduais ou municipais.</h2>
								<a href="cadastro" class="btt" title="Cadastre-se">Cadastre-se</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>	
<!-- End Slide -->

<div id="fh5co-main">
	<!-- Features -->
	<div id="fh5co-features">
		<div class="container">
			<div class="row text-center">
				<div class="col-md-8 col-md-offset-2">
					<!--<h2 class="fh5co-section-lead">Sobre</h2>-->
					<h2 class="text-center margin-bottom-xsmall"><i>Sobre o Doador Online</i></h2>
					<h3 class="fh5co-section-sub-lead">
						Somos um banco de sangue virtual, disponível a qualquer momento para suprir
						as demandas de doação de sangue dos pacientes submetidos às cirurgias
						cardíacas, partos, transplantes e afins.
					</h3>
				</div>
				<div class="fh5co-spacer fh5co-spacer-md"></div>
			</div>
			<div id="badge" class="row margin-bottom-xsmall">
				<div class="small-12 columns text-center">
					<hr style="border-top:2px solid #eeeeee;">
					<img src="imgs/web/premios.png" style="position: relative; top: -70px;">
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-4 fh5co-feature-border">
					<div class="fh5co-feature-icon to-animate col-md-offset-1">
						<img src="imgs/web/concursos/fundacao-telefonica.png" height="180px" width="300px">
					</div>
					<!--<div>
						<p><b>Vencedor do Concurso Nacional Pense Grande 2016.</b></p>
					</div>-->
				</div>
				
				<div class="col-md-4 col-sm-4 fh5co-feature-border">
					<div class="fh5co-feature-icon to-animate col-md-offset-1">
						<img src="imgs/web/concursos/desafio-universitario.png" height="180px" width="300px">
					</div>
					<!--<div class="text-center  col-md-offset-3">
						<p align="justify"><b>Finalista Nacional em 2014.</b></p>
					</div>-->
				</div>
				
				<div class="col-md-4 col-sm-4">
					<div class="fh5co-feature-icon to-animate">
						<img src="imgs/web/concursos/demo-day.png" height="180px" width="300px">
					</div>
					<!--<div class="text-center  col-md-offset-1">
						<p align="justify"><b>Finalista da Demo Day Alagoas 2015.</b></p>
					</div>-->
				</div>
				
			</div>
		</div>
	</div>
	<!-- Features -->


	<!-- Products -->
	<div class="container" id="fh5co-products">
		<div class="row text-left">
			<div class="col-md-12">
				<h2 class="fh5co-feature-text text-center"><i>Nossa Equipe</i></h2>
			</div>
			<div class="fh5co-spacer fh5co-spacer-md"></div>
		</div>
		<div class="row">
			<div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12 fh5co-mb30">
				<div class="fh5co-product">
					<img src="imgs/web/fundadores/edson_rodrigues.png" alt="Edson Rodrigues" class="img-responsive img-rounded to-animate">
					<h4>Edson Rodrigues</h4>
					<p>Co-Fundador & Gestor Administrativo.</p>
				</div>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12 fh5co-mb30">
				<div class="fh5co-product">
					<img src="imgs/web/fundadores/jalon_cerqueira.png" alt="Jalon Cerqueira" class="img-responsive img-rounded to-animate">
					<h4>Jalon Cerqueira</h4>
					<p>Co-Fundador & Desenvolvedor.</p>
				</div>
			</div>
			<div class="visible-sm-block visible-xs-block clearfix"></div>
			<div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12 fh5co-mb30">
				<div class="fh5co-product">
					<img src="imgs/web/fundadores/ruan-lee.png" alt="Ruan Lee" class="img-responsive img-rounded to-animate">
					<h4>Ruan Lee</h4>
					<p>Comunicação & Marketing.</p>
				</div>
			</div>
			
		</div>
	</div>
	<!-- Products -->
	<div class="fh5co-spacer fh5co-spacer-lg"></div>

	<div id="fh5co-clients">
		<div class="container">
			<div class="row">
				<div class="fh5co-hero-intro text-center">
					<h1 class="fh5co-hero"><font color='white'>Doe sangue. Salve vidas. Seja um herói!</font></h1>
				</div>
				<!--<div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 fh5co-client-logo text-center to-animate"><img src="imgs/web/client_1.png" alt="FREEHTML5.co Free HTML5 Bootstrap Template" class="img-responsive"></div>
				<div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 fh5co-client-logo text-center to-animate"><img src="imgs/web/client_2.png" alt="FREEHTML5.co Free HTML5 Bootstrap Template" class="img-responsive"></div>
				<div class="visible-sm-block visible-xs-block clearfix"></div>
				<div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 fh5co-client-logo text-center to-animate"><img src="imgs/web/client_3.png" alt="FREEHTML5.co Free HTML5 Bootstrap Template" class="img-responsive"></div>
				<div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 fh5co-client-logo text-center to-animate"><img src="imgs/web/client_4.png" alt="FREEHTML5.co Free HTML5 Bootstrap Template" class="img-responsive"></div>
				-->
			</div>
		</div>
	</div>

	<div class="fh5co-bg-section" style="background-image: url(imgs/web/slide_2.jpg); background-attachment: fixed;">
		<div class="fh5co-overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="fh5co-hero-wrap">
						<div class="fh5co-hero-intro text-center">
							<h1 class="fh5co-lead"><span class="quo">&ldquo;</span>Doar sangue é mais que um ato de amor. Trata-se de solidariedade e civismo, coisas que se encontram no sangue do doador.<span class="quo">&rdquo;</span></h1>
							<p class="author">&mdash; <cite>Jeferson Calixto</cite></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

