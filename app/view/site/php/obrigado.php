<?php

################################################################################
# Resgata as informações do banco
################################################################################
if ($_GET['email']) {
	try {
		$info	 = $db->extraiPrimeiro('SELECT NOME, EMAIL FROM `SLSEG_PESSOA`
				WHERE EMAIL = :email', array(':email' => $_GET['email']));
	} catch ( \Exception $e ) {
		\AppClass\App\Erro::halt ( $e->getMessage () );
	}

	$nome			 = ($info->NOME) ? $info->NOME : null;
	$email			 = ($info->EMAIL) ? $info->EMAIL : null;

} else {
	$nome	  	 	 = null;
	$email			 = null;
}

################################################################################
# Carregando o template html
################################################################################
$tpl = new \AppClass\App\Template ();
$tpl->load ( \AppClass\App\Util::getCaminhoCorrespondente ( __FILE__, \AppClass\App\ZWS::EXT_HTML ) );

################################################################################
# Define os valores das variáveis
################################################################################
$tpl->set ( 'URL_FORM'			   , $_SERVER ['SCRIPT_NAME'] );

$tpl->set ( 'NOME'		   		   , $nome);
$tpl->set ( 'EMAIL'			       , $email);

################################################################################
# Por fim exibir a página HTML
################################################################################
$tpl->show ();

