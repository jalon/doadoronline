﻿<!-- Begin Content -->
<aside class="fh5co-page-heading">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="fh5co-page-heading-lead">
					Onde Doar
					<span class="fh5co-border"></span>
				</h1>
				
			</div>
		</div>
	</div>
</aside>

<div id="fh5co-main">	
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-md-push-2">	
				<div class="row">
					<form action="#" method="post">						
						<div class="col-md-6" id="divCodEstadoID">
							<div class="form-group">
								<label for="input-text">Estado</label>
								<select id="codEstadoID" name="codEstado" onChange="onCidade();" data-width="100%" class="form-control input-lg">
									<option selected>Selecione o Estado</option>
									<option value="AL">Alagoas</option>
									<option value="PE">Pernambuco</option>
								</select>
								<div id="divHelpCodEstadoID"></div>
							</div>	
						</div>
						
						<div class="col-md-6 hidden" id="divCodCidadeID">
							<div class="form-group">
								<label for="input-text">Cidade</label>
								<select id="codCidadeID" name="codCidade" onChange="onHemocentro();" data-width="100%" class="form-control input-lg">
									<option value="Selecione" selected>Selecione a Cidade</option>
									<option value="Arapiraca">Arapiraca</option>
									<option value="Coruripe">Coruripe</option>
									<option value="Maceio">Maceió</option>
								</select>
								<div id="divHelpCodCidadeID"></div>
							</div>	
						</div>	

						<div id="divHemoMaceioID" class="hidden">
							<div class="form-group col-sm-12">
								<div class="notes col-sm-9">
									<p style="text-align: justify">
										<strong>HEMOAL - Hemocentro de Alagoas</strong>
									</p>
									<p style="text-align: justify">
										<strong><i class="fa fa-map-marker"></i> Endereço:</strong> Rua Doutor Jorge de Lima, 58 - Trapiche - próximo ao Estádio Rei Pelé.
									</p>
									<p><strong><i class="fa fa-clock-o"></i> Atendimento:</strong> Segunda a Sexta-feira 7h às 18h e Sábado 8h às 12h.</p>
									<p style="text-align: justify">
										<strong><i class="fa fa-phone"></i> Telefone:</strong> (82) 3315-2107
									</p>
								 </div>
							</div>
							<div class="form-group col-sm-12">
								<div class="notes col-sm-9">
									<p style="text-align: justify">
										<strong>HEMOAL - HOSPITAL DO AÇÚCAR</strong>
									</p>
									<p style="text-align: justify">
										<strong><i class="fa fa-map-marker"></i> Endereço:</strong> Avenida Fernandes Lima, S/N – Farol - próximo a Casa Vieira.
									</p>
									<p><strong><i class="fa fa-clock-o"></i> Atendimento:</strong> Segunda a Sexta-feira 08h às 11h. Andar térreo - próximo ao ambulatório do SUS.</p>
									<p style="text-align: justify">
										<strong><i class="fa fa-phone"></i> Telefone:</strong> (82) 3218-0242 / (82) 3315-2109
									</p>
								 </div>
						  </div>
						  <div class="form-group col-sm-12">
							<div class="notes col-sm-9">
								<p style="text-align: justify">
									<strong>HEMOPAC - Hemoterapia e Patologia Clínica de Maceió</strong>
								</p>
								<p style="text-align: justify">
									<strong><i class="fa fa-map-marker"></i> Endereço:</strong> Rua Itatiaia, 96 – Farol - próximo ao CESMAC da praça centenária.
								</p>
								<p><strong><i class="fa fa-clock-o"></i> Atendimento:</strong> Segunda a Sexta-Feira 07h às 12h e 13h às 17h.</p>
								<p style="text-align: justify">
									<strong><i class="fa fa-phone"></i> Telefone:</strong> (82) 3311-1500
								</p>
							 </div>
						  </div>
						  <div class="form-group col-sm-12">
							<div class="notes col-sm-9">
								<p style="text-align: justify">
									<strong>Santa Casa de Misericórdia de Maceió</strong>
								</p>
								<p style="text-align: justify">
									<strong><i class="fa fa-map-marker"></i> Endereço:</strong> Rua Barão de Maceio, 288 – Centro - próximo ao Quartel Geral da Polícia Militar de AL.
								</p>
								<p><strong><i class="fa fa-clock-o"></i> Atendimento:</strong> Segunda a Sexta-Feira 7h:30 às 11h e 13h às 16h</p>
								<p style="text-align: justify">
									<strong><i class="fa fa-phone"></i> Telefone:</strong> (82) 2123-6240 / (82) 2123-6098
								</p>
							 </div>
						  </div>
						  <div class="form-group col-sm-12">
							<div class="notes col-sm-9">
								<p style="text-align: justify">
									<strong>Hospital Universitário Professor Alberto Antunes</strong>
								</p>
								<p style="text-align: justify">
									<strong><i class="fa fa-map-marker"></i> Endereço:</strong> Avenida Lourival Melo Mota, S/N - Tabuleiro do Martins - próximo a UFAL
								</p>
								<p><strong><i class="fa fa-clock-o"></i> Atendimento:</strong> Segunda a Sexta-Feira 7h:30 às 12h:30</p>
								<p style="text-align: justify">
									<strong><i class="fa fa-phone"></i> Telefone:</strong> (82) 3202-3743 / (82) 3744-3742
								</p>
							 </div>
						  </div>
						</div>	

						<div id="divHemoArapiracaID" class="hidden">
							<div class="form-group col-sm-12">
								<div class="notes col-sm-9">
									<p style="text-align: justify">
										<strong>HEMOAR - Hemocentro de Arapiraca</strong>
									</p>
									<p style="text-align: justify">
										<strong><i class="fa fa-map-marker"></i> Endereço:</strong> Rua Dr. Geraldo Barbosa Lima, 49 – centro - por trás do prédio do Hospital Regional de Arapiraca.
									</p>
									<p><strong><i class="fa fa-clock-o"></i> Atendimento:</strong> Segunda a Sexta-Feira 8h às 12h e 14h às 18h.</p>
									<p style="text-align: justify">
										<strong><i class="fa fa-phone"></i> Telefone:</strong> (82) 3521–4934
									</p>
								 </div>
							</div>
						</div>
						
						<div id="divHemoCoruripeID" class="hidden">
							<div class="form-group col-sm-12">
								<div class="notes col-sm-9">
									<p style="text-align: justify">
										<strong>UCT - Unidade de Coleta e Transfusão</strong>
									</p>
									<p style="text-align: justify">
										<strong><i class="fa fa-map-marker"></i> Endereço:</strong> Rua Hildete Assis Baeta de Azevedo, 47 – centro - ao lado do Hospital Municipal de Coruripe.
									</p>
									<p><strong><i class="fa fa-clock-o"></i> Atendimento:</strong> Terça e Quinta-Feira 8h às 13h.</p>
									<p style="text-align: justify">
										<strong><i class="fa fa-phone"></i> Telefone:</strong> (82) 3315 – 2107
									</p>
								 </div>
							</div>							
						</div>	

						<div id="divHemoRecifeID" class="hidden">
							<div class="form-group col-sm-12">
								<div class="notes col-sm-9">
									<p style="text-align: justify">
										<strong>HEMOPE – Fundação de Hematologia e Hemoterapia de Pernambuco</strong>
									</p>
									<p style="text-align: justify">
										<strong><i class="fa fa-map-marker"></i> Endereço:</strong> Rua Joaquim Nabuco, 171 – Graças – Recife, próximo ao Hospital Jayme da Fonte.
									</p>
									<p><strong><i class="fa fa-clock-o"></i> Atendimento:</strong> Segunda a sábado (inclusive feriados) das 07h:15 ás 18h:30.</p>
									<p style="text-align: justify">
										<strong><i class="fa fa-phone"></i> Telefone:</strong> (81) 3182 – 4600 / 3182 - 4780
									</p>
								 </div>
							</div>	
							<div class="form-group col-sm-12">
								<div class="notes col-sm-9">
									<p style="text-align: justify">
										<strong>HEMATO - Banco de Sangue de Recife</strong>
									</p>
									<p style="text-align: justify">
										<strong><i class="fa fa-map-marker"></i> Endereço:</strong> Avenida Lins Petit, 264 - Ilha do Leite - Recife, próximo à praça chora menino.
									</p>
									<p><strong><i class="fa fa-clock-o"></i> Atendimento:</strong> Segunda a sábado, das 07h às 18h.</p>
									<p style="text-align: justify">
										<strong><i class="fa fa-phone"></i> Telefone:</strong> (81) 3972 - 4050
									</p>
								 </div>
							</div>		
						</div>
					</form>						
				</div>		
			</div>
		</div>
	</div>
</div>
<!-- End Content -->

<script type="text/javascript" charset="%CHARSET%">
function onCidade(){
	var vEstado		  = $('#codEstadoID').val();

	if(vEstado == "AL"){
		$('#codCidadeID').html("<option value='Selecione' selected>Selecione a Cidade</option> <option value=\"Arapiraca\">Arapiraca</option> <option value=\"Coruripe\">Coruripe</option> <option value=\"Maceio\">Maceió</option>");
		$('#divCodCidadeID').removeClass("hidden");
	}else if(vEstado == "PE"){
		$('#codCidadeID').html("<option value='Selecione' selected>Selecione a Cidade</option> <option value='Recife'>Recife</option>");
		$('#divCodCidadeID').removeClass("hidden");
	}else{
		$('#divCodCidadeID').addClass("hidden");
	}
}

function onHemocentro(){
	var vCidade		  = $('#codCidadeID').val();
	
	if(vCidade == "Maceio"){
		$('#divHemoArapiracaID').addClass("hidden");
		$('#divHemoCoruripeID').addClass("hidden");
		$('#divHemoRecifeID').addClass("hidden");
		
		$('#divHemoMaceioID').removeClass("hidden");
	}else if(vCidade == "Arapiraca"){
		$('#divHemoCoruripeID').addClass("hidden");
		$('#divHemoMaceioID').addClass("hidden");
		$('#divHemoRecifeID').addClass("hidden");
		
		$('#divHemoArapiracaID').removeClass("hidden");
	}else if(vCidade == "Coruripe"){
		$('#divHemoArapiracaID').addClass("hidden");
		$('#divHemoMaceioID').addClass("hidden");
		$('#divHemoRecifeID').addClass("hidden");
		
		$('#divHemoCoruripeID').removeClass("hidden");
	}else if(vCidade == "Recife"){
		$('#divHemoArapiracaID').addClass("hidden");
		$('#divHemoCoruripeID').addClass("hidden");
		$('#divHemoMaceioID').addClass("hidden");	
		
		$('#divHemoRecifeID').removeClass("hidden");
	}else{		
		$('#divHemoMaceioID').addClass("hidden");
		$('#divHemoArapiracaID').addClass("hidden");
		$('#divHemoCoruripeID').addClass("hidden");		
		$('#divHemoRecifeID').addClass("hidden");
	}
}
</script>
