<aside class="fh5co-page-heading">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="fh5co-page-heading-lead">
					Agregar e cadastrar doadores de sangue em todo o Brasil,
					com a finalidade de salvar vidas e resgatar sorrisos.
					<span class="fh5co-border"></span>
				</h2>
				
			</div>
		</div>
	</div>
</aside>

<div id="fh5co-main">	
	<div class="container">
		<div class="row">
			<div class="col-md-7">
				<div class="info-feature-horizontal">
					<div class="content">
						<h4><i class="fa fa-flag color-icon"></i> É NISSO EM QUE NÓS ACREDITAMOS</h4>
						<p align="Justify">Tudo o que você precisa para doação de sangue em uma única plataforma.
							Ferramentas eficientes, dinâmicas e soluções inteligentes que irão revolucionar
							o processo de doação de sangue, tudo de uma maneira muito simples! Doador
							Online vem para atender todas as necessidades de doação de sangue no Brasil.</p>
					</div>
				</div>
				<div class="info-feature-horizontal">
					<div class="content">
						<h4><i class="glyphicon glyphicon-heart"></i> O QUE OFERECEMOS</h4>
						<p align="Justify">Doador Online é uma plataforma inteligente de captação e fidelização de doadores,
							bem como a localização rápida e segura de doadores. Informação e conexão entre
							doadores, potenciais doadores e receptores com os hemocentros e pacientes. Agiliza o
							processo de captação de novos doadores e busca mantê-lo através do universo virtual.
							Auxilia o doador, informando-o sobre quando estará apto a doar novamente, cuidados
							pós-doação, agendar doação, além de motivá-lo com informações da importância de
							seu ato solidário. Os hemocentros, por sua vez utilizarão o sistema para localizar
							doadores que estejam hábeis a doar, respeitando o prazo entre doações.</p>
					</div>
				</div>
				<div class="info-feature-horizontal">
					<div class="content">
						<h4><i class="fa fa-group"></i> POR QUE O DOADOR ONLINE?</h4>
						<p align="Justify">Os estoques de sangue dos hemocentros estão constantemente escassos e,
							por outro lado, a demanda nunca para, logo, vislumbrou-se a necessidade da
							criação de uma ferramenta ágil e inovadora. Somos um banco de sangue
							virtual, disponível a qualquer momento para suprir as demandas de doação de
							sangue dos pacientes submetidos às cirurgias cardíacas, partos, transplantes e afins.</p>
					</div>
				</div>

			</div>
			<div class="col-md-5" align='center' style="margin-top: 40px" data-animation="bounceInRight">
				<img alt="" class="img-responsive img-center" height="70%" width="70%"
					src="imgs/web/logo/Logo_no_name.png">
			</div>
		</div>
	</div>
</div>

