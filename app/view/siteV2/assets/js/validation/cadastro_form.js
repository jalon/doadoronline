$(function() {
	$('input').iCheck({
		checkboxClass: 'icheckbox_square-red',
		radioClass: 'iradio_square-red',
		increaseArea: '20%' // optional
	});	 
	
	// attach jquery plugin to validate address
	$('#emailCadID').mailgun_validator({
	  api_key: 'pubkey-a356fcec44f99197a58f4e24a949f3bb', // replace this with your Mailgun public API key
	  in_progress: validation_in_progress,
	  success: validation_success,
	  error: validation_error,
	});
	
	// Mascara telefone 9 e 8 digito + DDD
	$('#telefoneID').change(function(event) {
		// Volta Mask 9 digito caso esteja preenchido a de 8
		$('#telefoneID').keypress(function(e) {
			if( isTeclaNumber( e.which ) && $(this).val().length == 14 ) {
				$('#telefoneID').mask('(00) 00000-0000');
			}
		});
		
		// Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
	    if($(this).val().length == 15){ 
		   $('#telefoneID').mask('(00) 00000-0000');
	    } else {
		   $('#telefoneID').mask('(00) 0000-0000');
	    }
	});
	
	// capture all enter and do nothing
	$('#emailCadID').keypress(function(e) {
	  if(e.which == 13) {
		$('#emailCadID').trigger('focusout');
		return false;
	  }
	});
	
	//Submit Cadastro formulário
	$('#btnSubmitCadID').on("click", function(e) {
		var vOK 	= validaCadastro();
		
		if(vOK == false) {
			return false;
		}else{
			$('#FormCadID').submit();
		}
	});
	
	$('#FormCadID').submit(function() {
		$('#btnSubmitCadID').html('Aguarde...  <i class="ace-icon fa fa-spinner fa-spin orange bigger-125"></i>');
		$('#btnSubmitCadID').attr("disabled","disabled");
		
		// Retirar mask para salvar
		$('[data-mask-retira="1"]').each(function( index ) {
			$( this ).val($( this ).cleanVal());
		});
		
		$.ajax({
			type:	"POST", 
			url:	"siteV2/dp/cadastro.dp.php",
			data:	$('#FormCadID').serialize(),
		}).done(function( data, textStatus, jqXHR) {
					
			if (checaRetornoOK(data) == true) {
				$('#divAlertCadID').addClass('alert alert-success');
				$('#divAlertCadID').removeClass('hidden');
				$('#alertMsgCadID').html( getMsgRetorno(data) );
				
				//thanksRedirect();
			} else {
				$('#divAlertCadID').addClass('alert alert-danger');
				$('#divAlertCadID').removeClass('hidden');
				$('#alertMsgCadID').html( getMsgRetorno(data) );
				
				//Voltar o botão de salvar
				$('#btnSubmitCadID').html('Cadastrar');
				$('#btnSubmitCadID').attr("disabled",false);
			}
			
			$('#btnSubmitCadID').html('Cadastrar');
			
		}).fail(function( jqXHR, textStatus, errorThrown) {
			$('#divAlertCadID').addClass('alert alert-danger');
			$('#divAlertCadID').removeClass('hidden');
			$('#divAlertCadID').html('Ocorreu um erro ao salvar seu cadastro, entre em contato com a equipe.');
				
			$('#btnSubmitCadID').html('Cadastrar');
			$('#btnSubmitCadID').attr("disabled",false);
		});
		
		// Reaplicar mask
		$('[data-mask-retira="1"]').each(function( index ) {
			$( this ).mask($( this ).attr('data-mask'));
			$( this ).trigger('keyup');
		});
		
		return false; 
	});
	
	function thanksRedirect() {
	  setTimeout(function(){
	    document.location.href = '/obrigado?email=' + $('#emailID').val();
	  }, 1000);
	}
	function validaCadastro(){
		var vNome 		  = $('#nomeCadID').val();	
		var vEmail 		  = $('#emailCadID').val();
		var vEstado		  = $('#codEstadoID').val();
		var vCidade		  = $('#codCidadeID').val();
		var vTelefone	  = $('#telefoneID').val();
		var vDtNascimento = $('#dataNascimentoID').val();
		var vCodSexo	  = $('#codSexoID').val();
		var vTipoSangue	  = $('#codTipoSangueID').val();
		var vDtUltDoacao  = $('#dataUltDoacaoID').val();
		var vHemocentro   = $('#codHemocentroID').val();
		var vCodTipo	  = $(".checked input[name=tipoUsuario]").val();
		
		var vValidEmail	  = $('#validEmailCadID').val();
		var vOK			  = true;
		// Zerar alerta erro
		$('#divAlertID').addClass('hidden');
		
		var err = 0;
		// Nome
		if (!vNome) {
			$('#divHelpNomeCadID').html(CriaSpanErro('Nome deve ser preenchido !!!'));
			vOK		= false;
		}else{
			$('#divHelpNomeCadID').html('');		
		}
		
		// Email
		if (!vEmail) {		
			$('#divHelpEmailCadID').html(CriaSpanErro('Email deve ser preenchido !!!'));
			vOK		= false;
		}else if (vValidEmail == 0) {	
			vOK		= false;
		}else{
			$('#divHelpEmailCadID').html('');		
		}
		
		// Estado
		if (!vEstado) {
			$('#divHelpCodEstadoID').html(CriaSpanErro('Estado deve ser preenchido !!!'));
			vOK		= false;
		}else{
			$('#divHelpCodEstadoID').html('');		
		}
		
		// Cidade
		if (!vCidade) {
			$('#divHelpCodCidadeID').html(CriaSpanErro('Cidade deve ser preenchido !!!'));
			vOK		= false;
		}else{
			$('#divHelpCodCidadeID').html('');		
		}
		
		// Telefone
		if (!vTelefone) {
			$('#divHelpTelefoneID').html(CriaSpanErro('Telefone deve ser preenchido !!!'));
			vOK		= false;
		}else if( !ValidaTelefone(vTelefone) ){
			$('#divHelpTelefoneID').html(CriaSpanErro('Telefone inválido !!!'));
			vOK		= false;
		}else{
			$('#divHelpTelefoneID').html('');		
		}
		
		// DtNascimento
		if (!vDtNascimento) {
			$('#divHelpDataNascimentoID').html(CriaSpanErro('Data de nascimento deve ser preenchido !!!'));
			vOK		= false;
		}else if( vDtNascimento.length < 10 ){
			$('#divHelpDataNascimentoID').html(CriaSpanErro('Data de nascimento inválida !!!'));
			vOK		= false;
		}else{
			$('#divHelpDataNascimentoID').html('');		
		}
		
		// Sexo
		if (!vCodSexo) {
			$('#divHelpCodSexoID').html(CriaSpanErro('Sexo deve ser escolhido !!!'));
			vOK		= false;
		}else{
			$('#divHelpCodSexoID').html('');		
		}
		
		// Tipo Sanguineo
		if (!vTipoSangue) {
			$('#divHelpCodTipoSangueID').html(CriaSpanErro('Tipo sanguíneo deve ser escolhido !!!'));
			vOK		= false;
		}else{
			$('#divHelpCodTipoSangueID').html('');		
		}
		
		if ( vCodTipo == "DOA") {
			// Data ultima Doação
			if (!vDtUltDoacao) {
				$('#divHelpDataUltDoacaoID').html(CriaSpanErro('Data da última doação é obrigatório !!!'));
				vOK		= false;
			}else{
				$('#divHelpDataUltDoacaoID').html('');		
			}
			
			// Hemocentro
			if (!vHemocentro) {
				$('#divHelpCodHemocentroID').html(CriaSpanErro('Local de doação é obrigatório !!!'));
				vOK		= false;
			}else{
				$('#divHelpCodHemocentroID').html('');		
			}
		}
		
		if (vOK == true) {
			return true ;
		}else{
			return false;
		}
	}
	
	//Email Validator
	// enquanto a pesquisa está a efectuar
	function validation_in_progress() {
		$('#divHelpEmailCadID').html("<img src='packages/mailgun-validator/loading.gif' height='16'/>");
		$('#btnSubmitCadID').html('Aguarde...  <i class="ace-icon fa fa-spinner fa-spin orange bigger-125"></i>');
		$('#btnSubmitCadID').attr("disabled","disabled");
	}
	
	// Se e-mail validado com sucesso
	function validation_success(data) {
		if( get_suggestion_str(data['is_valid'], data['did_you_mean']) == true){
			$('#divHelpEmailCadID').html( ' ' );
			$('#validEmailCadID').val(1);
		}else if( get_suggestion_str(data['is_valid'], data['did_you_mean']) ){ //Sugestao
			$('#divHelpEmailID').html( CriaSpanErro( get_suggestion_str(data['is_valid'], data['did_you_mean']) ) );
			$('#validEmailCadCadvID').val(1);
		}
		
		$('#btnSubmitCadID').html('Cadastrar');
		$('#btnSubmitCadID').attr("disabled",false);
	}
	
	// Se e-mail é invalido
	function validation_error(error_message) {
		$('#divHelpEmailCadID').html( CriaSpanErro(error_message) );
		$('#validEmailCadID').val(0);
		
		$('#btnSubmitCadID').html('Cadastrar');
		$('#btnSubmitCadID').attr("disabled",false);
	}
	
	// Sugestão de email
	function get_suggestion_str(is_valid, alternate) {
		if (is_valid) {
		  if (alternate) {
			return 'Você quis dizer: ' + alternate + '?';
		  }else{
			return true;
		  }	  
		} else if (alternate) {
		  return 'Você quis dizer: ' +  alternate + '?';
		} else {
		  validation_error( 'Email inválido' );
		}
	}
	
	//iChecks
	$('#doadorID').on('ifClicked', function(event){
		$('#divDataUltDoacaoID').removeClass('hidden');
		$('#divCodHemocentroID').removeClass('hidden');
	});
	
	$('#potencialDoadorID').on('ifClicked', function(event){
		$('#dataUltDoacaoID').val(null);
		$('#codHemocentroID').val(null);
		$('#divDataUltDoacaoID').addClass('hidden');
		$('#divCodHemocentroID').addClass('hidden');
	});
	
	$('#receptorID').on('ifClicked', function(event){
		$('#dataUltDoacaoID').val(null);
		$('#codHemocentroID').val(null);
		$('#divDataUltDoacaoID').addClass('hidden');
		$('#divCodHemocentroID').addClass('hidden');
	});
});


function buscaCidade(){
	var vEstado		  = $('#codEstadoID').val();
	
	if(vEstado){
		$.ajax({
	        type:"GET",
	        url: "/Adm/getCidade.dp.php",
	        data:{codEstado: vEstado},
		}).done(function( data, textStatus, jqXHR) {
            $('#codCidadeID').html(data);
            
		}).fail(function( jqXHR, textStatus, errorThrown) {
	        $('#codCidadeID').html('<option>Nehuma cidade encontrada!</option>');
	    });
		 
		$('#codCidadeID').removeAttr("disabled");
	}else{
		$('#codCidadeID').attr("disabled", true);
	}
}

function buscaHemocentro(){
	var vEstado		  = $('#codEstadoID').val();
	
	if(vEstado){
		$.ajax({
	        type:"GET",
	        url: "/Adm/getCidadeHemocentro.dp.php",
	        data:{codEstado: vEstado},
		}).done(function( data, textStatus, jqXHR) {
            $('#codHemocentroID').html(data);
            
		}).fail(function( jqXHR, textStatus, errorThrown) {
	        $('#codHemocentroID').html('<option>Nenhum Hemocentro encontrado para esse Estado!</option>');
	    });
		 
		$('#codHemocentroID').removeAttr("disabled");
	}else{
		$('#codHemocentroID').attr("disabled", true);
	}
}