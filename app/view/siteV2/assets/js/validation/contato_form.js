$(function() {
	//Mascaras
	$('[data-type="mask"]').each(function( index ) {
		$( this ).mask($( this ).attr('data-mask'));
	});
	
	// Mascara telefone 9 e 8 digito + DDD
	$('#numeroID').change(function(event) {
		// Volta Mask 9 digito caso esteja preenchido a de 8
		$('#numeroID').keypress(function(e) {
			if( isTeclaNumber( e.which ) && $(this).val().length == 14 ) {
				$('#numeroID').mask('(00) 00000-0000');
			}
		});
		
		// Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
	    if($(this).val().length == 15){ 
		   $('#numeroID').mask('(00) 00000-0000');
	    } else {
		   $('#numeroID').mask('(00) 0000-0000');
	    }
	});

	// capture all enter and do nothing
	$('#emailID').keypress(function(e) {
	  if(e.which == 13) {
		$('#emailID').trigger('focusout');
		return false;
	  }
	});

	// capture clicks on validate and do nothing
	$("#btnSubmitID").click(function() {
	  return false;
	});
	
	// attach jquery plugin to validate address
	$('#emailID').mailgun_validator({
	  api_key: 'pubkey-a356fcec44f99197a58f4e24a949f3bb', // replace this with your Mailgun public API key
	  in_progress: validation_in_progress,
	  success: validation_success,
	  error: validation_error,
	});
	
	//Submit formulário de Contato
	$('#btnSubmitID').on("click", function(e) {
		var vOK 	= validaContato();
		
		if(vOK == false) {
			return false;
		}else{
			$('#FormContatoID').submit();
		}
	});
	
	$('#FormContatoID').submit(function() {
		$('#btnSubmitID').html('Aguarde...  <i class="ace-icon fa fa-spinner fa-spin orange bigger-125"></i>');
		$('#btnSubmitID').attr("disabled","disabled");
		$('#cancel').attr("disabled","disabled");
		
		$.ajax({
			type:	"POST", 
			url:	"packages/phpmailer/mailgun.php",
			data:	$('#FormContatoID').serialize(),
		}).done(function( data, textStatus, jqXHR) {
			
			if (checaRetornoOK(data) == true) {
				$('#divAlertID').removeClass('alert alert-danger');
				$('#divAlertID').addClass('alert alert-success');
				$('#divAlertID').removeClass('hidden');
				$('#alertMsgID').html(getMsgRetorno(data));
			}else{		
				$('#divAlertID').removeClass('alert alert-success');
				$('#divAlertID').addClass('alert alert-danger');
				$('#divAlertID').removeClass('hidden');
				$('#alertMsgID').html(getMsgRetorno(data));	
			}
			
			//Voltar o botão de salvar
			$('#btnSubmitID').html('Enviar ');
			$('#btnSubmitID').attr("disabled",false);
			$('#cancel').attr("disabled",false);
			
		}).fail(function( jqXHR, textStatus, errorThrown) {
			$('#divAlertID').removeClass('alert alert-success');
			$('#divAlertID').addClass('alert alert-danger');
			$('#divAlertID').removeClass('hidden');
			$('#alertMsgID').html(getMsgRetorno('Ocorreu um erro ao enviar seu contato, tente novamente mais tarde ou entre em contato direto pelo email: suporte@doadoronline.com.br.'));
				
			$('#btnSubmitID').html('Enviar');
			$('#btnSubmitID').attr("disabled",false);
			$('#cancel').attr("disabled",false);
		});
		
		return false; 
	});
	
	function validaContato(){
		var vNome 		  = $('#nomeID').val();	
		var vEmail 		  = $('#emailID').val();
		var vNumero 	  = $('#numeroID').val();
		var vTitulo		  = $('#tituloID').val();
		var vMensagem	  = $('#mensagemID').val();
		
		var vValidEmail	  = $('#validEmailID').val();
		var vOK			  = true;
		
		// Nome
		if (!vNome) {
			$('#divNomeID').addClass('has-error');
			$('#divHelpNomeID').html(CriaSpanErro('Nome deve ser preenchido !!!'));
			vOK		= false;
		}else{
			$('#divNomeID').removeClass('has-error');
			$('#divHelpNomeID').html('');		
		}
		
		// Email
		if (!vEmail) {		
			$('#divEmailID').addClass('has-error');
			$('#divHelpEmailID').html(CriaSpanErro('Email deve ser preenchido !!!'));
			vOK		= false;
		}else if (vValidEmail == 0) {	
			$('#divEmailID').addClass('has-error');	
			vOK		= false;
		}else{
			$('#divEmailID').removeClass('has-error');
			$('#divHelpEmailID').html('');		
		}
		
		// Numero
		if (!vNumero) {
			$('#divNumeroID').addClass('has-error');
			$('#divHelpNumeroID').html(CriaSpanErro('Número deve ser preenchido !!!'));
			vOK		= false;
		}else{
			$('#divNumeroID').removeClass('has-error');
			$('#divHelpNumeroID').html('');		
		} 
		
		// Titulo
		if (!vTitulo) {
			$('#divTituloID').addClass('has-error');
			$('#divHelpTituloID').html(CriaSpanErro('Título deve ser preenchido !!!'));
			vOK		= false;
		}else{
			$('#divTituloID').removeClass('has-error');
			$('#divHelpTituloID').html('');		
		} 
		
		// Mensagem
		if (!vMensagem) {
			$('#divMensagemID').addClass('has-error');
			$('#divHelpMensagemID').html(CriaSpanErro('Mensagem deve ser preenchido !!!'));
			vOK		= false;
		}else{
			$('#divMensagemID').removeClass('has-error');
			$('#divHelpMensagemID').html('');		
		} 
		
		if (vOK == true) {
			return true ;
		}else{
			return false;
		}
	}
	
	// Email Validator
	// enquanto a pesquisa está a efectuar
	function validation_in_progress() {
		$('#divHelpEmailID').html("<img src='packages/mailgun-validator/loading.gif' height='16'/>");
		$('#btnSubmitID').html('Aguarde...  <i class="ace-icon fa fa-spinner fa-spin orange bigger-125"></i>');
		$('#btnSubmitID').attr("disabled","disabled");
	}
	
	// Se e-mail validado com sucesso
	function validation_success(data) {
		if( get_suggestion_str(data['is_valid'], data['did_you_mean']) == true){
			$('#divHelpEmailID').html( ' ' );
			$('#validEmailID').val(1);
		}else if( get_suggestion_str(data['is_valid'], data['did_you_mean']) ){ //Sugestao
			$('#divHelpEmailID').html( CriaSpanErro( get_suggestion_str(data['is_valid'], data['did_you_mean']) ) );
			$('#validEmailID').val(1);
		}
		
		$('#btnSubmitID').html('<i class="fa fa-check bigger-110"></i> Salvar');
		$('#btnSubmitID').attr("disabled",false);
	}
	
	// Se e-mail é invalido
	function validation_error(error_message) {
		$('#divHelpEmailID').html( CriaSpanErro(error_message) );
		$('#validEmailID').val(0);
		
		$('#btnSubmitID').html('<i class="fa fa-check bigger-110"></i> Salvar');
		$('#btnSubmitID').attr("disabled",false);
	}
	
	// Sugestão de email
	function get_suggestion_str(is_valid, alternate) {
		if (is_valid) {
		  if (alternate) {
			return 'Você quis dizer: ' + alternate + '?';
		  }else{
			return true;
		  }	  
		} else if (alternate) {
		  return 'Você quis dizer: ' +  alternate + '?';
		} else {
		  validation_error( 'Email inválido' );
		}
	}
});