$(function() {
	// capture clicks on validate and do nothing
	$("#btnSubmitLGID").click(function() {
	  return false;
	});
	
	//Submit formulário de Login
	$('#btnSubmitLGID').on("click", function(e) {
		var vOK 	= validaLogin();
		
		if(vOK == false) {
			return false;
		}else{
			$('#FormLoginID').submit();
		}
	});
	
	$('#FormLoginID').submit(function() {
		$('#btnSubmitLGID').html('Aguarde...  <i class="ace-icon fa fa-spinner fa-spin orange bigger-125"></i>');
		$('#btnSubmitLGID').attr("disabled","disabled");
		
		$.ajax({
			type:	"POST", 
			url:	"/bin/auth.php",
			data:	$('#FormLoginID').serialize(),
		}).done(function( data, textStatus, jqXHR) {
			
			if ( data == "Login efetuado com sucesso!" ) window.location = "/painel";
			
			$('#divAlertLoginID').removeClass('hidden');
			$('#responseID').html( data );
				
			//Voltar o botão de salvar
			$('#btnSubmitLGID').html('Entrar');
			$('#btnSubmitLGID').attr("disabled",false);
			
		}).fail(function( jqXHR, textStatus, errorThrown) {
			$('#divAlertLoginID').removeClass('hidden');
			$('#responseID').html('Ocorreu um erro ao efetuar seu login, tente mais tarde ou entre em contato com a equipe.');
				
			$('#btnSubmitLGID').html('Entrar');
			$('#btnSubmitLGID').attr("disabled",false);
		});
		
		return false; 
	});
	
	function validaLogin(){
		var vUsuario	  = $('#usuarioID').val();
		var vSenha 		  = $('#senhaID').val();	
		var vOK			  = true;

		// Usuario
		if (!vUsuario) {		
			$('#divHelpUsuarioID').html(CriaSpanErro('Usuário deve ser preenchido !!!'));
			vOK		= false;
		}else{
			$('#divHelpUsuarioID').html('');		
		}
		
		// Senha
		if (!vSenha) {
			$('#divHelpSenhaID').html(CriaSpanErro('Senha deve ser preenchida !!!'));
			vOK		= false;
		}else{
			$('#divHelpSenhaID').html('');		
		}
		
		if (vOK == true) {
			return true ;
		}else{
			return false;
		}
	}
});