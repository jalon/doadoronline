-- phpMyAdmin SQL Dump
-- version 4.2.13.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 20-Jan-2016 às 18:12
-- Versão do servidor: 5.6.17
-- PHP Version: 5.6.1


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `saveLife`
--

-- --------------------------------------------------------
--
-- Extraindo dados da tabela `SLADM_ORGANIZACAO`
--
INSERT INTO `SLADM_ORGANIZACAO` (`CODIGO`, `IDENTIFICACAO`, `NOME`, `COD_ESTADO`, `COD_CIDADE`, `ENDERECO`, `OBSERVACAO`) VALUES
('1', 'PAINEL', 'painel', 'AL', 2708600, 'Trav. 29 de Setembro', NULL),
-- Maceio
('2', 'HEMOAL', 'HEMOAL - Hemocentro de Alagoas', 'AL', 2704302, 'Rua Doutor Jorge de Lima, 58 – Trapiche - próximo ao Estádio Rei Pelé.', 'Segunda a Sexta-feira 7h às 18h e Sábado 8h às 12h.'),
('3', 'HEMOAL-ACUCAR', 'HEMOAL - HOSPITAL DO AÇÚCAR', 'AL', 2704302, 'Avenida Fernandes Lima, S/N – Farol - próximo a Casa Vieira.', 'Segunda a Sexta-feira 08h às 11h. Andar térreo - próximo ao ambulatório do SUS.'),
('4', 'HEMOPAC', 'HEMOPAC - Hemoterapia e Patologia Clínica de Maceió', 'AL', 2704302, 'Rua Itatiaia, 96 – Farol - próximo ao CESMAC da praça centenária.', 'Segunda a Sexta-Feira 07h às 12h e 13h às 17h.'),
('5', 'SANTA-CASA-MACEIO', 'Santa Casa de Misericórdia de Maceió', 'AL', 2704302, 'Rua Barão de Maceió, 288 – Centro - próximo ao Quartel Geral da Polícia Militar de AL.', 'Segunda a Sexta-Feira 7h:30 às 11h e 13h às 16h'),
('6', 'HU', 'Hospital Universitário Professor Alberto Antunes', 'AL', 2704302, 'Avenida Lourival Melo Mota, S/N - Tabuleiro do Martins - próximo a UFAL', 'Segunda a Sexta-Feira 7h:30 às 12h:30'),
-- Arapiraca
('7', 'HEMOAR', 'HEMOAR - Hemocentro de Arapiraca', 'AL', 2700300, 'Rua Dr. Geraldo Barbosa Lima, 49 – centro - por trás do prédio do Hospital Regional de Arapiraca', 'Segunda a Sexta-Feira 8h às 12h e 14h às 18h.'),
-- Coruripe
('8', 'UCT', 'Unidade de Coleta e Transfusão', 'AL', 2702306, 'Rua Hildete Assis Baeta de Azevedo, 47 – centro - ao lado do Hospital', 'Terça e Quinta-Feira 8h às 13h');

-- CONTATO
INSERT INTO `SLADM_ORGANIZACAO_CONTATO` (`CODIGO`, `COD_ORGANIZACAO`, `NUMERO`) VALUES
('1', '1', '82996624040'),
('2', '2', '8233152107'),
('3', '3', '8232180242'), ('4', '3', '8233152109'),
('5', '4', '8233111500'),
('6', '5', '8221236240'), ('7', '5', '8221236098'),
('8', '6', '8232023743'), ('9', '6', '8237443742'),
-- Arapiraca
('10', '7', '8235214934'),
-- Coruripe
('11', '8', '8233152107');

