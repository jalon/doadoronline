-- phpMyAdmin SQL Dump
-- version 4.2.13.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 20-Jan-2016 às 18:12
-- Versão do servidor: 5.6.17
-- PHP Version: 5.6.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `suaFormatura`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `SLADM_REGIAO`
--

CREATE TABLE IF NOT EXISTS `SLADM_REGIAO` (
  `CODIGO` varchar(2) NOT NULL,
  `NOME` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `SLADM_REGIAO`
--

INSERT INTO `SLADM_REGIAO` (`CODIGO`, `NOME`) VALUES
('CE', 'CENTROESTE'),
('DF', 'DISTRITO FEDERAL'),
('NE', 'NORDESTE'),
('NO', 'NORTE'),
('SE', 'SUDESTE'),
('SU', 'SUL');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `SLADM_REGIAO`
--
ALTER TABLE `SLADM_REGIAO`
 ADD PRIMARY KEY (`CODIGO`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
