-- phpMyAdmin SQL Dump
-- version 4.2.13.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 20-Jan-2016 às 18:12
-- Versão do servidor: 5.6.17
-- PHP Version: 5.6.1


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `saveLife`
--

-- --------------------------------------------------------
--
-- Extraindo dados da tabela `SLSEG_PESSOA`
--

INSERT INTO `SLSEG_PESSOA` (`CODIGO`, `NOME`, `EMAIL`, `TELEFONE`, `COD_ESTADO`, `COD_CIDADE`, `DAT_NASCIMENTO`, `COD_SEXO`, `TIPO_SANGUINEO`, `COD_TIPO`, `DATA_ULT_DOACAO`, `DATA_CADASTRO`) VALUES
(1, 'Jalon Vítor Cerqueira Silva', 'jalonvitor@gmail.com', '82996624040', 'AL', 2708600, '1994-02-25', 'MAS', 1, 'PDO', NULL, '2016-02-25 13:00:30'),
(2, 'Jalon Vítor Cerqueira Silva', 'jalon_cerqueira@doadoronline.com.br', '82996624040', 'AL', 2708600, '1994-02-25', 'MAS', 1, 'PDO', NULL, '2016-02-25 13:00:00'),
(3, 'Edson Rodrigues da Silva Júnior', 'edson91686332@hotmail.com', '82996858526', 'AL', '2708600', '1994-04-09', 'MAS', 9, 'PDO', NULL, '2016-08-14 10:21:30'),
(4, 'Ruan Lee Silvério da Silva', 'ruanlee2009@hotmail.com', '82996576324', 'AL', '2704302', '1997-08-07', 'MAS', 9, 'PDO', NULL, '2016-08-16 10:37:00'),
(5, 'Christian Oliveira', 'christian_oliveira@doadoronline.com.br', '82987246416', 'AL', '2704302', '1995-07-17', 'MAS', 7, 'PDO', NULL, '2016-08-27 15:55:00'),

(6, 'Amanayara Ferreira de Carvalho', 'amanayara.ferreira@gmail.com', '82996041509', 'AL', '2706703', '1996-05-27', 'FEM', 7, 'DOA', NULL, '2016-08-19 14:36:01'),
(7, 'Cristiana Roberta Cordeiro', 'cristiana_roberta@hotmail.com', '82988950883', 'AL', '2704302', '1978-06-01', 'FEM', 7, 'DOA', NULL, '2016-08-22 14:00:12'),
(8, 'CAMILA TAVARES C D SILVA', 'camilatavaresc_@hotmail.com', '82987420734', 'AL', '2704302', '1996-05-29', 'FEM', 7, 'DOA', NULL, '2016-08-22 19:32:29'),
(9, 'Renato Harley de Souza Andrade', 'renatoharley@outlook.com', '82998018487', 'AL', '2704302', '1989-02-18', 'MAS', 8, 'PDO', NULL, '2016-08-26 20:21:16'),
(10, 'Edson', 'edsonjuninho_13@hotmail.com', '82996858526', 'BA', '2900108', '1994-04-09', 'MAS', 7, 'REC', NULL, '2016-08-29 00:14:38'),
(11, 'Tamires Lopes Ribeiro', 'tami-ribeiro@outlook.com', '73998095674', 'SP', '3550308', '1997-11-30', 'FEM', 9, 'PDO', NULL, '2016-08-29 00:20:38'),
(12, 'Romildo Rodrigues', 'romildo.rodrigues@eletrobrasalagoas.com', '82988198353', 'AL', '2704302', '1971-03-07', 'MAS', 7, 'PDO', NULL, '2016-08-29 17:24:24'),
(13, 'Josineide Freire Lopes', 'josifreire@live.com', '82981879810', 'AL', '2704302', '1989-10-06', 'FEM', 7, 'PDO', NULL, '2016-08-29 21:39:27'),
(14, 'Carlla Ramos da Silva', 'carlla.silva@ics.ufal.br', '82988030234', 'AL', '2704302', '1989-05-02', 'FEM', 9, 'DOA', NULL, '2016-09-03 13:39:05'),
(15, 'Klebson Santana de Albuquerque', 'klebson06@gmail.com', '8296680702', 'AL', '2704302', '1998-06-22', 'MAS', 8, 'PDO', NULL, '2016-09-03 16:53:40'),
(16, 'Evandro leao dos santos alves', 'evandro.valeria@outlook.com', '82988685428', 'AL', '2704302', '1994-07-24', 'MAS', 1, 'DOA', NULL, '2016-09-04 03:24:45'),
(17, 'patricia da silva', 'pattyaureliano@hotmail.com', '82996726595', 'AL', '2704302', '1994-06-17', 'FEM', 9, 'PDO', NULL, '2016-09-04 13:00:41'),
(18, 'Nycholas Pires da Silva', 'nycholaspsilva@gmail.com', '82987129694', 'AL', '2704302', '1997-02-20', 'MAS', 7, 'DOA', NULL, '2016-09-04 17:07:20'),
(19, 'Daniel do Nascimento', 'danielmcz.nascimento@gmail.com', '82987046370', 'AL', '2704302', '1997-06-08', 'MAS', 9, 'DOA', NULL, '2016-09-04 22:06:24'),
(20, 'Monyki Emille Costa Nogueira Goncalves', 'mecngoncalves@hotmail.com', '8288516971', 'AL', '2704302', '1988-06-01', 'FEM', 6, 'DOA', NULL, '2016-09-05 01:58:54'),
(21, 'Marcelo Jose de Melo Araujo Junior', 'marceloaraujojunior@hotmail.com', '82996275388', 'AL', '2704302', '1993-02-17', 'MAS', 1, 'DOA', NULL, '2016-09-05 02:15:21'),
(22, 'Thays CORINE Pereira Calheiros', 'Tcalheiros12@gmail.com', '82993219877', 'AL', '2700409', '1994-09-29', 'FEM', 3, 'PDO', NULL, '2016-09-05 08:12:47'),
(23, 'Maria da Conceição de sousa', 'sousamariah6@gmail.con', '82998161724', 'AL', '2708600', '1991-05-21', 'FEM', 8, 'DOA', NULL, '2016-09-05 10:32:18'),
(24, 'Jéssica Kécia Vieira de Oliviera', 'jkvieira51@gmail.com', '82981656194', 'AL', '2707107', '1993-04-29', 'FEM', 3, 'DOA', NULL, '2016-09-05 11:10:26'),
(25, 'LARISSA DA ROCHA CARVALHO', 'rocha.larissacarvalho@gmail.com', '82999305340', 'AL', '2704302', '1995-04-07', 'FEM', 1, 'DOA', NULL, '2016-09-05 12:28:23'),
(26, 'Bruna Mayara Barbosa da Silva', 'brunnam@outlook.com', '82988456405', 'AL', '2704302', '1997-10-17', 'FEM', 9, 'PDO', NULL, '2016-09-05 12:45:28'),
(27, 'Adriana Barros de Farias Lima', 'adribflima@hotmail.com', '82988794409', 'AL', '2704302', '1972-11-13', 'FEM', 2, 'DOA', NULL, '2016-09-05 15:23:50'),
(28, 'Claudinete Melo', 'claudinetemelo@hotmail.com', '82999157630', 'AL', '2706901', '1986-11-17', 'FEM', 7, 'DOA', NULL, '2016-09-05 15:32:12'),
(29, 'Ana Marta Torres de Paula', 'anamartatorres@gmail.com', '82999706952', 'AL', '2703106', '1992-03-23', 'FEM', 1, 'DOA', NULL, '2016-09-05 16:03:11'),
(30, 'José Fernando Felix Santos', 'fernando-fernandes98@hotmail.com', '82991234235', 'AL', '2708600', '1987-08-19', 'MAS', 3, 'PDO', NULL, '2016-09-05 16:40:20'),
(31, 'Raphaela lopes dos santos', 'raphaelalopes27@outlook.com', '82999952282', 'AL', '2704302', '1986-12-02', 'FEM', 1, 'PDO', NULL, '2016-09-05 19:41:03'),
(32, 'Clarisse claire dos santos custodio', 'Clarisse-custodio@outlook.com', '82994220256', 'AL', '2704302', '1998-04-15', 'FEM', 7, 'PDO', NULL, '2016-09-05 20:33:43'),
(33, 'Adélia Augusta de Melo Dimas', 'adeliadimas@live.com', '82999194959', 'AL', '2704302', '1994-03-30', 'FEM', 1, 'DOA', NULL, '2016-09-05 21:35:39'),
(34, 'Ana Beatriz Oliveira dos Santos', 'anab_santos@outlook.com', '82996961289', 'AL', '2704302', '1998-04-12', 'FEM', 1, 'PDO', NULL, '2016-09-05 23:35:12'),
(35, 'Tâmara Larissa Mendonça Guimarães', 'tantamiu@hotmail.com', '82988112088', 'AL', '2704302', '1996-07-23', 'FEM', 9, 'PDO', NULL, '2016-09-06 01:23:11'),
(36, 'MARIA HELLOÁ COSTA DE OLIVEIRA', 'helloacosta1@hotmail.com', '82999600978', 'AL', '2704302', '1991-11-21', 'FEM', 7, 'DOA', NULL, '2016-09-06 12:00:00');




