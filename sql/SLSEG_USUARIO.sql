-- phpMyAdmin SQL Dump
-- version 4.2.13.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 20-Jan-2016 às 18:12
-- Versão do servidor: 5.6.17
-- PHP Version: 5.6.1


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `saveLife`
--

-- --------------------------------------------------------
--
-- Extraindo dados da tabela `SLSEG_USUARIO`
--

INSERT INTO `SLSEG_USUARIO` (`CODIGO`, `COD_ORGANIZACAO`, `COD_PESSOA`, `USUARIO`, `SENHA`, `COD_STATUS`, `HASH`, `COD_PERFIL`) VALUES
(1, 1, 1, 'jalonvitor@gmail.com', 'f2727a82bdbf186ded8bcfb8a1311d07', 1, NULL, 'USU'),
(2, 1, 2, 'jalon_cerqueira@doadoronline.com.br', '39ef7525e3a4e8e481134a071f0d4d4d', 1, NULL, 'USU'),
(3, 1, 3, 'edson91686332@hotmail.com', '4e68d4c33a0bf7b4eb667d2499501714', 1, NULL, 'USU'),
(4, 1, 4, 'ruanlee2009@hotmail.com', '28abdbfc3c1c7dde21a153c798cf8b53', 1, NULL, 'USU'),
(5, 1, 5, 'christian_oliveira@doadoronline.com.br', 'NULL', 1, NULL, 'USU'),

(6, 1, 6, 'amanayara.ferreira@gmail.com', NULL, 0, 'c44ad6a4ad069cf03a51a7d1649ea26c', 'USU'),
(7, 1, 7, 'cristiana_roberta@hotmail.com', NULL, 0, '640accab411ad5a6cef853ab5fc4cd39', 'USU'),
(8, 1, 8, 'camilatavaresc_@hotmail.com', 'df747656e22990a19f2596a8b55ae1f7', 1, NULL, 'USU'),
(9, 1, 9, 'renatoharley@outlook.com', '55c6bc1e08ce782a5ff2e91406d85400', 1, NULL, 'USU'),
(10, 1, 10, 'edsonjuninho_13@hotmail.com', NULL, 0, '9451537b22663a6baf666fecc53ea2fe', 'USU'),
(11, 1, 11, 'tami-ribeiro@outlook.com', NULL, 0, '9ca1e2dd27df03f8ac0552c5c5cace02', 'USU'),
(12, 1, 12, 'romildo.rodrigues@eletrobrasalagoas.com', '889f151d55a0b35a711102d0ef48ff94', 1, NULL, 'USU'),
(13, 1, 13, 'josifreire@live.com', '97fa326ba9c0d18c719ce501654ec741', 1, NULL, 'USU'),
(14, 1, 14, 'carlla.silva@ics.ufal.br', NULL, 0, '94bb0a15186aedf310d3d4bf153a0824', 'USU'),
(15, 1, 15, 'klebson06@gmail.com', NULL, 0, '1657bd7da3e8dd9d9e6eada3e2060fee', 'USU'),
(16, 1, 16, 'evandro.valeria@outlook.com', NULL, 0, '3ae5c1647f655d01dbf10e531fac3ecf', 'USU'),
(17, 1, 17, 'pattyaureliano@hotmail.com', NULL, 0, '8b47a8e6c36c43a64aafde8297aa19d1', 'USU'),
(18, 1, 18, 'nycholaspsilva@gmail.com', NULL, 0, '28d994c7a8d32ced1eb006b9ca30e73d', 'USU'),
(19, 1, 19, 'danielmcz.nascimento@gmail.com', 'ce89420788c21a82611fd80fec520a45', 1, NULL, 'USU'),
(20, 1, 20, 'mecngoncalves@hotmail.com', '372f5aad68f5ec5a21c27b25bd551a8b', 1, NULL, 'USU'),
(21, 1, 21, 'marceloaraujojunior@hotmail.com', NULL, 0, '5feec79752351267d407e716dede9307', 'USU'),
(22, 1, 22, 'Tcalheiros12@gmail.com', NULL, 0, '7fa6d1872f4e2b25a573a797f05108ff', 'USU'),
(23, 1, 23, 'sousamariah6@gmail.con', NULL, 0, '4a50d8945aa6d2d87331ff2406ee8748', 'USU'),
(24, 1, 24, 'jkvieira51@gmail.com', '1a62b72ae636070b93f4e4cb8c594dd0', 1, NULL, 'USU'),
(25, 1, 25, 'rocha.larissacarvalho@gmail.com', '4a08fc33c50308608b2c34efdb19979e', 1, NULL, 'USU'),
(26, 1, 26, 'brunnam@outlook.com', NULL, 0, 'e11c67482c4cc7199b6847b76bd484e9', 'USU'),
(27, 1, 27, 'adribflima@hotmail.com', NULL, 0, '72c31f0aed2ddfd4f981502c7d4e0da1', 'USU'),
(28, 1, 28, 'claudinetemelo@hotmail.com', NULL, 0, 'dc1845490849a54b2acdec857188f76b', 'USU'),
(29, 1, 29, 'anamartatorres@gmail.com', NULL, 0, 'bfb6919a0382859306502c8e33b3b2ca', 'USU'),
(30, 1, 30, 'fernando-fernandes98@hotmail.com', NULL, 0, '3bd0e2be80270977400bcef6b4c23112', 'USU'),
(31, 1, 31, 'raphaelalopes27@outlook.com', NULL, 0, 'a28a7ba8baf721a99529c0257f548b7f', 'USU'),
(32, 1, 32, 'Clarisse-custodio@outlook.com', NULL, 0, 'c11b9a0c1534ed063e06a162e2086428', 'USU'),
(33, 1, 33, 'adeliadimas@live.com', NULL, 0, 'b863d950f510ae40d5256d7f97c9ffa5', 'USU'),
(34, 1, 34, 'anab_santos@outlook.com', NULL, 0, '546e853b6e39172cfa175349b31b6f96', 'USU'),
(35, 1, 35, 'tantamiu@hotmail.com', NULL, 0, 'ea1d516594e3862a3e024ee81997c47c', 'USU'),
(36, 1, 36, 'helloacosta1@hotmail.com', 'f12db8076a61268785c0a921f4b8a462', 1, NULL, 'USU');





